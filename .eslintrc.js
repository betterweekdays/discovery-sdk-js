module.exports = {
    "extends": "google",
    "rules": {
        "max-nested-callbacks": ["error", 5]
    }
};