/* global beforeEach, describe, it */

var assert = require('assert');

var BWD;

describe('services/Users.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.2\/users$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Users.get();
    });
  });

  describe('#getMe()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        fullView: 0
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.5\/users\/me$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Users.getMe(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var userId = 5871;
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.2\/users\/5871$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Users.getById(userId);
    });
  });

  describe('#updateMe()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var user = {
        firstName: 'test',
        lastName: 'test'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.5\/users\/me$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.entity, user, 'entity is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        done();
      };
      BWD.Users.updateMe(user);
    });
  });

  describe('#update()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var user = {
        firstName: 'test',
        lastName: 'test'
      };
      var userId = '742';

      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.2\/users\/742$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity, user, 'entity is valid');
        done();
      };
      BWD.Users.update(userId, user);
    });
  });

  describe('#registerAuth0Token()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var token = 'KEArEAPe4FiHdbw0';
      var user = {
        firstName: 'test',
        lastName: 'test'
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.1\/user-registration$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity, user, 'entity is valid');
        done();
      };
      BWD.Users.registerAuth0Token(token, user);
    });
  });

  describe('#recordLoginStats()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.2\/users\/me\/login$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.recordLoginStats();
    });
  });

  describe('#recordLogoutStats()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.2\/users\/me\/logout$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.recordLogoutStats();
    });
  });

  describe('#getAssessmentInfo()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/user-assessments\/me$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.getAssessmentInfo();
    });
  });

  describe('#updateAssessmentInfo()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var assessmentInfo = {
        rawAnswers: JSON.stringify({})
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/user-assessments\/me$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.entity, assessmentInfo, 'entity is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        done();
      };
      BWD.Users.updateAssessmentInfo(assessmentInfo);
    });
  });

  describe('#submitAssessmentInfo()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/user-assessments\/me\/submit$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        done();
      };
      BWD.Users.submitAssessmentInfo();
    });
  });

  describe('#getIndividualAssessmentResults()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const teamMemberId = '1442';
      const endpoint = /\/discovery\/v1.0\/user-assessments\/individual\/1442$/;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(endpoint) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.getIndividualAssessmentResults(teamMemberId);
    });
  });

  describe('#triggerPayWallEvent()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/user-assessments\/paywall$/;
      const data = {
        message: 'c770ef75-dbcd-4401-acfc-6199d363757c' // Segment Anonymous ID
      };
      const query = {
        anonymous: 1
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Users.triggerPayWallEvent(data, query);
    });
  });

  describe('#triggerAssessPreviewStartedEvent()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/user-assessments\/preview$/;
      const data = {
        message: 'c770ef75-dbcd-4401-acfc-6199d363757c' // Segment Anonymous ID
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Users.triggerAssessPreviewStartedEvent(data);
    });
  });

  describe('#triggerResultsViewedEvent()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const id = '354304';
      const endpoint = /\/discovery\/v1.0\/user-assessments\/viewed\/354304$/;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(endpoint) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.triggerResultsViewedEvent(id);
    });
  });

  describe('#getAssessmentCredits()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        used: 1
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/assessmentcredit\/me$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Users.getAssessmentCredits(query);
    });
  });

  describe('#registerAssessment()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.0\/user-assessments$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.registerAssessment();
    });
  });

  describe('#claimAssessmentCredits()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const accessCode = 'code123';
      const regex = /\/discovery\/v1.1\/assessmentcreditclaim\/me\/code123/;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(regex) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.claimAssessmentCredits(accessCode);
    });
  });

  describe('#getTeams()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/team\/me$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.getTeams();
    });
  });

  describe('#getTeamAggregateAssessmentResults()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 50
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/team\/me\/aggregate$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Users.getTeamAggregateAssessmentResults(query);
    });
  });

  describe('#triggerTeamAggregateResultsViewedEvent()', function() {
    it('should invoke a request with appropriate options', function(done) {
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/team\/me\/aggregate\/viewed$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.triggerTeamAggregateResultsViewedEvent();
    });
  });

  describe('#getProfile()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const profileId = 2;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/profile\/2$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.getProfile(profileId);
    });
  });

  describe('#createProfile()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var data = {
        title: "Profile from API 1",
        publicProfileURL: "fredric-26"
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/profile$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Users.createProfile(data);
    });
  });

  describe('#updateProfile()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var pid = 'me';
      var data = {
        title: "Profile from API 1 edited",
        publicProfileURL: "fredric-profile-26",
        shareGraduationDate: "1",
        photoURL: "http:://www.google.com"
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/profile\/me$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Users.updateProfile(pid, data);
    });
  });

  describe('#discourseLogin()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var sso = "bm9uY2U9MDhhOTEyODhhMzk0ZWQ0M2ZlNGJiZTU5ODg3YzAxNDQmcm";
      sso += "V0dXJuX3Nzb191cmw9aHR0cCUzQSUyRiUyRnN0YWdlLWNvbW11bml";
      sso += "0eS50aGV3aGV0aGVyLmNvbSUyRnNlc3Npb24lMkZzc29fbG9naW4%3D";
      var data = {
        sso: sso,
        sig: "bad9fe7504f0b8513bbbad542f10565e76972831424822330aba3f7725a23a42"
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/discourse\/login$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Users.discourseLogin(data);
    });
  });

  describe('#getReferrals()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const uid = 1442;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.0\/referrals\/1442$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Users.getReferrals(uid);
    });
  });
});
