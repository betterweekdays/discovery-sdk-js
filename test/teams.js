/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Teams.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var teamId = 25;
      var query = {
        count: 50
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/team\/25$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Teams.getById(teamId, query);
    });
  });

  describe('#getAggregateAssessmentResultsV10()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var teamId = 25;
      var query = {
        count: 50
      };

      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.0\/team\/25\/aggregate$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Teams.getAggregateAssessmentResultsV10(teamId, query);
    });
  });

  describe('#triggerAggregateResultsViewedEvent()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var teamId = 25;

      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/team\/25\/aggregate\/viewed$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Teams.triggerAggregateResultsViewedEvent(teamId);
    });
  });
});
