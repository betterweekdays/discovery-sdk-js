/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Glassdoor.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#getCompanyInfo()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var params = {
        companyId: 912
      };
      BWD.GlassdoorClient.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/glassdoor$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, params, 'query option is valid');
        done();
      };
      BWD.Glassdoor.getCompanyInfo(params);
    });
  });
});
