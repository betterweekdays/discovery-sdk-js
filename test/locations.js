/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Locations.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/locations$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Locations.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var locationId = 123456;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/locations\/123456$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Locations.getById(locationId);
    });
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'St. Louis';
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/autocomplete\/location$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params.title, title,
          'title query option is valid');
        done();
      };
      BWD.Locations.autocomplete(title);
    });
  });
});
