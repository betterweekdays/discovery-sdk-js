/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Companies.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'count': 50,
        'filter[id][value]': '123456'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.1\/companies$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Companies.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 123456;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.1\/companies\/123456$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Companies.getById(companyId);
    });
  });

  describe('#setFavorite()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/favorite$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Companies.setFavorite(companyId, data);
    });
  });

  describe('#isFavorite()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/favorite$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.isFavorite(companyId);
    });
  });

  describe('#setPass()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/pass$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.setPass(companyId);
    });
  });

  describe('#isPass()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/pass$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.isPass(companyId);
    });
  });

  describe('#setViewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/viewed$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.setViewed(companyId);
    });
  });

  describe('#isViewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/viewed$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.isViewed(companyId);
    });
  });

  describe('#setPreviewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/previewed$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.setPreviewed(companyId);
    });
  });

  describe('#isPreviewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/previewed$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.isPreviewed(companyId);
    });
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'Express';
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/autocomplete\/company$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params.title, title,
          'title query option is valid');
        done();
      };
      BWD.Companies.autocomplete(title);
    });
  });

  describe('#askForHelp()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      var data = {
        message: "I'd like mentorship on this"
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/ask$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Companies.askForHelp(companyId, data);
    });
  });

  describe('#getAsksForHelp()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/ask$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Companies.getAsksForHelp(companyId);
    });
  });

  describe('#follow()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/follow$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Companies.follow(companyId, data);
    });
  });

  describe('#isFollowing()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/follow$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.isFollowing(companyId);
    });
  });

  describe('#updateMatch()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      var data = {
        source: 'e2e',
        interest: -1
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/match$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Companies.updateMatch(companyId, data);
    });
  });

  describe('#getMatchStatus()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var companyId = 12;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/companies\/12\/match$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Companies.getMatchStatus(companyId);
    });
  });
});
