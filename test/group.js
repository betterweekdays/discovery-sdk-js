/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Teams.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var groupId = 1;
      var query = {};
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/group\/1$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Group.getById(groupId, query);
    });
  });

  describe('#getMe()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {};
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/group\/me$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Group.getMe(query);
    });
  });

  describe('#createGroup()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/group$/;
      const data = {
        title: 'Test group',
        whoJoin: 'any',
        whoPostContent: 'all_users',
        findGroup: 'closed',
        contentView: 'registered',
        description: 'This is my group'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Group.createGroup(data);
    });
  });

  describe('#updateGroup()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/group\/1$/;
      var groupId = 1;
      const data = {
        description: 'This is my group with edited description'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Group.updateGroup(groupId, data);
    });
  });

  describe('#joinGroup()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/group\/1\/join$/;
      var groupId = 1;
      const data = {
        uid: 1442
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Group.joinGroup(groupId, data);
    });
  });

  describe('#leaveGroup()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/group\/1\/leave$/;
      var groupId = 1;
      const data = {
        uid: 1442
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Group.leaveGroup(groupId, data);
    });
  });
});
