/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Matches.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#getCompanies()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/matches\/company$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Matches.getCompanies(query);
    });
  });
});
