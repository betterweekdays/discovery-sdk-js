/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Feed.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/feed$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Feed.get(query);
    });
  });

  describe('#getByType()', function() {
    it('should invoke a request with appropriate type and options',
      function(done) {
        var type = 'article';
        var query = {
          count: 10
        };
        BWD.Client.request = function(options) {
          assert.ok(
            options.path.search(/\/discovery\/v1.0\/feed\/article$/) !== -1,
            'path option is valid'
          );
          assert.strictEqual(options.params, query, 'query option is valid');
          done();
        };
        BWD.Feed.getByType(type, query);
      }
    );
  });

  describe('#getArticles()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.0\/feed\/article$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Feed.getArticles(query);
    });
  });

  describe('#getJobs()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.0\/feed\/job$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Feed.getJobs(query);
    });
  });

  describe('#getCompanies()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.0\/feed\/company$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Feed.getCompanies(query);
    });
  });
});
