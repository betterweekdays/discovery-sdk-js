/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Cips.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/cips$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Cips.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var cipID = 110199;// computer science CIP id in Drupal
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/cips\/110199$/) !== -1,
          'path option is valid');
        done();
      };
      // done();
      BWD.Cips.getById(cipID);
    });
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'St. Louis';
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/autocomplete\/cip$/) !== -1,
          'path option is valid');

        assert.strictEqual(options.params.title, title,
          'title query option is valid');
        done();
      };
      BWD.Cips.autocomplete(title);
    });
  });
});
