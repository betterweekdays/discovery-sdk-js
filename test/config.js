/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Config.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should set and load valid config options', function() {
      var val = 'test';
      BWD.Config.set({
        APIHost: val
      });
      assert.deepStrictEqual(BWD.Config.get('APIHost'), val,
        'value is saved and loaded');
    });

    it('should set and load valid a single config option', function() {
      var val = 'test';
      BWD.Config.set('APIHost', val);
      assert.deepStrictEqual(BWD.Config.get('APIHost'), val,
        'value is saved and loaded');
    });

    it('should ignore invalid options', function() {
      var val = 'test';
      BWD.Config.set({
        InvalidOptionKey: val
      });
      assert.deepStrictEqual(BWD.Config.get('InvalidOptionKey'), undefined,
        'invalid value is ignored');
    });
  });
});
