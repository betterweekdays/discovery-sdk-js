/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Courses.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 10
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/courses$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Courses.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var locationId = 123456;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/courses\/123456$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Courses.getById(locationId);
    });
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'Financial';
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/autocomplete\/course$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params.title, title,
          'title query option is valid');
        done();
      };
      BWD.Courses.autocomplete(title);
    });
  });
});
