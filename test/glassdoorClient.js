/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/GlassdoorClient.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#backendInterceptor()', function() {
    it('should add Backend headers if the backend env is set', function() {
      BWD.Config.set({
        BackendEnv: 'stage'
      });
      var request = {};
      request = BWD.backendInterceptor(request);
      assert.equal(request.headers.Backend, 'stage',
        'backend interceptor adds backend header with necessary environment');
    });
  });

  describe('#ensureGlassdoorData()', function() {
    it('should retry glassdoor request ' +
      'if the glassdoor data is not yet available in the db', function() {
      var glassdoorData = {
        company: 'company_id'
      };

      BWD.ensureGlassdoorDataInterceptor = function(response) {
        if (response.entity.data && response.entity.data[0] &&
        response.entity.data[0].status === 'REQUESTED') {
          assert.ok(true,
            'request will be retried');
          response.entity.data[0] = glassdoorData;
          return response;
        }
        return response;
      };

      var options = {
        path: 'path/test'
      };
      var responseData = {
        data: [
          {status: 'REQUESTED'}
        ]
      };
      BWD.glassdoorRest = function(_options) {
        assert.deepEqual(_options, options,
          'options are passed to the rest module');
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'application/json'
            },
            entity: responseData
          };
          resolve(BWD.ensureGlassdoorDataInterceptor(response));
        });
      };
      return BWD.GlassdoorClient.request(options)
        .then(function(result) {
          assert.deepEqual(result.data[0], glassdoorData,
            'glassdoor data about company received');
        });
    });
  });

  describe('#request()', function() {
    it('should execute a successful request using mocked rest', function() {
      var options = {
        path: 'path/test',
        params: {
          alpha: 'beta'
        }
      };
      var responseData = {
        alpha: 'beta'
      };
      BWD.glassdoorRest = function(_options) {
        assert.deepEqual(_options, options,
          'options are passed to the rest module');
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'application/json'
            },
            entity: responseData
          };
          resolve(response);
        });
      };
      return BWD.GlassdoorClient.request(options)
        .then(function(result) {
          assert.deepEqual(result, responseData, 'response data received');
        });
    });

    it('should execute to the correct backend using mocked rest', function() {
      var options = {
        path: 'path/test',
        params: {
          alpha: 'beta'
        }
      };
      var responseData = {
        alpha: 'beta'
      };
      BWD.Config.set('BackendEnv', 'stage');
      BWD.glassdoorRest = function(_options) {
        options.headers = {
          Backend: 'stage'
        };
        assert.deepEqual(_options, options,
          'options are passed to the rest module');
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'application/json'
            },
            entity: responseData
          };
          resolve(response);
        });
      };
      return BWD.GlassdoorClient.request(options)
        .then(function(result) {
          assert.deepEqual(result, responseData, 'response data received');
        });
    });

    it('should report a failed request using mocked rest', function(done) {
      var options = {
        path: 'path/test'
      };
      BWD.glassdoorRest = function(/* _options */) {
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 401
            },
            request: {
              method: 'GET',
              path: 'path/test'
            }
          };
          resolve(response);
        });
      };
      BWD.GlassdoorClient.request(options)
        .catch(function(error) {
          assert.equal(error.message,
            'Request returned status code 401: GET path/test',
            'error response reported');
          done();
        });
    });
  });
});
