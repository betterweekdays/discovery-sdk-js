/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Articles.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'count': 50,
        'filter[id][value]': '123456'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.2\/articles$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query,
          'query string parameters is valid');
        done();
      };
      BWD.Articles.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 123456;
      var query = {
        fields: ['stats']
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.2\/articles\/123456$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query,
          'query string parameters is valid');
        done();
      };
      BWD.Articles.getById(articleId, query);
    });
  });

  describe('#setFavorite()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/favorite$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.setFavorite(articleId, data);
    });
  });

  describe('#isFavorite()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/favorite$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Articles.isFavorite(articleId);
    });
  });

  describe('#setPass()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/pass$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.setPass(articleId, data);
    });
  });

  describe('#isPass()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/pass$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Articles.isPass(articleId);
    });
  });

  describe('#setPreviewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/previewed$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.setPreviewed(articleId, data);
    });
  });

  describe('#isPreviewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/previewed$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Articles.isPreviewed(articleId);
    });
  });

  describe('#setViewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/viewed$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.setViewed(articleId, data);
    });
  });

  describe('#isViewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/viewed$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Articles.isViewed(articleId);
    });
  });

  describe('#setReadMore()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/read-more$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.setReadMore(articleId, data);
    });
  });

  describe('#isReadMore()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/read-more$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Articles.isReadMore(articleId);
    });
  });

  describe('#askForHelp()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        message: "I'd like mentorship on this"
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/ask$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.askForHelp(articleId, data);
    });
  });

  describe('#getAsksForHelp()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/articles\/1234\/ask$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Articles.getAsksForHelp(articleId);
    });
  });

  describe('#getCategories()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 50
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/articlecategories$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query,
          'query string parameters is valid');
        done();
      };
      BWD.Articles.getCategories(query);
    });
  });

  describe('#getCategoriesById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 123456;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/articlecategories\/123456$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Articles.getCategoriesById(articleId);
    });
  });

  describe('#followCategories()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(
            /\/discovery\/v1.0\/articlecategories\/1234\/follow$/
          ) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Articles.followCategories(articleId, data);
    });
  });

  describe('#isFollowingCategories()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(
            /\/discovery\/v1.0\/articlecategories\/1234\/follow$/
          ) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Articles.isFollowingCategories(articleId);
    });
  });

  describe('#createArticle()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var endpoint = /\/discovery\/v1.2\/articles$/;
      var body = 'Extroverted people are often very outgoing,';
      body += ' the life of the party, and enjoy meeting and';
      body += ' talking to people. Any of those sound like you?';
      body += ' Check out the vid above to see if you agree';
      body += ' with the extrovert view of life edited.';
      var data = {
        title: 'Testing Article #1',
        body: body,
        categories: [427773, 480277]
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Articles.createArticle(data);
    });
  });

  describe('#updateArticle()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var articleId = 1234;
      var endpoint = /\/discovery\/v1.2\/articles\/1234$/;
      var data = {
        title: 'Testing Article #1 edited'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Articles.updateArticle(articleId, data);
    });
  });
});
