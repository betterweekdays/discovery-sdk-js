/* eslint camelcase: ["error", {properties: "never"}] */
/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Auth.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
    BWD.Auth.reset();
  });

  describe('#login()', function(done) {
    it('should invoke Auth0.login() with the given credentials', function() {
      var email = 'test_email';
      var password = 'test_password';
      BWD.Auth0 = {
        login: function(options, callback) {
          assert.equal(options.email, email, 'email parameter found');
          assert.equal(options.password, password, 'password parameter found');
          callback(null, true);
        }
      };
      BWD.Auth.login(email, password)
        .then(function(result) {
          assert(result, 'login returned successfully');
        });
    });
  });

  describe('#signup()', function(done) {
    it('should invoke Auth0.signup() with the given credentials', function() {
      var email = 'test_email';
      var password = 'test_password';
      var fullname = 'test_fullname';
      BWD.Auth0 = {
        signup: function(options, callback) {
          assert.equal(options.email, email, 'email parameter found');
          assert.equal(options.password, password, 'password parameter found');
          assert.equal(options.fullname, fullname, 'fullname parameter found');
          callback(null, true);
        }
      };
      BWD.Auth.signup(email, password, fullname)
        .then(function(result) {
          assert(result, 'signup returned successfully');
        });
    });
  });

  describe('#refreshToken()', function(done) {
    it('should invoke Auth0.silentAuthentication()', function() {
      BWD.Auth0 = {
        silentAuthentication: function(options, callback) {
          var result = {
            accessToken: 'abcdefghijklmnop'
          };
          callback(null, result);
        }
      };
      BWD.Auth.refreshToken()
        .then(function(result) {
          assert(true, 'refreshToken returned successfully');
        });
    });
  });
});
