/* global before, beforeEach, afterEach, describe, it */

var assert = require('assert');
var dogapi = require('dogapi');
var clone = require('lodash/cloneDeep');

var BWD;
var APIEnv = process.env.BWD_API_ENV || 'prod';
var BackendEnv = process.env.BWD_API_BACKEND || 'stage';

var commonDatadogTags = [];

/**
 * Submit a new timing metric
 * @param {*} points a single data point (e.g. 50),
 * an array of data points (e.g. [50, 100]) or
 * an array of [timestamp, value] elements (e.g. [[now, 50], [now, 100]])
 * @param {*} extra optional object which can contain tags, metric type
 */
function sendTimingMetric(points, extra) {
  dogapi.metric.send(
    'discovery.sdk.performance',
    points,
    extra,
    function(err, results) {
      if (err) {
        console.log('datadog error');
        console.log(err);
      }
    }
  );
}

describe('e2e_readonly', function() {
  var ddTags = [];

  before(function() {
    if (BackendEnv) {
      commonDatadogTags.push('env:' + BackendEnv);
    }
    ddTags = clone(commonDatadogTags);
  });

  beforeEach(function() {
    if (!process.env.hasOwnProperty('BWD_API_KEY') ||
        !process.env.hasOwnProperty('BWD_API_SECRET')) {
      console.log('Skipping e2e tests because the API key or secret ' +
        'environment variable (BWD_API_KEY or BWD_API_SECRET) is missing.');
      this.skip();
    }

    if (!process.env.hasOwnProperty('BWD_DATADOG_API_KEY')) { // eslint-disable-line no-negated-condition
      console.log('datadog api or app key not set');
    } else {
      dogapi.initialize({
        api_key: process.env.BWD_DATADOG_API_KEY // eslint-disable-line camelcase
      });

      ddTags.push('user:' + process.env.BWD_USER_EMAIL);
    }

    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
    BWD.Config.set({
      APIKey: process.env.BWD_API_KEY,
      APISecret: process.env.BWD_API_SECRET,
      APIEnv: APIEnv,
      BackendEnv: BackendEnv
    });
    return BWD.Client.getAccessToken();
  });

  afterEach(function() {
    var tags = clone(commonDatadogTags);
    var timingExtra = {tags: tags};

    timingExtra.tags.push(
      'test:' + this.currentTest.title.split(' ').join('-'));
    timingExtra.tags.push(
      'status:' + this.currentTest.state
    );

    sendTimingMetric(this.currentTest.duration, timingExtra);
  });

  describe('BWD.Jobs', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 50
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.getById()');
          done();
        });
    });

    it('#isFavorite()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.isFavorite(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.isFavorite()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isPass()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.isPass(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.isPass()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isInterestedInApplying()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.isInterestedInApplying(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.isInterestedInApplying()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#getAsksForHelp()', function(done) {
      var params = {
        range: 10
      };
      var jobId = null;
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            jobId = results.data[0].id;
            return BWD.Jobs.getAsksForHelp(jobId);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.getAsksForHelp()');
          done();
        });
    });

    it('#isPreviewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.isPreviewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.isPreviewed()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isViewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.isViewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.isViewed()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#getInteractions()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.getInteractions(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.getInteractions()');
          done();
        });
    });
  });

  describe('BWD.Companies', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 50
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.getById()');
          done();
        });
    });

    it('#get() with normalized name filter', function(done) {
      var query = {
        'filter[normalizedName]': 'st-louis-regional-chamber'
      };
      var companyName = 'St. Louis Regional Chamber';
      BWD.Companies.get(query)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('name') && results.data[0].name) {
            assert.equal(results.data[0].name, companyName,
            'got successful St. Louis Regional Chamber company');
            done();
          }
        });
    });

    it('#isFavorite()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.isFavorite(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.isFavorite()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#getAsksForHelp()', function(done) {
      var params = {
        range: 10
      };
      var companyId = null;
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            companyId = results.data[0].id;
            return BWD.Companies.getAsksForHelp(companyId);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.getAsksForHelp()');
          done();
        });
    });

    it('#isPass()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.isPass(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.isPass()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isViewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.isViewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.isViewed()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isPreviewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.isPreviewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.isPreviewed()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'Express';
      BWD.Companies.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.autocomplete()');
          done();
        });
    });

    it('#isFollowing()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.isFollowing(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.isFollowing()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('follow'),
            'data contain necessary follow field');
          done();
        });
    });

    it('#getMatchStatus()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.getMatchStatus(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.getMatchStatus()');
          done();
        });
    });
  });

  describe('BWD.Schools', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 50
      };
      BWD.Schools.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Schools.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Schools.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Schools.getById()');
          done();
        });
    });

    it('#get() with normalized name filter', function(done) {
      var query = {
        'filter[name]': 'Morehouse College'
      };
      var schoolName = 'Morehouse College';
      BWD.Schools.get(query)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Schools.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('name') && results.data[0].name) {
            assert.equal(results.data[0].name, schoolName,
            'got successful Morehouse College school');
            done();
          }
        });
    });
  });

  describe('BWD.Users', function() {
    it('#getMe() and #getById()', function(done) {
      BWD.Users.getMe()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Users.getMe()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Users.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Users.getById()');
          done();
        });
    });

    it('#get()', function(done) {
      BWD.Users.get()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Users.get()');
          done();
        });
    });

    it('#getAssessmentInfo()', function(done) {
      BWD.Users.getAssessmentInfo()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Users.getAssessmentInfo()');
          done();
        });
    });
  });

  describe('BWD.Favorites', function() {
    it('#get()', function(done) {
      var query = {
        type: 'favoritejob'
      };
      BWD.Favorites.get(query)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Favorites.get()');
          done();
        });
    });
  });

  describe('BWD.Interactions', function() {
    it('#get()', function(done) {
      var params = {
        range: 10
      };
      BWD.Interactions.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Interactions.get()');
          done();
        });
    });
  });

  describe('BWD.Feed', function() {
    it('#get()', function(done) {
      BWD.Feed.get()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Feed.get()');
          done();
        });
    });

    it('#getArticles()', function(done) {
      BWD.Feed.getArticles()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Feed.getArticles()');
          done();
        });
    });

    it('#getJobs()', function(done) {
      BWD.Feed.getJobs()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Feed.getJobs()');
          done();
        });
    });

    it('#getCompanies()', function(done) {
      BWD.Feed.getCompanies()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Feed.getCompanies()');
          done();
        });
    });
  });

  describe('BWD.Messaging', function() {
    it('#getMessages() and #getMessage()', function(done) {
      var params = {
        'filter[msgType]': 'message',
        'fields': 'id,from,recipient,body,subject'
      };
      BWD.Messaging.getMessages(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Messaging.getMessages()');
          if (results.data && results.data.length &&
            results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Messaging.getMessage(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Messaging.getMessage()');
          done();
        });
    });

    it('#isMessagesRead()', function(done) {
      var params = {
        range: 10
      };
      BWD.Messaging.getMessages(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Messaging.getMessages()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Messaging.isMessagesRead(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Messaging.isMessagesRead()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });
  });

  describe('BWD.Locations', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Locations.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Locations.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Locations.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Locations.getById()');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'St. Louis';
      BWD.Locations.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Locations.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Cips', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Cips.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Cips.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Cips.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Cips.getById()');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'Food';
      BWD.Cips.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Cips.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Onets', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Onets.get(params)
          .then(function(results) {
            assert.ok(results.hasOwnProperty('data'),
                'got successful response for BWD.Onets.get()');
            if (results.data && results.data.length &&
                results.data[0].hasOwnProperty('id') && results.data[0].id) {
              return BWD.Onets.getById(results.data[0].id);
            }
          })
          .then(function(results) {
            assert.ok(results.hasOwnProperty('data'),
                'got successful response for BWD.Onets.getById()');
            done();
          });
    });

    it('#autocomplete()', function(done) {
      var title = 'Health';
      BWD.Onets.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Onets.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Orgs', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Orgs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Orgs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Orgs.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Orgs.getById()');
          done();
        });
    });
  });

  describe('autocomplete', function() {
    it('#BWD.JobAreas.autocomplete()', function(done) {
      var title = 'healthcare';
      BWD.JobAreas.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.JobAreas.autocomplete()');
          done();
        });
    });

    it('#BWD.Schools.autocomplete()', function(done) {
      var title = 'university';
      BWD.Schools.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Schools.autocomplete()');
          done();
        });
    });

    it('#BWD.DegreeSpecializations.autocomplete()', function(done) {
      var title = 'architecture';
      BWD.DegreeSpecializations.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for ' +
            'BWD.DegreeSpecializations.autocomplete()');
          done();
        });
    });

    it('#BWD.DegreePrograms.autocomplete()', function(done) {
      var title = 'finance';
      BWD.DegreePrograms.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.DegreePrograms.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Articles', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getById()');
          done();
        });
    });

    it('#isFavorite()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.isFavorite(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.isFavorite()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#getAsksForHelp()', function(done) {
      var params = {
        range: 10
      };
      var articleId = null;
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            articleId = results.data[0].id;
            return BWD.Articles.getAsksForHelp(articleId);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getAsksForHelp()');
          done();
        });
    });

    it('#isPass()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.isPass(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.isPass()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isPreviewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.isPreviewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.isPreviewed()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isViewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.isViewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.isViewed()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#isReadMore()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.isReadMore(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.isReadMore()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('stat'),
            'data contain necessary stat field');
          done();
        });
    });

    it('#getCategories() and #getCategoriesById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.getCategories(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getCategories()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.getCategoriesById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getCategoriesById()');
          done();
        });
    });

    it('#isFollowingCategories()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.getCategories(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getCategories()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.isFollowingCategories(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.isFollowingCategories()');
          assert.ok(results.data && results.data.length &&
            results.data[0].hasOwnProperty('follow'),
            'data contain necessary follow field');
          done();
        });
    });
  });

  describe('BWD.Stats', function() {
    it('#get()', function(done) {
      var params = {
        type: 'interestedapplying,favoritearticle'
      };
      BWD.Stats.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Stats.get()');
          done();
        });
    });

    it('#getSummary()', function(done) {
      var params = {
        from: 0,
        to: Date.now()
      };
      BWD.Stats.getSummary(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Stats.getSummary()');
          done();
        });
    });
  });

  describe('BWD.Search', function() {
    it('#search() for jobs', function(done) {
      var contentType = 'job';
      var query = {
        title: 'Healthcare',
        jobarea: 152,
        internships: false
      };
      BWD.Search.search(contentType, query)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.search()');
          done();
        });
    });

    it('#search() for articles', function(done) {
      var contentType = 'article';
      var query = {
        title: 'developer',
        internships: false
      };
      BWD.Search.search(contentType, query)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.search()');
          done();
        });
    });

    it('#search() for companies', function(done) {
      var contentType = 'company';
      var query = {
        title: 'designer',
        internships: true
      };
      BWD.Search.search(contentType, query)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.search()');
          done();
        });
    });
  });

  describe('BWD.Courses', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        range: 10
      };
      BWD.Courses.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Courses.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Courses.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Courses.getById()');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'Web';
      BWD.Courses.autocomplete(title)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Courses.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Advice', function() {
    it('#get()', function(done) {
      BWD.Advice.get()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Advice.get()');
          done();
        });
    });
  });

  describe('BWD.Competencies', function() {
    it('#get() and #getById()', function(done) {
      var params = {
        competencyType: "dealing_people"
      };
      BWD.Competencies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Competencies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Competencies.getById(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Competencies.getById()');
          done();
        });
    });
  });

  describe('BWD.Matches', function() {
    it('#getCompanies()', function(done) {
      BWD.Matches.getCompanies()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Matches.getCompanies()');
          done();
        });
    });
  });
});
