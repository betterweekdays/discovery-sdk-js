/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Stats.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var params = {
        'filter[type][value][1]': 'favoritejob',
        'filter[type][value][2]': 'interestedapplying',
        'filter[type][operator]': 'IN'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/stats$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, params, 'query option is valid');
        done();
      };
      BWD.Stats.get(params);
    });
  });

  describe('#getSummary()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var params = {
        from: 0,
        to: Date.now()
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.0\/stats\/summary$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, params, 'query option is valid');
        done();
      };
      BWD.Stats.getSummary(params);
    });
  });
});
