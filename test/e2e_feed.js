/* global beforeEach, afterEach, describe, it, after, before */
/* eslint max-nested-callbacks: ["error", 6] */

var assert = require('assert');

var BWD;

var countBy = require('lodash/countBy');
var uniqBy = require('lodash/uniqBy');
var every = require('lodash/every');
var map = require('lodash/map');
var each = require('lodash/each');
var difference = require('lodash/difference');
var intersection = require('lodash/intersection');
var padEnd = require('lodash/padEnd');
var dogapi = require('dogapi');
var includes = require('lodash/includes');
var groupBy = require('lodash/groupBy');
var pickBy = require('lodash/pickBy');
var keys = require('lodash/keys');
var clone = require('lodash/cloneDeep');
var feed = [];
var jobSources = {};
var CURATED_SOURCE = 'curated';

var datadogTags = [];

var APIEnv = process.env.BWD_API_ENV || 'prod';
var BackendEnv = process.env.BWD_API_BACKEND || 'stage';

/**
 * Find out number in Fibonacci sequence by index
 * @param {number} index Position index in Fibonacci sequence
 * @return {number} Fibonacci number by index in sequence
 */
function fibonacci(index) {
  var a = 1;
  var b = 0;
  var temp;

  while (index >= 0) {
    temp = a;
    a += b;
    b = temp;
    index--;
  }

  return b;
}

/**
 * Callback function is invoked after metric sending
 * @param {*} err error
 * @param {*} results result status
 */
function dogapiMetricCallback(err, results) {
  if (err) {
    console.log('datadog error');
    console.log(err);
  }
}

/**
 * Submit a new timing metric
 * @param {*} points a single data point (e.g. 50),
 * an array of data points (e.g. [50, 100]) or
 * an array of [timestamp, value] elements (e.g. [[now, 50], [now, 100]])
 * @param {*} extra optional object which can contain tags, metric type
 */
function sendTimingMetric(points, extra) {
  dogapi.metric.send(
    'feed.performance',
    points,
    extra,
    dogapiMetricCallback
  );
}

describe('e2e_feed', function() {
  var ddTags = [];
  var relevancyScore = 0;
  var relevancyTests = [
    'ensure feed does not contain passed items',
    'ensure feed contains at least 10 cip matches',
    'ensure feed contains jobs with matching ONET',
    'ensure feed contains jobs with matching goal'
  ];

  var totalJobContent = 0;

  before(function() {
    if (BackendEnv) {
      datadogTags.push('env:' + BackendEnv);
    }
    ddTags = clone(datadogTags);
  });

  beforeEach(function() {
    if (!process.env.hasOwnProperty('BWD_USER_EMAIL') ||
        !process.env.hasOwnProperty('BWD_USER_PASS') ||
        !process.env.hasOwnProperty('AUTH0_CLIENT_ID')) {
      console.log('Skipping e2e tests because email or password' +
        ' or Auth0 Connection environment variable (BWD_USER_EMAIL ' +
        'or BWD_USER_PASS or AUTH0_CLIENT_ID) is missing.');
      this.skip();
    }

    var email = process.env.BWD_USER_EMAIL;
    var password = process.env.BWD_USER_PASS;

    if (!process.env.hasOwnProperty('BWD_DATADOG_API_KEY')) { // eslint-disable-line no-negated-condition
      console.log('datadog api or app key not set');
    } else {
      dogapi.initialize({
        api_key: process.env.BWD_DATADOG_API_KEY // eslint-disable-line camelcase
      });

      ddTags.push('user:' + email);
    }

    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
    BWD.Config.set({
      Auth0ID: process.env.AUTH0_CLIENT_ID,
      Auth0Connection: process.env.AUTH0_CONNECTION ||
        'Username-Password-Authentication'
    });
    return BWD.Auth.login(email, password)
      .then(function(result) {
        BWD.Config.set({
          APIEnv: APIEnv,
          BackendEnv: BackendEnv,
          AccessToken: {
            id_token: result.idToken // eslint-disable-line camelcase
          }
        });
      })
      .catch(function() {
        assert.ok(false, 'Auth0 ID token could not be retrieved with the ' +
          'given username, password, and client ID.');
      });
  });

  afterEach(function() {
    var tags = clone(datadogTags);
    var datadogExtra = {tags: tags};
    datadogExtra.tags.push(
      'test:' + this.currentTest.title.split(" ").join("-"));

    var timingExtra = {tags: tags};
    timingExtra.tags.push(
      'status:' + this.currentTest.state
    );

    var status;
    if (this.currentTest.state === 'passed') {
      status = 1;
      if (includes(relevancyTests, this.currentTest.title)) {
        relevancyScore++;
      }
    }

    if (this.currentTest.state === 'failed') {
      status = 0;
    }

    dogapi.metric.send(
      'feed.results',
      status,
      datadogExtra,
      dogapiMetricCallback
    );

    sendTimingMetric(this.currentTest.duration, timingExtra);
  });

  after(function() {
    var tags = clone(datadogTags);
    var datadogExtra = {tags: tags};
    var score = relevancyScore / 4;

    dogapi.metric.send(
      'feed.relevancy',
      score,
      datadogExtra,
      dogapiMetricCallback
    );

    datadogExtra.type = "count";
    dogapi.metric.send(
      'feed.relevancy.count',
      relevancyScore,
      datadogExtra,
      dogapiMetricCallback
    );
  });

  describe('#BWD.Feed.get()', function() {
    var FEED_ITEMS_LENGTH = 100;
    it('should result in a success and give unique results', function() {
      var params = {
        range: FEED_ITEMS_LENGTH
      };
      return BWD.Feed.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Feed.get()');
          feed = results.data;
          var items = results.data;
          var uniqueItems = uniqBy(items, 'id');
          console.log('\nFeed results:\n');
          console.log(padEnd('Num', 4) + ' ' + padEnd('Content ID', 12) +
            padEnd('Type', 12) + ' ' + padEnd('Source', 20));
          console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') +
            padEnd('', 12, '-') + ' ' + padEnd('', 20, '-'));
          map(items, function(item, index) {
            console.log(padEnd(index, 4) + ' ' + padEnd(item.id, 12) +
              padEnd(item.type, 12) + ' ' + padEnd(item.source, 20));
          });
          console.log('\nSummary:\n');
          var itemsByType = countBy(items, function(item) {
            return item.type;
          });
          console.log('Types:');
          map(itemsByType, function(count, type) {
            console.log(' - ' + type + ': ' + count);
          });
          var itemsBySource = countBy(items, function(item) {
            return item.source;
          });
          console.log('\nSources:');
          map(itemsBySource, function(count, source) {
            console.log(' - ' + source + ': ' + count);
          });

          var jobs = feed
            .filter(function(item) {
              return item.type === 'job';
            });
          totalJobContent = jobs.length;

          var duplicates = pickBy(
            groupBy(jobs, function(item) {
              return item.id;
            }),
            function(x) {
              return x.length > 1;
            }
          );

          if (keys(duplicates).length) {
            console.log('\nDuplicates:');
            console.log(' - ' + keys(duplicates).length +
              ' duplicate entities\n');
            console.log(padEnd('Num', 4) + ' ' + padEnd('Content ID', 12) +
              ' ' + padEnd('Type', 12) + ' ' + padEnd('Source', 20));
            console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') + ' ' +
              padEnd('', 12, '-') + ' ' + padEnd('', 20, '-'));
            var index = 0;
            map(keys(duplicates), function(id) {
              map(duplicates[id], function(item) {
                console.log(padEnd(index, 4) + ' ' + padEnd(item.id, 12) + ' ' +
                padEnd(item.type, 12) + ' ' + padEnd(item.source, 20));
                index++;
              });
            });
          }

          console.log('\n');

          dogapi.metric.send("feed.result.count", items.length, {
            tags: [
              'user:' + process.env.BWD_USER_EMAIL,
              'env:' + BackendEnv
            ],
            type: 'count'
          }, dogapiMetricCallback);

          assert.strictEqual(items.length, FEED_ITEMS_LENGTH, 'got 100 items');
          assert.strictEqual(uniqueItems.length, FEED_ITEMS_LENGTH,
            'got 100 unique items');
        });
    });

    it('verify that articles and companies are included in the feed ' +
      'with a pattern using the Bashir sequence', function() {
      var i = 0;
      var jobType = true;
      var articleType = true;
      var countProcessedChunks = -1;
      var countArticlesAndCompanies = 0;

      var checkAgainst;
      if (feed.length < 25) {
        checkAgainst = feed.length;
      } else {
        checkAgainst = 25;
      }

      for (i; i < checkAgainst;) {
        if (jobType) {
          if (countProcessedChunks === 4) {
            countProcessedChunks = 0;
          } else {
            countProcessedChunks++;
          }

          var countJobs = fibonacci(countProcessedChunks);
          var jobs = feed.slice(i, i + countJobs);
          i += countJobs;

          assert.ok(every(jobs, function(job) {
            return job.type === 'job';
          }));
        } else {
          var items = feed.slice(i, i + 1);
          i++;
          if (countArticlesAndCompanies === 5) {
            countArticlesAndCompanies = 1;
            articleType = true;
          } else {
            countArticlesAndCompanies++;
          }

          if (articleType) {
            assert.ok(every(items, function(item) {
              return item.type === 'article';
            }));
          } else {
            assert.ok(every(items, function(item) {
              return item.type === 'company';
            }));
          }
          articleType = !articleType;
        }
        jobType = !jobType;
      }
    });

    it('verify that at least 10 articles load in the entire feed ', function() {
      var articles = feed.filter(function(item) {
        return item.type === 'article';
      });
      var totalArticleContent = articles.length;

      assert.ok(totalArticleContent >= 10,
      "Expected at least 10 articles in feed only " +
      totalArticleContent + " articles only found");
    });

    it('ensure feed does not contain passed items', function() {
      var PASSED_JOB_TYPE = 'pass_job,vote';
      var PASSED_ARTICLE_TYPE = 'passedarticle';
      var types = [
        PASSED_JOB_TYPE,
        PASSED_ARTICLE_TYPE
      ];
      var params = {
        'sort': '-createdDate',
        'type': types.join(),
        'filter[value][value]': 1,
        'filter[value][operator]': '!='
      };
      return BWD.Stats.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Stats.get()');
          var statsIds = results.data.map(function(stat) {
            return parseInt(stat.ref, 10);
          });

          // filter out curated items
          var feedItemIds = feed.filter(function(item) {
            return item.source !== CURATED_SOURCE;
          }).map(function(item) {
            return parseInt(item.id, 10);
          });
          var diff = difference(feedItemIds, statsIds);
          var inter = intersection(feedItemIds, statsIds);
          assert.equal(diff.length, feedItemIds.length,
            'feed does not contain passed items. passed items found in feed: ' +
            inter.join(', '));
        });
    });

    it('ensure feed contains at least 10 cip matches',
      function() {
        var jobFromCip = feed
          .filter(function(item) {
            return item.type === 'job' && item.source === 'cip';
          });
        assert.ok(jobFromCip.length >= 10,
          'ensure that at least 10 feed items are jobs with a \'cip\' source');
      }
    );

    it('ensure feed contains jobs with matching ONET', function() {
      var feedJobItemsIds = feed
        .filter(function(item) {
          return item.type === 'job' && item.source === 'cip';
        }).map(function(item) {
          return parseInt(item.id, 10);
        });

      var promises = [
        BWD.Users.getMe(),
        BWD.Jobs.getById(feedJobItemsIds)
      ];

      return Promise.all(promises)
        .then(function(results) {
          var user = results[0].data[0];
          var userFieldsOfStudy = user.fieldsOfStudy.filter(function(fos) {
            return fos !== null;
          });
          var userFieldsOfStudyOnetIds = [];
          userFieldsOfStudy.forEach(function(fos) {
            if (fos.onets) {
              fos.onets = fos.onets.map(function(onet) {
                return parseInt(onet, 10);
              });
              userFieldsOfStudyOnetIds =
                userFieldsOfStudyOnetIds.concat(fos.onets);
            }
          });

          console.log('\nUser Fields Of Study:\n');
          console.log(padEnd('Num', 4) + ' ' + padEnd('ID', 12) + ' ' +
            padEnd('Name', 30) + ' ' + padEnd('ONETs', 20));
          console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') + ' ' +
            padEnd('', 30, '-') + ' ' + padEnd('', 20, '-'));
          map(userFieldsOfStudy, function(fos, index) {
            console.log(padEnd(index, 4) + ' ' + padEnd(fos.id, 12) + ' ' +
              padEnd(fos.name, 30) + ' ' +
              padEnd(fos.onets ? fos.onets.join(', ') : '', 20));
          });

          var jobs = results[1].data;
          console.log('\nJobs:\n');
          console.log(padEnd('Num', 4) + ' ' + padEnd('ID', 12) +
            padEnd('ONETs', 12));
          console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') +
            padEnd('', 12, '-'));
          map(jobs, function(job, index) {
            console.log(padEnd(index, 4) + ' ' + padEnd(job.id, 12) +
              padEnd(job.onets ? job.onets.map(function(onet) {
                return onet.id;
              }).join(', ') : '', 12));
          });

          assert.ok(
            every(jobs, function(job) {
              var jobOnetIds = job.onets.map(function(onet) {
                return parseInt(onet.id, 10);
              });
              return intersection(jobOnetIds,
                userFieldsOfStudyOnetIds).length !== 0;
            }),
            'ensure that all jobs with the source = \'cip\' have an ONET ' +
            'that matches one of the ONETs associated with' +
            ' the user\'s field of study.'
          );
        });
    });

    it('ensure feed contains jobs with matching goal', function() {
      var feedJobItemsIds = feed
        .filter(function(item) {
          return item.type === 'job';
        }).map(function(item) {
          return parseInt(item.id, 10);
        });

      var promises = [
        BWD.Users.getMe(),
        BWD.Jobs.getById(feedJobItemsIds)
      ];

      return Promise.all(promises)
        .then(function(results) {
          var user = results[0].data[0];

          console.log('\nUser Goal:\n');
          console.log(padEnd('Num', 4) + ' ' + padEnd('ID', 12) + ' ' +
            padEnd('Name', 20));
          console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') + ' ' +
            padEnd('', 20, '-'));
          console.log(padEnd(1, 4) + ' ' + padEnd(user.id, 12) + ' ' +
            padEnd(user.goal, 20));

          var jobs = results[1].data;
          console.log('\nJobs:\n');
          console.log(padEnd('Num', 4) + ' ' + padEnd('ID', 12) +
            ' ' + padEnd('Goal', 12) + ' ' + padEnd('Internship', 12) +
            ' ' + padEnd('Min Experience', 12));
          console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') +
            ' ' + padEnd('', 12, '-') + ' ' + padEnd('', 12, '-') +
            ' ' + padEnd('', 12, '-'));
          map(jobs, function(job, index) {
            console.log(padEnd(index, 4) + ' ' + padEnd(job.id, 12) +
              ' ' + padEnd(job.goal, 12) + ' ' + padEnd(job.internship, 12) +
              ' ' + padEnd(job.minExperience || 'null', 12));
          });

          var matchCount = 0;

          // the curated job should be considered as a match
          if (user.goal === 'summerinternship') {
            each(jobs, function(job) {
              var jobSource = jobSources[job.id];
              if (jobSource === CURATED_SOURCE || job.internship) {
                matchCount++;
              }
            });
          } else {
            each(jobs, function(job) {
              var jobSource = jobSources[job.id];
              if (jobSource === CURATED_SOURCE ||
                (job.minExperience === null && !job.internship) ||
                (job.goal === user.goal && !job.internship)) {
                matchCount++;
              }
            });
          }

          console.log('\nTotal number of Jobs which match goal ' + matchCount);

          assert.ok(matchCount / totalJobContent >= .75,
            'Ensure that 75% of the jobs have a goal and internship flag ' +
            'that matches the user.');
        });
    });

    it('ensure feed contains jobs with matching location', function() {
      var feedJobItemsIds = feed
        .filter(function(item) {
          return item.type === 'job';
        }).map(function(item) {
          return parseInt(item.id, 10);
        });
      var promises = [
        BWD.Users.getMe(),
        BWD.Jobs.getById(feedJobItemsIds)
      ];

      return Promise.all(promises)
        .then(function(results) {
          var user = results[0].data[0];
          var preferredLocations = uniqBy(user.locations, 'id');
          var preferredLocationsIds = map(preferredLocations, 'id');

          var jobs = results[1].data;

          console.log('\nUser Preferred Locations:\n');
          console.log(padEnd('Num', 4) + ' ' + padEnd('Location ID', 12) +
            ' ' + padEnd('Location Name', 20));
          console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') +
            ' ' + padEnd('', 20, '-'));
          map(preferredLocations, function(location, index) {
            console.log(padEnd(index, 4) + ' ' + padEnd(location.id, 12) +
              ' ' + padEnd(location.name, 20));
          });

          return BWD.Locations.getById(
            preferredLocationsIds.join(),
            {expanded: true}
          )
            .then(function(results) {
              if (results.data && results.data.length) {
                results.data.forEach(function(location) {
                  if (location.expandedLocations &&
                    location.expandedLocations.length) {
                    preferredLocationsIds = preferredLocationsIds
                      .concat(location.expandedLocations);
                  }
                });
              }

              console.log('\nJobs:\n');
              console.log(padEnd('Num', 4) + ' ' + padEnd('ID', 12) +
                ' ' + padEnd('Location ID', 12) +
                ' ' + padEnd('Location Name', 20) +
                ' ' + padEnd('Location Invalid', 16));
              console.log(padEnd('', 4, '-') + ' ' + padEnd('', 12, '-') +
                ' ' + padEnd('', 12, '-') + ' ' + padEnd('', 20, '-') +
                ' ' + padEnd('', 16, '-'));
              var validJobs = [];
              map(jobs, function(job, index) {
                var jobSource = jobSources[job.id];

                // the curated job should be considered as a match
                var validJob = jobSource === CURATED_SOURCE ||
                  includes(preferredLocationsIds, job.location.id);
                validJobs.push(validJob);

                console.log(
                  padEnd(index, 4) + ' ' + padEnd(job.id, 12) + ' ' +
                  padEnd(job.location.id, 12) + ' ' +
                  padEnd(job.location.name, 20) + ' ' +
                  padEnd((validJob ? '' : 'x'), 16)
                );
              });

              assert.ok(
                every(validJobs),
                'ensure that all jobs have a location ID that matches' +
                ' one of the user\'s preferred locations.'
              );
            });
        });
    });
  });
});
