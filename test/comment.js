/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Comment.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'count': 50,
        'page[number]': '1'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/comment$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query,
          'query string parameters is valid');
        done();
      };
      BWD.Comment.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var cid = 1;
      var query = {
        count: 5
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/comment\/1$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.params, query, 'query options are valid');
        done();
      };
      BWD.Comment.getById(cid, query);
    });
  });

  describe('#createComment()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var endpoint = /\/discovery\/v1.0\/comment$/;
      var data = {
        title: 'Comment for Article',
        body: 'Excellent article.',
        nid: 25210082
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Comment.createComment(data);
    });
  });

  describe('#updateComment()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var endpoint = /\/discovery\/v1.0\/comment\/14$/;
      var commentId = 14;
      var data = {
        body: 'Excellent article, updated.'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'PATCH', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Comment.updateComment(commentId, data);
    });
  });
});
