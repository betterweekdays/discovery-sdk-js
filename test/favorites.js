/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Favorites.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'filter[type]': 'favoritejob'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/favorites$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Favorites.get(query);
    });
  });
});
