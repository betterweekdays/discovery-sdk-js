/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Messaging.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#getMessages()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'filter[msgType]': 'message',
        'fields': 'from,recipient,subject,body'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.1\/messages$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Messaging.getMessages(query);
    });
  });

  describe('#getMessage()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var messageId = 123456;
      var query = {
        fields: 'from,recipient,body,subject'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.1\/messages\/123456$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Messaging.getMessage(messageId, query);
    });
  });

  describe('#sendMessage()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var message = {
        recipient: '96',
        body: 'test message body'
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.1\/messages$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity, message, 'entity is valid');
        done();
      };
      BWD.Messaging.sendMessage(message);
    });
  });

  describe('#readMessages()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var messageIds = '1234,5678';
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/messages\/1234,5678\/read$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Messaging.readMessages(messageIds);
    });
  });

  describe('#isMessagesRead()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var messageId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/messages\/1234\/read$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Messaging.isMessagesRead(messageId);
    });
  });
});
