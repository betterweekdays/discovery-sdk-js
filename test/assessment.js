/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Assessment.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#getResultsByShareCode()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var shareCode = 'A8G';
      var path = /\/discovery\/v1.0\/user-assessments\/share-code\/A8G$/;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(path) !== -1,
          'path option is valid');
        done();
      };
      BWD.Assessment.getResultsByShareCode(shareCode);
    });
  });
});
