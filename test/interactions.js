/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Interactions.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        count: 50
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/interactions$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Interactions.get(query);
    });
  });

  describe('#setDone()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var interactionId = '9192674';

      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/interactions\/9192674$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity.action, 'done', 'entity is valid');
        done();
      };
      BWD.Interactions.setDone(interactionId);
    });
  });

  describe('#setNotDone()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var interactionId = '9192674';

      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/interactions\/9192674$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.headers['Content-Type'], 'application/json',
          'Content-Type is valid');
        assert.strictEqual(options.entity.action, 'not-done',
          'entity is valid');
        done();
      };
      BWD.Interactions.setNotDone(interactionId);
    });
  });
});
