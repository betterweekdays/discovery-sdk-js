/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/JobAreas.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'healthcare';
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/autocomplete\/jobarea$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params.title, title,
          'title query option is valid');
        done();
      };
      BWD.JobAreas.autocomplete(title);
    });
  });
});
