/* global before, beforeEach, afterEach, describe, it */

var assert = require('assert');
var lowerCase = require('lodash/lowerCase');
var dogapi = require('dogapi');
var clone = require('lodash/cloneDeep');

var BWD;
var APIEnv = process.env.BWD_API_ENV || 'prod';
var BackendEnv = process.env.BWD_API_BACKEND || 'stage';

var commonDatadogTags = [];

/**
 * Submit a new timing metric
 * @param {*} points a single data point (e.g. 50),
 * an array of data points (e.g. [50, 100]) or
 * an array of [timestamp, value] elements (e.g. [[now, 50], [now, 100]])
 * @param {*} extra optional object which can contain tags, metric type
 */
function sendTimingMetric(points, extra) {
  dogapi.metric.send(
    'discovery.sdk.performance',
    points,
    extra,
    function(err, results) {
      if (err) {
        console.log('datadog error');
        console.log(err);
      }
    }
  );
}

describe('e2e', function() {
  var ddTags = [];

  before(function() {
    if (BackendEnv) {
      commonDatadogTags.push('env:' + BackendEnv);
    }
    ddTags = clone(commonDatadogTags);
  });

  beforeEach(function() {
    if (!process.env.hasOwnProperty('BWD_API_KEY') ||
        !process.env.hasOwnProperty('BWD_API_SECRET')) {
      console.log('Skipping e2e tests because the API key or secret ' +
        'environment variable (BWD_API_KEY or BWD_API_SECRET) is missing.');
      this.skip();
    }

    if (!process.env.hasOwnProperty('BWD_DATADOG_API_KEY')) { // eslint-disable-line no-negated-condition
      console.log('datadog api or app key not set');
    } else {
      dogapi.initialize({
        api_key: process.env.BWD_DATADOG_API_KEY // eslint-disable-line camelcase
      });

      ddTags.push('user:' + process.env.BWD_USER_EMAIL);
    }

    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
    BWD.Config.set({
      APIKey: process.env.BWD_API_KEY,
      APISecret: process.env.BWD_API_SECRET,
      APIEnv: APIEnv,
      BackendEnv: BackendEnv
    });
    return BWD.Client.getAccessToken();
  });

  afterEach(function() {
    var tags = clone(commonDatadogTags);
    var timingExtra = {tags: tags};

    timingExtra.tags.push(
      'test:' + this.currentTest.title.split(' ').join('-'));
    timingExtra.tags.push(
      'status:' + this.currentTest.state
    );

    sendTimingMetric(this.currentTest.duration, timingExtra);
  });

  describe('BWD.Jobs', function() {
    it('#setFavorite()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.setFavorite(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Jobs.setFavorite()');
          done();
        });
    });

    it('#setPass()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.setPass(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Jobs.setPass()');
          done();
        });
    });

    it('#setInterestedInApplying()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.setInterestedInApplying(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Jobs.setInterestedInApplying()');
          done();
        });
    });

    it('#askForHelp()', function(done) {
      var params = {
        range: 10
      };
      var jobId = null;
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            jobId = results.data[0].id;
            var query = {
              message: "I'd like my resume reviewed for this"
            };
            return BWD.Jobs.askForHelp(jobId, query);
          }
        })
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Jobs.askForHelp()');
        })
        .then(function(results) {
          return BWD.Jobs.getAsksForHelp(jobId);
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.getAsksForHelp()');
          assert.ok(results.data.length &&
            parseInt(results.data[0].id, 10) === parseInt(jobId, 10),
            'stats were created.'
          );
          done();
        });
    });

    it('#setPreviewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.setPreviewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Jobs.setPreviewed()');
          done();
        });
    });

    it('#setViewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Jobs.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Jobs.setViewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Jobs.setViewed()');
          done();
        });
    });
  });

  describe('BWD.Companies', function() {
    it('#setFavorite()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.setFavorite(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Companies.setFavorite()');
          done();
        });
    });

    it('#askForHelp() and #getAsksForHelp()', function(done) {
      var params = {
        range: 10
      };
      var companyId = null;
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            companyId = results.data[0].id;
            var query = {
              message: "I'd like my resume reviewed for this"
            };
            return BWD.Companies.askForHelp(companyId, query);
          }
        })
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Companies.askForHelp()');
        })
        .then(function(results) {
          return BWD.Companies.getAsksForHelp(companyId);
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.getAsksForHelp()');
          assert.ok(results.data.length &&
            parseInt(results.data[0].id, 10) === parseInt(companyId, 10),
            'stats were created.'
          );
          done();
        });
    });

    it('#setPass()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.setPass(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Companies.setPass()');
          done();
        });
    });

    it('#setViewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.setViewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Companies.setViewed()');
          done();
        });
    });

    it('#setPreviewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.setPreviewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Companies.setPreviewed()');
          done();
        });
    });

    it('#follow()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Companies.follow(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Companies.follow()');
          done();
        });
    });

    it('#updateMatch()', function(done) {
      var params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            var data = {interest: 1};
            return BWD.Companies.updateMatch(results.data[0].id, data);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Companies.updateMatch()');
          done();
        });
    });
  });

  describe('BWD.Users', function() {
    it('#updateMe() and #update()', function(done) {
      var user = {
        firstName: 'mark',
        lastName: 'test'
      };
      BWD.Users.updateMe(user)
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Users.updateMe()');
        })
        .then(BWD.Users.getMe)
        .then(function(results) {
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            assert.equal(lowerCase(results.data[0].firstName),
              lowerCase(user.firstName),
              "user's first name set successfully");
            user.firstName += '2';
            return BWD.Users.update(results.data[0].id, user);
          }
        })
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Users.update()');
        })
        .then(BWD.Users.getMe)
        .then(function(results) {
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            assert.equal(lowerCase(results.data[0].firstName),
              lowerCase(user.firstName),
              "user's first name set successfully");
            done();
          }
        });
    });

    it('#recordLoginStats()', function(done) {
      BWD.Users.recordLoginStats()
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Users.recordLoginStats()');
          done();
        });
    });

    it('#recordLogoutStats()', function(done) {
      BWD.Users.recordLogoutStats()
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Users.recordLogoutStats()');
          done();
        });
    });
  });

  describe('BWD.Interactions', function() {
    it('#setDone()', function(done) {
      var params = {
        range: 10
      };
      BWD.Interactions.get(params)
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Interactions.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Interactions.setDone(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Interactions.setDone()');
          done();
        });
    });

    it('#setNotDone()', function(done) {
      var params = {
        range: 10
      };
      BWD.Interactions.get(params)
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Interactions.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Interactions.setNotDone(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Interactions.setNotDone()');
          done();
        });
    });
  });

  describe('BWD.Messaging', function() {
    it('#sendMessage()', function(done) {
      BWD.Users.get()
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Users.get()');
          if (results.data && results.data.length &&
            results.data[0].hasOwnProperty('id') && results.data[0].id) {
            var message = {
              recipient: results.data[0].id,
              body: 'test body message'
            };
            return BWD.Messaging.sendMessage(message);
          }
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Messaging.sendMessage()');
          done();
        });
    });

    it('#readMessages()', function(done) {
      var params = {
        range: 10
      };
      BWD.Messaging.getMessages(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Messaging.getMessages()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Messaging.readMessages(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
              'got successful response for BWD.Messaging.readMessages()');
          done();
        });
    });
  });

  describe('BWD.Articles', function() {
    it('#setFavorite()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.setFavorite(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
             'got successful response for BWD.Articles.setFavorite()');
          done();
        });
    });

    it('#askForHelp() and #getAsksForHelp()', function(done) {
      var params = {
        range: 10
      };
      var articleId = null;
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            articleId = results.data[0].id;
            var query = {
              message: "I'd like my resume reviewed for this"
            };
            return BWD.Articles.askForHelp(articleId, query);
          }
        })
        .then(function(results) {
          assert.ok(true,
            'got successful response for BWD.Articles.askForHelp()');
        })
        .then(function(results) {
          return BWD.Articles.getAsksForHelp(articleId);
        })
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getAsksForHelp()');
          assert.ok(results.data.length &&
            parseInt(results.data[0].id, 10) === parseInt(articleId, 10),
            'stats were created.'
          );
          done();
        });
    });

    it('#setPass()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.setPass(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
             'got successful response for BWD.Articles.setPass()');
          done();
        });
    });

    it('#setPreviewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.setPreviewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
             'got successful response for BWD.Articles.setPreviewed()');
          done();
        });
    });

    it('#setViewed()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.setViewed(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
             'got successful response for BWD.Articles.setViewed()');
          done();
        });
    });

    it('#setReadMore()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.setReadMore(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
             'got successful response for BWD.Articles.setReadMore()');
          done();
        });
    });

    it('#followCategories()', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.getCategories(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getCategories()');
          if (results.data && results.data.length &&
              results.data[0].hasOwnProperty('id') && results.data[0].id) {
            return BWD.Articles.followCategories(results.data[0].id);
          }
        })
        .then(function(results) {
          assert.ok(true,
             'got successful response for BWD.Articles.followCategories()');
          done();
        });
    });
  });
});
