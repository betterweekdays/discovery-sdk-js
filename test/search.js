/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Search.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#search()', function() {
    it('should invoke a request with appropriate content type and options',
      function(done) {
        var contentType = 'company';
        var query = {
          title: 'designer',
          jobarea: 152,
          internships: true
        };
        BWD.Client.request = function(options) {
          assert.ok(options.path
            .search(/\/discovery\/v1.0\/search\/company$/) !== -1,
            'path option is valid');
          assert.strictEqual(options.params,
            query, 'query string parameters is valid');
          done();
        };
        BWD.Search.search(contentType, query);
      }
    );
  });
});
