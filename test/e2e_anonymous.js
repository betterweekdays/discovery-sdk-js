/* global beforeEach, describe, it */

var assert = require('assert');

var BWD;

describe('e2e_anonymous_access', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();

    var token = BWD.Config.get('AccessToken');
    assert.equal(token, null, 'Authorization header is not set up');
  });

  describe('BWD.Jobs', function() {
    it('#getById() should result in a success', function(done) {
      var jobId = 38325648;
      BWD.Jobs.getById(jobId)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Jobs.getById()');
          done();
        });
    });

    it('#get() with companyId param should result in a success',
      function(done) {
        const params = {
          company: 1046
        };
        BWD.Jobs.get(params)
          .then(function(results) {
            assert.ok(results.hasOwnProperty('data'),
              'got successful response for BWD.Jobs.get()');
            done();
          });
      }
    );

    it('#isFavorite() should not result in a success', function(done) {
      var jobId = 38325648;
      BWD.Jobs.isFavorite(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.isFavorite()');
          done();
        });
    });

    it('#isPass() should not result in a success', function(done) {
      var jobId = 38325648;
      BWD.Jobs.isPass(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.isPass()');
          done();
        });
    });

    it('#isInterestedInApplying()', function(done) {
      var jobId = 38325648;
      BWD.Jobs.isInterestedInApplying(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.isInterestedInApplying()');
          done();
        });
    });

    it('#isPreviewed() should not result in a success', function(done) {
      var jobId = 38325648;
      BWD.Jobs.isPreviewed(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.isPreviewed()');
          done();
        });
    });

    it('#isViewed() should not result in a success', function(done) {
      var jobId = 38325648;
      BWD.Jobs.isViewed(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.isViewed()');
          done();
        });
    });

    it('#getAsksForHelp() should not result in a success', function(done) {
      var jobId = 38325648;
      BWD.Jobs.getAsksForHelp(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.getAsksForHelp()');
          done();
        });
    });

    it('#getInteractions()', function(done) {
      var jobId = 38325648;
      BWD.Jobs.getInteractions(jobId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.getInteractions()');
          done();
        });
    });

    // write
    it('#setFavorite() should not result in a success', function(done) {
      var id = 38325648;
      BWD.Jobs.setFavorite(id)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Jobs.setFavorite()');
          done();
        });
    });

    it('#setPass() should not result in a success', function(done) {
      var id = 38325648;
      BWD.Jobs.setPass(id)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Jobs.setPass()');
          done();
        });
    });

    it('#setInterestedInApplying() should not result in a success',
      function(done) {
        var id = 38325648;
        BWD.Jobs.setInterestedInApplying(id)
          .catch(function(error) {
            assert.ok(true,
              'got error response for BWD.Jobs.setInterestedInApplying()');
            done();
          });
      }
    );

    it('#askForHelp() should not result in a success', function(done) {
      var id = 38325648;
      var query = {
        message: "I'd like my resume reviewed for this"
      };
      BWD.Jobs.askForHelp(id, query)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.askForHelp()');
          done();
        });
    });

    it('#setPreviewed() should not result in a success', function(done) {
      var id = 38325648;
      BWD.Jobs.setPreviewed(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Jobs.setPreviewed()');
          done();
        });
    });

    it('#setViewed() should not result in a success', function(done) {
      var id = 38325648;
      BWD.Jobs.setViewed(id)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Jobs.setViewed()');
          done();
        });
    });
  });

  describe('BWD.Companies', function() {
    it('#get() should result in a success', function(done) {
      const params = {
        range: 10
      };
      BWD.Companies.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.get()');
          done();
        });
    });

    it('#getById() should result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.getById(companyId)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Companies.getById()');
          done();
        });
    });

    it('#isFavorite() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.isFavorite(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.isFavorite()');
          done();
        });
    });

    it('#isPass() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.isPass(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.isPass()');
          done();
        });
    });

    it('#isViewed() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.isViewed(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.isViewed()');
          done();
        });
    });

    it('#isPreviewed() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.isPreviewed(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.isPreviewed()');
          done();
        });
    });

    it('#autocomplete() should not result in a success', function(done) {
      var title = 'Nike';
      BWD.Companies.autocomplete(title)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.autocomplete()');
          done();
        });
    });

    it('#getAsksForHelp() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.getAsksForHelp(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.getAsksForHelp()');
          done();
        });
    });

    it('#isFollowing() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.isFollowing(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.isFollowing()');
          done();
        });
    });

    it('#getMatchStatus() should not result in a success', function(done) {
      var companyId = 370;
      BWD.Companies.getMatchStatus(companyId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.getMatchStatus()');
          done();
        });
    });

    // write
    it('#setFavorite() should not result in a success', function(done) {
      var id = 370;
      BWD.Companies.setFavorite(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.setFavorite()');
          done();
        });
    });

    it('#setPass() should not result in a success', function(done) {
      var id = 370;
      BWD.Companies.setPass(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.setPass()');
          done();
        });
    });

    it('#setViewed() should not result in a success', function(done) {
      var id = 370;
      BWD.Companies.setViewed(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.setViewed()');
          done();
        });
    });

    it('#setPreviewed() should not result in a success', function(done) {
      var id = 370;
      BWD.Companies.setPreviewed(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.setPreviewed()');
          done();
        });
    });

    it('#askForHelp() should not result in a success', function(done) {
      var id = 370;
      var query = {
        message: "I'd like my resume reviewed for this"
      };
      BWD.Companies.askForHelp(id, query)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.askForHelp()');
          done();
        });
    });

    it('#follow() should not result in a success', function(done) {
      var id = 370;
      BWD.Companies.follow(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.follow()');
          done();
        });
    });

    it('#updateMatch() should not result in a success', function(done) {
      var id = 370;
      var data = {interest: -1};
      BWD.Companies.updateMatch(id, data)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Companies.updateMatch()');
          done();
        });
    });
  });

  describe('BWD.Articles', function() {
    it('#get() should result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Articles.get(params)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.get()');
          done();
        });
    });

    it('#getById() should result in a success', function(done) {
      var articleId = 65684;
      BWD.Articles.getById(articleId)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Articles.getById()');
          done();
        });
    });

    it('#isFavorite() should not result in a success', function(done) {
      var articleId = 65684;
      BWD.Articles.isFavorite(articleId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.isFavorite()');
          done();
        });
    });

    it('#isPass() should not result in a success', function(done) {
      var articleId = 65684;
      BWD.Articles.isPass(articleId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.isPass()');
          done();
        });
    });

    it('#isPreviewed() should not result in a success', function(done) {
      var articleId = 65684;
      BWD.Articles.isPreviewed(articleId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.isPreviewed()');
          done();
        });
    });

    it('#isViewed() should not result in a success', function(done) {
      var articleId = 65684;
      BWD.Articles.isViewed(articleId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.isViewed()');
          done();
        });
    });

    it('#getAsksForHelp() should not result in a success', function(done) {
      var articleId = 65684;
      BWD.Articles.getAsksForHelp(articleId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.getAsksForHelp()');
          done();
        });
    });

    // write
    it('#setFavorite() should not result in a success', function(done) {
      var id = 15256068;
      BWD.Articles.setFavorite(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.setFavorite()');
          done();
        });
    });

    it('#setPass() should not result in a success', function(done) {
      var id = 15256068;
      BWD.Articles.setPass(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.setPass()');
          done();
        });
    });

    it('#setPreviewed() should not result in a success', function(done) {
      var id = 15256068;
      BWD.Articles.setPreviewed(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.setPreviewed()');
          done();
        });
    });

    it('#setViewed() should not result in a success', function(done) {
      var id = 15256068;
      BWD.Articles.setViewed(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.setViewed()');
          done();
        });
    });

    it('#setReadMore() should not result in a success', function(done) {
      var id = 15256068;
      BWD.Articles.setReadMore(id)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.setReadMore()');
          done();
        });
    });

    it('#askForHelp() should not result in a success', function(done) {
      var id = 15256068;
      var query = {
        message: "I'd like my resume reviewed for this"
      };
      BWD.Articles.askForHelp(id, query)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Articles.askForHelp()');
          done();
        });
    });
  });

  describe('BWD.Glassdoor', function() {
    it('#getCompanyInfo() should result in a success', function(done) {
      var params = {
        companyId: 369
      };
      var headers = {
        'User-Agent': 'rest/1.3.2'
      };
      BWD.Glassdoor.getCompanyInfo(params, headers)
        .then(function(results) {
          assert.ok(results.hasOwnProperty('data'),
            'got successful response for BWD.Glassdoor.getCompanyInfo()');
          assert.equal(results.data[0].status, 'COMPLETED',
            'data contain necessary info from Glassdoor');
          done();
        });
    });
  });

  describe('BWD.Users', function() {
    it('#getMe() should not result in a success', function(done) {
      BWD.Users.getMe()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Users.getMe()');
          done();
        });
    });

    it('#get() should not result in a success', function(done) {
      BWD.Users.get()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Users.get()');
          done();
        });
    });

    // write
    it('#updateMe() should not result in a success', function(done) {
      var data = {
        firstName: 'Mark'
      };
      BWD.Users.updateMe(data)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Users.updateMe()');
          done();
        });
    });

    it('#update() should not result in a success', function(done) {
      var userId = 5871;
      var data = {
        firstName: 'Mark'
      };
      BWD.Users.update(userId, data)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Users.update()');
          done();
        });
    });

    it('#recordLoginStats() should not result in a success', function(done) {
      BWD.Users.recordLoginStats()
        .catch(function(error) {
          assert.ok(true,
              'got successful response for BWD.Users.recordLoginStats()');
          done();
        });
    });

    it('#recordLogoutStats() should not result in a success', function(done) {
      BWD.Users.recordLogoutStats()
        .catch(function(error) {
          assert.ok(true,
              'got successful response for BWD.Users.recordLogoutStats()');
          done();
        });
    });
  });

  describe('BWD.Stats', function() {
    it('#get() should not result in a success', function(done) {
      const params = {
        type: ['favoritejob', 'favoritearticle']
      };
      BWD.Stats.get(params)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Stats.get()');
          done();
        });
    });

    it('#getSummary() should not result in a success', function(done) {
      var params = {
        from: 0,
        to: Date.now()
      };
      BWD.Stats.getSummary(params)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Stats.getSummary()');
          done();
        });
    });
  });

  describe('BWD.Feed', function() {
    it('#get() should not result in a success', function(done) {
      BWD.Feed.get()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Feed.get()');
          done();
        });
    });

    it('#getArticles() should not result in a success', function(done) {
      BWD.Feed.getArticles()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Feed.getArticles()');
          done();
        });
    });

    it('#getJobs() should not result in a success', function(done) {
      BWD.Feed.getJobs()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Feed.getJobs()');
          done();
        });
    });

    it('#getCompanies() should not result in a success', function(done) {
      BWD.Feed.getCompanies()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Feed.getCompanies()');
          done();
        });
    });
  });

  describe('BWD.Interactions', function() {
    it('#get() should not result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Interactions.get(params)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Interactions.get()');
          done();
        });
    });

    // write
    it('#setDone() should not result in a success', function(done) {
      var interactionId = 19699677;
      BWD.Interactions.setDone(interactionId)
        .catch(function(error) {
          assert.ok(true,
              'got error response for BWD.Interactions.setDone()');
          done();
        });
    });

    it('#setNotDone() should not result in a success', function(done) {
      var interactionId = 19699677;
      BWD.Interactions.setNotDone(interactionId)
        .catch(function(error) {
          assert.ok(true,
              'got error response for BWD.Interactions.setNotDone()');
          done();
        });
    });
  });

  describe('BWD.Messaging', function() {
    it('#getMessages() should not result in a success', function(done) {
      var params = {
        'filter[msgType]': 'message'
      };
      BWD.Messaging.getMessages(params)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Messaging.getMessages()');
          done();
        });
    });

    it('#getMessage() should not result in a success', function(done) {
      var messageId = 19701155;
      BWD.Messaging.getMessage(messageId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Messaging.getMessage()');
          done();
        });
    });

    it('#isMessagesRead()', function(done) {
      var messageId = 19701155;
      BWD.Messaging.isMessagesRead(messageId)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Messaging.isMessagesRead()');
          done();
        });
    });

    // write
    it('#sendMessage() should not result in a success', function(done) {
      var userId = 5871;
      var message = {
        recipient: userId,
        body: 'test body message'
      };
      BWD.Messaging.sendMessage(message)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Messaging.sendMessage()');
          done();
        });
    });

    it('#readMessages() should not result in a success', function(done) {
      var messageId = 19701155;
      BWD.Messaging.readMessages(messageId)
        .catch(function(results) {
          assert.ok(true,
            'got error response for BWD.Messaging.readMessages()');
          done();
        });
    });
  });

  describe('BWD.Search', function() {
    it('#search() should not result in a success', function(done) {
      var contentType = 'job';
      var query = {
        title: 'Healthcare',
        jobarea: 152,
        internships: false
      };
      BWD.Search.search(contentType, query)
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Search.search()');
          done();
        });
    });
  });

  describe('BWD.Locations', function() {
    it('#get() should not result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Locations.get(params)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Locations.get()');
          done();
        });
    });

    it('#autocomplete() should not result in a success', function(done) {
      var title = 'St. Louis';
      BWD.Locations.autocomplete(title)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Locations.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Cips', function() {
    it('#get() should not result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Cips.get(params)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Cips.get()');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'Food';
      BWD.Cips.autocomplete(title)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Cips.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Onets', function() {
    it('#get() should not result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Onets.get(params)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Onets.get()');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'Health';
      BWD.Onets.autocomplete(title)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Onets.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Orgs', function() {
    it('#get() should not result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Orgs.get(params)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Orgs.get()');
          done();
        });
    });
  });

  describe('BWD.Courses', function() {
    it('#get() should not result in a success', function(done) {
      var params = {
        range: 10
      };
      BWD.Courses.get(params)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Courses.get()');
          done();
        });
    });

    it('#autocomplete()', function(done) {
      var title = 'Web';
      BWD.Courses.autocomplete(title)
        .catch(function(error) {
          assert.ok(true,
            'got error response for BWD.Courses.autocomplete()');
          done();
        });
    });
  });

  describe('BWD.Advice', function() {
    it('#get() should not result in a success', function(done) {
      BWD.Advice.get()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Advice.get()');
          done();
        });
    });
  });

  describe('BWD.Matches', function() {
    it('#getCompanies() should not result in a success', function(done) {
      BWD.Matches.getCompanies()
        .catch(function(error) {
          assert.ok(true, 'got error response for BWD.Matches.getCompanies()');
          done();
        });
    });
  });
});
