/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Schools.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'filter[name]': 'Morehouse College'
      };

      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/schools$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Schools.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var schoolId = 60281;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/schools\/60281$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Schools.getById(schoolId);
    });
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'university';
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.0\/autocomplete\/school$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params.title, title,
          'title query option is valid');
        done();
      };
      BWD.Schools.autocomplete(title);
    });
  });
});
