/* global beforeEach, describe, it */

var assert = require('assert');

var BWD;

describe('services/Invite.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#inviteEmail()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/invite\/email$/;
      const data = {
        mail: 'invite_mail001@betterweekdays.com',
        fullName: 'Testing User'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Invite.inviteEmail(data);
    });
  });

  describe('#inviteSms()', function() {
    it('should invoke a request with appropriate options', function(done) {
      const endpoint = /\/discovery\/v1.0\/invite\/sms$/;
      const data = {
        phone: '+16175551227',
        fullName: 'Testing Invite Phone User'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Invite.inviteSms(data);
    });
  });
});
