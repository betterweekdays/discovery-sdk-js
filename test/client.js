/* eslint camelcase: ["error", {properties: "never"}] */
/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Client.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#authInterceptor()', function() {
    it('should add auth header if id token is set', function() {
      BWD.Config.set({
        AccessToken: {
          id_token: 'abcd1234.abcd1234.abcd1234'
        }
      });
      var request = {};
      request = BWD.authInterceptor(request);
      assert.equal(request.headers.Authorization,
        'Bearer JWT abcd1234.abcd1234.abcd1234',
        'auth interceptor adds id token');
    });

    it('should add auth header if access token is set', function() {
      BWD.Config.set({
        AccessToken: {
          access_token: 'abcd1234'
        }
      });
      var request = {};
      request = BWD.authInterceptor(request);
      assert.equal(request.headers.Authorization,
        'Bearer abcd1234',
        'auth interceptor adds access token');
    });

    it('should leave auth headers if they already are set', function() {
      BWD.Config.set({
        AccessToken: {
          id_token: 'abcd1234.abcd1234.abcd1234'
        }
      });
      var request = {
        headers: {
          Authorization: 'custom access token'
        }
      };
      request = BWD.authInterceptor(request);
      assert.equal(request.headers.Authorization, 'custom access token',
        'auth interceptor preserves existing access token');
    });

    it('should do nothing if the token is not set', function() {
      var request = {
        headers: {}
      };
      request = BWD.authInterceptor(request);
      assert.ok(!request.headers.Authorization,
        'auth interceptor does nothing without an access token');
    });
  });

  describe('#buildUrl()', function() {
    it('should construct an https URL', function() {
      BWD.Config.set({
        APIHost: 'host',
        UseSSL: true
      });
      assert.deepStrictEqual(BWD.Client.buildUrl('test'),
        'https://host/' + BWD.Config.get('APIBaseUrl') + '/test',
        'valid https URL generated');
    });

    it('should construct an http URL', function() {
      BWD.Config.set({
        APIHost: 'host',
        UseSSL: false
      });
      assert.deepStrictEqual(BWD.Client.buildUrl('test'),
        'http://host/' + BWD.Config.get('APIBaseUrl') + '/test',
        'valid http URL generated');
    });
  });

  describe('#buildOAuthUrl()', function() {
    it('should construct an https URL', function() {
      BWD.Config.set({
        APIHost: 'host',
        UseSSL: true
      });
      assert.deepStrictEqual(BWD.Client.buildOAuthUrl('test'), 'https://host/oauth/test',
        'valid https URL generated');
    });

    it('should construct an http URL', function() {
      BWD.Config.set({
        APIHost: 'host',
        UseSSL: false
      });
      assert.deepStrictEqual(BWD.Client.buildOAuthUrl('test'), 'http://host/oauth/test',
        'valid http URL generated');
    });
  });

  describe('#getAccessToken()', function() {
    it('should request an access token with appropriate config',
      function(done) {
        BWD.Config.set({
          APIKey: 'test1234',
          APISecret: 'AlphaBetaGamma'
        });
        BWD.Client.request = function(options) {
          assert.ok(options.path.search(/\/oauth\/token$/),
            'path option is valid');
          var base64 = require('rest/util/base64');
          var headerAuth = 'Basic ' + base64.encode('test1234:AlphaBetaGamma');
          assert.strictEqual(options.headers.Authorization, headerAuth,
            'authorization header is valid');
          var responsePromise = require('rest/util/responsePromise');
          return responsePromise.promise(function(resolve) {
            resolve({token: 'test'});
          });
        };
        BWD.Client.getAccessToken()
          .then(function(result) {
            assert.deepEqual(result, {token: 'test'}, 'token is returned');
            done();
          });
      });
  });

  describe('#getLogin()', function() {
    it('should return a URL to the login page', function() {
      BWD.Config.set({
        APIHost: 'host',
        APIKey: 'key'
      });
      assert.deepStrictEqual(BWD.Client.getLogin(), 'https://host/oauth/login?apiKey=key',
        'login URL generated');
    });
  });

  describe('#getLogout()', function() {
    it('should return a URL to the logout page', function() {
      BWD.Config.set({
        APIHost: 'host'
      });
      assert.deepStrictEqual(BWD.Client.getLogout(), 'https://host/oauth/logout',
        'logout URL generated');
    });
  });

  describe('#isTokenExpired()', function() {
    it('should report for valid token', function() {
      BWD.Config.set({
        Auth0TokenExpires: new Date(Date.now() + 1000000)
      });
      assert(!BWD.isTokenExpired(), 'valid token is reported as valid');
    });

    it('should for expired token', function() {
      BWD.Config.set({
        Auth0TokenExpires: new Date()
      });
      assert(BWD.isTokenExpired(), 'expired token is reported as expired');
    });
  });

  describe('#request()', function() {
    it('should execute a successful request using mocked rest', function() {
      var options = {
        path: 'path/test',
        params: {
          alpha: 'beta'
        }
      };
      var responseData = {
        alpha: 'beta'
      };
      BWD.rest = function(_options) {
        assert.deepEqual(_options, options,
          'options are passed to the rest module');
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'application/json'
            },
            entity: responseData
          };
          resolve(response);
        });
      };
      return BWD.Client.request(options)
        .then(function(result) {
          assert.deepEqual(result, responseData, 'response data received');
        });
    });

    it('should execute to the correct backend using mocked rest', function() {
      var options = {
        path: 'path/test',
        params: {
          alpha: 'beta'
        }
      };
      var responseData = {
        alpha: 'beta'
      };
      BWD.Config.set('BackendEnv', 'stage');
      BWD.rest = function(_options) {
        options.headers = {
          Backend: 'stage'
        };
        assert.deepEqual(_options, options,
          'options are passed to the rest module');
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'application/json'
            },
            entity: responseData
          };
          resolve(response);
        });
      };
      return BWD.Client.request(options)
        .then(function(result) {
          assert.deepEqual(result, responseData, 'response data received');
        });
    });

    it('should receive non-JSON data using mocked rest', function() {
      var options = {
        path: 'path/test'
      };
      var responseData = 'test data response';
      BWD.rest = function(/* _options */) {
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'text/plain'
            },
            entity: responseData
          };
          resolve(response);
        });
      };
      return BWD.Client.request(options)
        .then(function(result) {
          assert.deepEqual(result, responseData, 'response data received');
        });
    });

    it('should report invalid JSON response using mocked rest', function() {
      var options = {
        path: 'path/test',
        params: {
          alpha: 'beta'
        }
      };
      var responseData = 'invalidjsondata';
      BWD.rest = function(/* _options */) {
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {
            status: {
              code: 200
            },
            headers: {
              'Content-Type': 'application/json'
            },
            entity: responseData
          };
          resolve(response);
        });
      };
      return BWD.Client.request(options)
        .catch(function(error) {
          assert.equal(error.message, 'Failed to parse JSON response.',
            'invalid JSON data reported');
        });
    });

    it('should report a failed request using mocked rest', function(done) {
      var options = {
        path: 'path/test'
      };
      BWD.rest = function(/* _options */) {
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve, reject) {
          var response = {
            entity: {
              detail: 'Unauthorized',
              status: 401,
              title: 'Bad credentials. ' +
                'Anonymous user resolved for a resource ' +
                'that requires authentication.'
            },
            status: {
              code: 401
            },
            request: {
              method: 'GET',
              path: 'path/test'
            }
          };
          reject(response.entity);
        });
      };
      BWD.Client.request(options)
        .catch(function(error) {
          assert.equal(error.status, 401, 'error response reported');
          done();
        });
    });

    it('should report a malformed request using mocked rest', function(done) {
      var options = {
        path: 'path/test'
      };
      BWD.rest = function(/* _options */) {
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {};
          resolve(response);
        });
      };
      BWD.Client.request(options)
        .catch(function(error) {
          assert.equal(error.message, 'Malformed request result.',
            'malformed response reported');
          done();
        });
    });

    it('should attempt a token refresh if it is expired', function(done) {
      var options = {
        path: 'path/test'
      };
      BWD.Config.set({
        Auth0TokenExpires: new Date()
      });
      BWD.Auth.refreshToken = function() {
        return new Promise(function(resolve, reject) {
          assert(true, 'token refresh is attempted due to expired token');
          BWD.Config.set({
            Auth0TokenExpires: new Date(Date.now() + 1000000)
          });
          resolve();
        });
      };
      BWD.rest = function(_options) {
        assert.deepEqual(_options, options, 'request after token refresh');
        var responsePromise = require('rest/util/responsePromise');
        return responsePromise.promise(function(resolve) {
          var response = {status: {code: 200}};
          resolve(response);
        });
      };
      BWD.Client.request(options)
        .then(function(result) {
          done();
        });
    });
  });
});
