/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Onets.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        fields: 'name,lead'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.0\/onets/) !== -1,
            'path option is valid');
        assert.strictEqual(options.params, query, 'query option is valid');
        done();
      };
      BWD.Onets.get(query, BWD.Client.request);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var onetId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(options.path
                .search(/\/discovery\/v1.0\/onets\/1234/) !== -1,
            'path option is valid');
        done();
      };
      BWD.Onets.getById(onetId);
    });
  });

  describe('#autocomplete()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var title = 'St. Louis';
      BWD.Client.request = function(options) {
        assert.ok(options.path
                .search(/\/discovery\/v1.0\/autocomplete\/onet/) !== -1,
            'path option is valid');
        assert.strictEqual(options.params.title, title,
            'title query option is valid');
        done();
      };
      BWD.Onets.autocomplete(title);
    });
  });
});
