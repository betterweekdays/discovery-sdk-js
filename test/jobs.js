/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Jobs.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#get()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var query = {
        'count': 50,
        'filter[id][value]': '123456'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(/\/discovery\/v1.1\/jobs$/) !== -1,
          'path option is valid');
        assert.strictEqual(options.params, query,
          'query string parameters is valid');
        done();
      };
      BWD.Jobs.get(query);
    });
  });

  describe('#getById()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 123456;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.1\/jobs\/123456$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Jobs.getById(jobId);
    });
  });

  describe('#setFavorite()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/favorite$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Jobs.setFavorite(jobId, data);
    });
  });

  describe('#isFavorite()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/favorite$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Jobs.isFavorite(jobId);
    });
  });

  describe('#setPass()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.1\/jobs\/1234\/pass$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Jobs.setPass(jobId, data);
    });
  });

  describe('#isPass()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path.search(/\/discovery\/v1.1\/jobs\/1234\/pass$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Jobs.isPass(jobId);
    });
  });

  describe('#setInterestedInApplying()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
          .search(/\/discovery\/v1.1\/jobs\/1234\/interested-in-apply$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Jobs.setInterestedInApplying(jobId, data);
    });
  });

  describe('#isInterestedInApplying()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
          .search(/\/discovery\/v1.1\/jobs\/1234\/interested-in-apply$/) !== -1,
          'path option is valid'
        );
        done();
      };
      BWD.Jobs.isInterestedInApplying(jobId);
    });
  });

  describe('#setRecommendJob()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/recommend-job$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Jobs.setRecommendJob(jobId);
    });
  });

  describe('#isRecommendJob()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/recommend-job$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Jobs.isRecommendJob(jobId);
    });
  });

  describe('#setPreviewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/previewed$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Jobs.setPreviewed(jobId, data);
    });
  });

  describe('#isPreviewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/previewed$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Jobs.isPreviewed(jobId);
    });
  });

  describe('#setViewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      var data = {
        source: 'e2e'
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/viewed$/) !== -1,
            'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Jobs.setViewed(jobId, data);
    });
  });

  describe('#isViewed()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/viewed$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Jobs.isViewed(jobId);
    });
  });

  describe('#getInteractions()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 123456;
      BWD.Client.request = function(options) {
        assert.ok(options.path
          .search(/\/discovery\/v1.1\/jobs\/123456\/todos$/) !== -1,
          'path option is valid');
        done();
      };
      BWD.Jobs.getInteractions(jobId);
    });
  });

  describe('#askForHelp()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      var data = {
        message: "I'd like mentorship on this"
      };
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/ask$/) !== -1,
          'path option is valid'
        );
        assert.strictEqual(options.entity, data,
          'entity parameters is valid');
        done();
      };
      BWD.Jobs.askForHelp(jobId, data);
    });
  });

  describe('#getAsksForHelp()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var jobId = 1234;
      BWD.Client.request = function(options) {
        assert.ok(
          options.path
            .search(/\/discovery\/v1.1\/jobs\/1234\/ask$/) !== -1,
            'path option is valid'
        );
        done();
      };
      BWD.Jobs.getAsksForHelp(jobId);
    });
  });
});
