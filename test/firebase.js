/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('services/Firebase.js', function() {
  beforeEach(function() {
    // Un-cache the SDK module to reset overridden functions.
    delete require.cache[require.resolve('../src/sdk')];
    BWD = require('../src/sdk');
    BWD.Config.reset();
  });

  describe('#subscribe()', function() {
    it('should invoke a request with appropriate options', function(done) {
      var endpoint = /\/discovery\/v1.0\/firebase\/subscribe$/;
      var data = {
        token: '128a62d491aa2b92',
        topic: 'app-content'
      };
      BWD.Client.request = function(options) {
        assert.ok(options.path.search(endpoint) !== -1, 'path option is valid');
        assert.strictEqual(options.method, 'POST', 'method option is valid');
        assert.strictEqual(options.entity, data, 'entity is valid');
        done();
      };
      BWD.Firebase.subscribe(data);
    });
  });
});
