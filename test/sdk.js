/* global beforeEach, describe, it */

var assert = require("assert");

var BWD;

describe('sdk.js', function() {
  describe('#constructor()', function() {
    beforeEach(function() {
      // Un-cache the SDK module to reset overridden functions.
      delete require.cache[require.resolve('../src/sdk')];
      BWD = require('../src/sdk');
      BWD.Config.reset();
    });

    it('should set and load valid config options', function() {
      var val = 'test';
      BWD = require('../src/sdk')({
        APIHost: val
      });
      assert.deepStrictEqual(BWD.Config.get('APIHost'), val,
        'config is saved and loaded by constructor');
    });
  });
});
