# Better Weekdays Discovery SDK for JavaScript

## Using in browser

In browser, access the API using the global `window.BWD`.

## Using in Node.js

For Node.js, load this module using:
    `var BWD = require('@betterweekdays/discovery-sdk-js');`

## Method reference

### BWD(_config_)

When this module is loaded, it provides access to components and their methods,
such as `BWD.Jobs` and `BWD.Jobs.get()`, and it is a function that can be
invoked with configuration to set (equivalent to using `BWD.Config.set()`).

Example usage:

```
var BWD = require('discovery-sdk')({
  APIEnv: 'prod',
  APIKey: 'abcd1234',
  APISecret: 'secret1234',
  BackendEnv: 'stage'
});
```

This is equivalent:

```
var BWD = require('discovery-sdk');
BWD.Config.set({
  APIEnv: 'prod',
  APIKey: 'abcd1234',
  APISecret: 'secret1234',
  BackendEnv: 'stage'
});
```

### BWD.Client.getAccessToken()

`BWD.Client.getAccessToken()` returns a promise that is resolved when an access
token has been obtained (or rejected when an error occurs or a token cannot be
obtained).

This method should be invoked before methods that require an access token, such
as `BWD.Jobs.get()`.

Example usage:

```
BWD.Client.getAccessToken()
  .then(function() {
    // Use methods that require an access token...
  });
```

### BWD.Client.getLogin()

`BWD.Client.getLogin()` returns the URL to which the user should be redirected
to begin the user sign in process. Typically this URL redirects to a login form
with the option to login through popular social providers.

Applications using this sign in process will need to implement a method for
catching the access token provided when the user is returned to the application
via a configured callback URL.

Prior to using this method, `BWD.Config.set()` should be used to set an API key
and environment. An API secret is not required for this method.

Example usage:

```
window.location = BWD.Client.getLogin();
```

### BWD.Client.getLogout()

`BWD.Client.getLogout()` returns the URL to which the user should be redirected
to begin the user sign out process. Typically the sign out process results in
the user being redirected back to the initiating application or the Better
Weekdays homepage.

Example usage:

```
window.location = BWD.Client.getLogout();
```

### BWD.Companies.get(_query_)

`BWD.Companies.get()` allows retrieving company profiles. It returns a promise
that is resolved when the API call returns successfully or is rejected if any
error is encountered.

An optional query argument may be given with any parameters supported by the
Discovery API.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var query = {
  count: 100,
  page: 2
};
BWD.Companies.get(query)
  .then(function(results) {
    // The list of companies is the array at results.data
  });
```

### BWD.Companies.getById(_companyId_)

`BWD.Companies.getById()` allows retrieving a single company profile by its ID.
It returns a promise that is resolved when the API call returns successfully or
is rejected if any error is encountered.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var companyId = 123456;
BWD.Companies.getById(companyId)
  .then(function(results) {
    // The job data is the first item in the array at results.data
  });
```

### BWD.Companies.setFavorite(_companyId_)

`BWD.Companies.setFavorite()` allows registering a company as favorite by its ID. 
It returns a promise that is resolved when the API call returns successfully 
or is rejected if any error is encountered. The stat is recorded asynchronously, 
so the return value indicates that it has been queued, but not saved.

The method requires a single argument specifying the company ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var companyId = 12;
BWD.Companies.setFavorite(companyId)
  .then(function(results) {
    // indicates that it has been queued
  });
```

### BWD.Companies.isFavorite(_companyId_)

`BWD.Companies.isFavorite()` allows to find out whether the company has been marked 
as favorite by its ID or not. It returns a promise that is resolved 
when the API call returns successfully or is rejected if any error is encountered.
The return value contains company favorite status in `stat` field of response entity.

The method requires a single argument specifying the company ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

The response entity looks like:

```
{
  "data": {
    "stat": false
  },
  "self": {
    "title": "Self",
    "href": "http://local.app.betterweekdays.com/api/v1.0/companies/12/favorite-company"
  }
}
```

Example usage:

```
var companyId = 12;
BWD.Companies.isFavorite(companyId)
  .then(function(results) {
    // The favorite info is the stat property of results.data
  });
```

### BWD.Config.set(_config_)

`BWD.Config.set()` allows setting configuration values. Allowed configuration
keys can be found in the `BWD.DefaultConfig` object defined in `src/sdk.js`.
Typically, applications using this SDK will use this method to set the API key,
secret, and environment.

Example usage:

```
BWD.Config.set({
  APIEnv: 'prod',
  APIKey: 'abcd1234',
  APISecret: 'secret1234'
});
```

### BWD.Jobs.get(_query_)

`BWD.Jobs.get()` allows retrieving job postings. It returns a promise that is
resolved when the API call returns successfully or is rejected if any error is
encountered.

An optional query argument may be given with any parameters supported by the
Discovery API.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var query = {
  count: 100,
  page: 2
};
BWD.Jobs.get(query)
  .then(function(results) {
    // The list of jobs is the array at results.data
  });
```

### BWD.Jobs.getById(_jobId_)

`BWD.Jobs.getById()` allows retrieving a single job posting by its ID. It
returns a promise that is resolved when the API call returns successfully or is
rejected if any error is encountered.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var jobId = 123456;
BWD.Jobs.getById(jobId)
  .then(function(results) {
    // The job data is the first item in the array at results.data
  });
```

### BWD.Jobs.setFavorite(_jobId_)

`BWD.Jobs.setFavorite()` allows registering a job as favorite by its ID. 
It returns a promise that is resolved when the API call returns successfully 
or is rejected if any error is encountered. The stat is recorded asynchronously, 
so the return value indicates that it has been queued, but not saved.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var jobId = 12;
BWD.Jobs.setFavorite(jobId)
  .then(function(results) {
    // indicates that it has been queued
  });
```

### BWD.Jobs.isFavorite(_jobId_)

`BWD.Jobs.isFavorite()` allows to find out whether the job has been marked 
as favorite by its ID or not. It returns a promise that is resolved 
when the API call returns successfully or is rejected if any error is encountered.
The return value contains job favorite status in `stat` field of response entity.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

The response entity looks like:

```
{
  "data": {
    "stat": false
  },
  "self": {
    "title": "Self",
    "href": "http://local.app.betterweekdays.com/api/v1.0/job/12/favorite-job"
  }
}
```

Example usage:

```
var jobId = 12;
BWD.Jobs.isFavorite(jobId)
  .then(function(results) {
    // The favorite info is the stat property of results.data
  });
```

### BWD.Jobs.setPass(_jobId_)

`BWD.Jobs.setPass()` allows registering a job as passed by its ID. 
It returns a promise that is resolved when the API call returns successfully 
or is rejected if any error is encountered. The stat is recorded asynchronously, 
so the return value indicates that it has been queued, but not saved.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var jobId = 12;
BWD.Jobs.setPass(jobId)
  .then(function(results) {
    // indicates that it has been queued
  });
```

### BWD.Jobs.isPass(_jobId_)

`BWD.Jobs.isPass()` allows to find out whether the job has been marked 
as passed by its ID or not. It returns a promise that is resolved 
when the API call returns successfully or is rejected if any error is encountered.
The return value contains job pass status in `vote` field of response entity.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

The response entity looks like:

```
{
  "data": {
    "vote": "0"
  },
  "self": {
    "title": "Self",
    "href": "http://local.app.betterweekdays.com/api/v1.0/job/12/pass"
  }
}
```

Example usage:

```
var jobId = 12;
BWD.Jobs.isPass(jobId)
  .then(function(results) {
    // The pass stats is the stat property of results.data
  });
```

### BWD.Jobs.setInterestedInApplying(_jobId_)

`BWD.Jobs.setInterestedInApplying()` allows registering "Interested in Applying" 
action for the job by its ID. It returns a promise that is resolved 
when the API call returns successfully or is rejected if any error 
is encountered. The stat is recorded asynchronously, 
so the return value indicates that it has been queued, but not saved.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var jobId = 12;
BWD.Jobs.setInterestedInApplying(jobId)
  .then(function(results) {
    // indicates that it has been queued
  });
```

### BWD.Jobs.isInterestedInApplying(_jobId_)

`BWD.Jobs.isInterestedInApplying()` allows to find out whether the job has been marked 
as "Interested in Applying" by its ID or not. It returns a promise that is resolved 
when the API call returns successfully or is rejected if any error is encountered.
The return value contains job pass status in `stat` field of response entity.

The method requires a single argument specifying the job ID, which should be a
positive integer.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

The response entity looks like:

```
{
  "data": {
    "stat": "true"
  },
  "self": {
    "title": "Self",
    "href": "http://local.app.betterweekdays.com/api/v1.0/job/12/interested-in-apply"
  }
}
```

Example usage:

```
var jobId = 12;
BWD.Jobs.isInterestedInApplying(jobId)
  .then(function(results) {
    // the stat property of results.data
  });
```

### BWD.Users.getMe()

`BWD.Users.getMe()` allows retrieving information about the logged in user. It
returns a promise that is resolved when the API call returns successfully or is
rejected if any error is encountered.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
BWD.Users.getMe()
  .then(function(results) {
    // The user info is the first item in the array at results.data
  });
```

### BWD.Users.getById(_userId_)

`BWD.Users.getById()` allows retrieving information about a user by its ID. It
returns a promise that is resolved when the API call returns successfully or is
rejected if any error is encountered.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
var userId = 742;
BWD.Users.getById(userId)
  .then(function(results) {
    // The user info is the first item in the array at results.data
  });
```

### BWD.Favorites.get()

`BWD.Favorites.get()` allows retrieving a list of the user's favorite jobs, 
articles, and companies. Favorites are stats. It returns a promise 
that is resolved when the API call returns successfully or is
rejected if any error is encountered.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

An query argument may be given with any parameters supported by the Discovery API.
The 'favoritejob', 'favoritearticle', 'favoritecompany' can be used as filter type.

Example usage:

```
var query = {
  'filter[type]': 'favoritejob'
};
BWD.Favorites.get(query)
  .then(function(results) {
    // The list of favorites is the array at results.data
  });
```

### BWD.Feed.get()

`BWD.Feed.get()` allows retrieving a list of personalized jobs for the current user.
It returns a promise that is resolved when the API call returns successfully or is
rejected if any error is encountered.

Prior to using this method, `BWD.Client.getAccessToken()` should be used to get
a valid access token.

Example usage:

```
BWD.Feed.get()
  .then(function(results) {
    // The list of personalized jobs is the array at results.data
  });
```