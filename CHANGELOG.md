# Change Log for @betterweekdays/discovery-sdk

## v4.1.0 [2017/11/8]

- Adding the `BWD.Users.getAssessmentInfo` method for retrieving a user's
assessment data.

## v4.0.0 [2017/10/2]

- Adding support for articles and companies content, including stats for both.
- Adding support for API user authentication with Auth0.
- Adding automatic retries for certain failed requests.
- Adding support for other content types, including messages, orgs, locations,
cips, job areas, schools, degrees programs, degree specializations, onets, and
courses.

## v3.6.0 [2016/12/7]

- Adding the `BWD.Feed.get` method for getting personalized feed data.
- Adding MIME interceptor for converting request/response data according to
the `Content-Type` header.

## v3.5.0 [2016/12/6]

- Adding the `BWD.Users.update` and `BWD.Users.updateMe` methods for updating
user information.
- Adding the `BWD.Favorites.get` method for getting favorites
jobs/articles/companies information.

## v3.4.0 [2016/12/2]

- Adding configuration setting for the backend API environment to use.

## v3.3.0 [2016/12/2]

- Adding the `BWD.Users.getMe` and `BWD.Users.getById` methods for retrieving
user information.

## v3.2.0 [2016/11/21]

- Adding the `BWD.Companies.get` and `BWD.Companies.getById` methods for
retrieving company profiles.

## v3.1.0 [2016/07/27]

- Adding the `BWD.Jobs.getById` method for retrieving a single job posting.
- Improving the API documentation and sample browser app.

## v3.0.0 [2016/06/13]

- First release supporting the Oauth implicit grant flow for user authorization.

## v2.0.0 [2016/05/25]

- Implements the jobs retrieval endpoint.
- First release supporting the Oauth client credentials authorization flow.
- Adding reference documentation in API.md.
