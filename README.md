# Better Weekdays Discovery SDK for JavaScript

[![npmjs.com Version](https://img.shields.io/npm/v/@betterweekdays/discovery-sdk.svg?maxAge=2592000)](https://www.npmjs.com/package/@betterweekdays/discovery-sdk)
[![Build Status](https://semaphoreci.com/api/v1/projects/14808c19-572f-4fd4-aba2-2b14ff076e1f/807948/badge.svg)](https://semaphoreci.com/joebwd/discovery-sdk-js)

This is a browser- and Node.js-compatible JavaScript SDK for integrating with
the Better Weekdays Discovery API.

This repository includes:

- `dist/`: Browser-ready JavaScript files (minified and normal) with full SDK
library and dependencies.

- `samples/browser/`: Example web app implementing the SDK for fetching job data
(and required authorization).

- `samples/node/`: Example node app implementing the SDK for fetching job data.

- `src/`: Source code for the SDK.

- `test/`: Unit and end-to-end tests for the SDK.

- `API.md`: SDK API reference document.

- `CHANGELOG.md`: Change log for SDK versions.

- `README.md`: This file.

The following operations for common development tasks are implemented as scripts
in the `package.json` file:

- `npm run compile`: Uses browserify to generate the browser-ready JavaScript
files found under `dist/`.

- `npm run lint`: Uses eslint to check JavaScript code style against the Google
JavaScript Style Guide.

- `npm run serve`: Runs a web server on a local address with the browser sample
app found under `samples/browser/`.

- `npm test`: Runs the unit and end-to-end tests found under `test/`. To run
end-to-end tests, an API key and secret must be provided with the `BWD_API_KEY`
and `BWD_API_SECRET` environment variables.
