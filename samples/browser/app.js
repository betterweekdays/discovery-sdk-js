/* global $, BWD, window */

window.goLogin = function() {
  BWD.Config.set({
    APIEnv: $('#env').val(),
    APIKey: $('#apiKey').val()
  });

  $('#bwdAccessTokenStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Redirecting to login...');

  window.location = BWD.Client.getLogin();
};

window.goLogout = function() {
  window.location = BWD.Client.getLogout();
};

window.getApigeeAccessToken = function() {
  window.event.preventDefault();

  BWD.Config.set({
    APIEnv: $('#env').val(),
    BackendEnv: $('#backEnv').val(),
    APIKey: $('#apiKey').val(),
    APISecret: $('#apiSecret').val(),
  });

  $('#bwdAccessTokenStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Client.getAccessToken()
    .then(function(results) {
      console.log('BWD.Client.getAccessToken() returned successfully. Results:',
        results);
      if (results.hasOwnProperty('access_token') &&
          results.hasOwnProperty('status') && results.status === 'approved') {
        $('#accessToken').val(results.access_token);
        $('#bwdAccessTokenStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully retrieved access token. The token will be ' +
            'stored for use in other methods, like for listing jobs.<br/><br/>' +
            'This request can be performed with curl using the following options:<pre>' + window.mockCurlToken() + '</pre>');
      } else {
        $('#accessToken').val('');
        $('#bwdAccessTokenStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('Received response, but did not receive a valid access token.' +
            ' Please try again.');
      }
    })
    .catch(function(error) {
      console.log(
        'An error occurred when invoking BWD.Client.getAccessToken(). Error:',
        error);
      $('#accessToken').val('');
      $('#bwdAccessTokenStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching an access token: ' +
          error.message);
    });
};

window.getAuth0Token = function(tokenType) {
  window.event.preventDefault();

  var auth0id = $('#auth0_id').val();
  BWD.Config.set({
    Auth0ID: auth0id
  });

  var email = $('#auth0_email').val();
  var password = $('#auth0_password').val();

  $('#bwdAuth0Status')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Auth.login(email, password)
    .then(function(result) {
      console.log('BWD.Auth.login() returned successfully. Results:', result);
      if (result.hasOwnProperty('accessToken') ||
      result.hasOwnProperty('idToken')) {
        BWD.Config.set({
          APIEnv: $('#env').val(),
          BackendEnv: $('#backEnv').val()
        });
        if (tokenType === 'id') {
          $('#auth0IdToken').val(result.idToken);
          BWD.Config.set({
            AccessToken: {
              id_token: result.idToken
            }
          });
        } else {
          $('#auth0AccessToken').val(result.accessToken);
          BWD.Config.set({
            AccessToken: {
              access_token: result.accessToken
            }
          });
        }
        $('#bwdAuth0Status')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully retrieved Auth0 ' + tokenType + ' token. The token will be ' +
            'stored for use in other methods, like for listing jobs.<br/>');
      } else {
        $('#auth0AccessToken').val('');
        $('#bwdAuth0Status')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('Received response, but did not receive a valid auth0 token.' +
            ' Please try again.');
      }
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Auth.login(). Error:', error);
      $('#auth0IdToken').val('');
      $('#auth0AccessToken').val('');
      $('#bwdAuth0Status')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching an Auth0 token: ' +
          (error.description || error.name));
    });
};

window.refreshAuth0Token = function() {
  window.event.preventDefault();

  $('#bwdAccessTokenStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Auth.refreshToken()
    .then(function(result) {
      console.log('BWD.Auth.refreshToken() returned successfully. Results:', result);
      $('#auth0AccessToken').val(result.accessToken);
      $('#auth0IdToken').val(result.idToken);
      $('#bwdAuth0Status')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully refreshed Auth0 token.<br/>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Auth.refreshToken(). Error:', error);
      $('#bwdAuth0Status')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when refreshing an auth0 token: ' + error.errorDescription);
    });
};

window.registerUser = function() {
  window.event.preventDefault();

  var auth0id = $('#auth0_id').val();
  BWD.Config.set({
    Auth0ID: auth0id
  });

  var fullname = $('#auth0_reg_fullname').val();
  var email = $('#auth0_reg_email').val();
  var password = $('#auth0_reg_password').val();

  $('#auth0RegisterStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Auth.signup(email, password)
    .then(function(result) {
      console.log('BWD.Auth.signup() returned successfully. Results:', result);
      $('#auth0AccessToken').val(result.accessToken);
      $('#auth0IdToken').val(result.idToken);
      BWD.Config.set({
        AccessToken: {
          id_token: result.idToken
        },
        APIEnv: $('#env').val(),
        BackendEnv: $('#backEnv').val()
      });
      var user = {
        email: email
      };
      BWD.Users.registerAuth0Token("JWT " + result.idToken, user)
        .then(function(result) {
          $('#auth0RegisterStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('Successfully registered new user.');
        })
        .catch(function(error) {
          console.log('An error occurred when invoking BWD.Users.registerAuth0Token(). Error:', error);
          $('#auth0RegisterStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when registering the Auth0 token with the API backend: ' + (error.title || error.error));
        });
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Auth.signup(). Error:', error);
      $('#auth0RegisterStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when registering an new user with Auth0: ' +
          (error.description || error.name));
    });
};

window.getJobs = function(preset) {
  window.event.preventDefault();

  var params = {
    range: $('#limit').val(),
    page: $('#page').val()
  };

  if (preset) {
    if (preset === 'nyc') {
      params['filter[location]'] = 29; // New York, NY
    } else if (preset === 'bac-chi') {
      // Bank of America jobs in Chicago
      params['filter[company]'] = 652; // Bank of America
      params['filter[location]'] = 24; // Chicago, IL
    }
  }

  window.jobsActivePreset = preset;

  $('#bwdJobsTableBody').html('');
  $('#bwdJobsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Jobs.get(params)
    .then(function(results) {
      console.log('BWD.Jobs.get() returned successfully. Results: ', results);
      var totalNum = results.count;
      var num = results.data.length;
      var tableHtml;
      for (var i = 0; i < num; i++) {
        var job = results.data[i];
        var company = job.company ? job.company.name : '';
        var location = job.location ? job.location.name : '';
        tableHtml += '<tr><td>' + job.id + '</td><td>' + job.title +
          '</td><td>' + company + '</td><td>' + location + '</td></tr>';
      }
      $('#bwdJobsTableBody').html(tableHtml);
      $('#bwdJobsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully received ' + num + ' jobs out of ' + totalNum + ' total.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('jobs', params, 'GET') + '</pre>');
      $('.pager-prev').parent().css('display', results.previous ? 'inline' : 'none');
      $('.pager-next').parent().css('display', results.next ? 'inline' : 'none');
      $('.pager').removeClass('hide');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Jobs.get(). Error: ',
        error);
      $('#bwdJobsTableBody').html('');
      $('#bwdJobsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching jobs: ' + (error.title || error.error));
      $('.pager').addClass('hide');
      $('.pager li').hide();
    });
};

window.searchJobs = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdJobsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var query = {
    title: 'Healthcare',
    location: 26154,
    jobarea: 152
  };

  $('#bwdSearchJobsTableBody').html('');
  $('#bwdJobsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Search.search('job', query)
    .then(function(results) {
      console.log('BWD.Search.search() returned successfully. Results: ', results);
      var totalNum = results.count;
      var num = results.data.length;
      var tableHtml;
      for (var i = 0; i < num; i++) {
        var stat = results.data[i];
        tableHtml += '<tr><td>' + stat.id + '</td><td>' + stat.type +
          '</td><td>' + stat.source + '</tr>';
      }
      $('#bwdSearchJobsTableBody').html(tableHtml);
      $('#bwdJobsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully received ' + num + ' stats .<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('search', query, 'GET') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Search.search(). Error: ',
        error);
      $('#bwdSearchJobsTableBody').html('');
      $('#bwdJobsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when searching jobs: ' + (error.title || error.error));
    });
};

window.pageJobs = function(num) {
  $('#page').val(parseInt($('#page').val(), 10) + Number(num));
  window.getJobs(window.jobsActivePreset);
};

window.getJob = function() {
  window.event.preventDefault();

  var jobId = $('#jobId').val();

  $('#bwdJobTableBody').html('');
  $('#bwdJobStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Jobs.getById(jobId)
    .then(function(results) {
      console.log('BWD.Jobs.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        var job = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
              str += objToTable(value, key);
              return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(job, '');
        $('#bwdJobStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received job.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('jobs/' + jobId, null, 'GET') + '</pre>');
      } else {
        $('#bwdJobStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No job found with given ID.');
      }
      $('#bwdJobTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Jobs.get(). Error: ',
        error);
      $('#bwdJobTableBody').html('');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the job: ' + (error.title || error.error));
    });
};

window.getInteractionForJob = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdJobStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var jobId = $('#jobId').val();

  $('#bwdJobInteractionsTableBody').html('');
  $('#bwdJobStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Jobs.getInteractions(jobId)
    .then(function(results) {
      console.log('BWD.Jobs.Interactions() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'title']);
        }

        $('#bwdJobStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received job interactions.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('jobs/' + jobId + '/todos', null, 'GET') + '</pre>');
      } else {
        $('#bwdJobStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No interactions for job found with given ID.');
      }
      $('#bwdJobInteractionsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Jobs.getInteractions(). Error: ',
        error);
      $('#bwdJobInteractionsTableBody').html('');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting interactions for the job: ' + (error.title || error.error));
    });
};

window.setPreviewed = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdJobStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var jobId = $('#jobId').val();

  $('#bwdJobTableBody').html('');
  $('#bwdJobStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Jobs.setPreviewed(jobId)
    .then(function(results) {
      console.log('BWD.Jobs.setPreviewed() performed successfully.');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set job previwed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('jobs/' + jobId + '/previewed-job', null, 'GET') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Jobs.setPreviewed(). Error: ',
        error);
      $('#bwdJobTableBody').html('');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the job previewed: ' + (error.title || error.error));
    });
};

window.askForHelpOnJob = function(message) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdJobStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var jobId = $('#jobId').val();

  $('#bwdJobTableBody').html('');
  $('#bwdJobStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    message: message
  };

  BWD.Jobs.askForHelp(jobId, query)
    .then(function(results) {
      console.log('BWD.Jobs.askForHelp() performed successfully.');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully ask for help.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('jobs/' + jobId + '/ask', query, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Jobs.askForHelp(). Error: ',
        error);
      $('#bwdJobTableBody').html('');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when asking for help: ' + (error.title || error.error));
    });
};

window.setViewed = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdJobStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var jobId = $('#jobId').val();

  $('#bwdJobTableBody').html('');
  $('#bwdJobStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Jobs.setViewed(jobId)
    .then(function(results) {
      console.log('BWD.Jobs.setViewed() performed successfully.');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set job viwed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('jobs/' + jobId + '/viewed-job', null, 'GET') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Jobs.setViewed(). Error: ',
        error);
      $('#bwdJobTableBody').html('');
      $('#bwdJobStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the job viewed: ' + (error.title || error.error));
    });
};

window.getUser = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdUserStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdUserTableBody').html('');
  $('#bwdUserStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Users.getMe()
    .then(function(results) {
      console.log('BWD.Users.getMe() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        var user = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
              str += objToTable(value, key);
              return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(user, '');
        $('#bwdUserStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received user.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('users/me', null, 'GET') + '</pre>');
      } else {
        $('#bwdUserStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No user found.');
      }
      $('#bwdUserTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Users.getMe(). Error: ',
        error);
      $('#bwdUserTableBody').html('');
      $('#bwdUserStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the user: ' + (error.title || error.error));
    });
};

window.claimAssessmentCredit = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdUserStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdUserStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Claiming...');

  var accessCode = $('#accessCode').val();

  BWD.Users.claimAssessmentCredits(accessCode)
    .then(function(results) {
      console.log('BWD.Users.claimAssessmentCredits() returned successfully. Results: ', results);
      if (results.data.can_use_credit) {
        $('#bwdUserStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully claimed credit.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('assessmentcreditclaim/me/' + accessCode, null, 'GET') + '</pre>');
      } else {
        if (!results.data.assessment_exists && !results.data.valid_access_code) {
          $('#bwdUserStatus')
            .attr('class', 'alert')
            .addClass('alert-warning')
            .html('Access code is invalid.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('assessmentcreditclaim/me/' + accessCode, null, 'GET') + '</pre>');
        } else {
          $('#bwdUserStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('Assessment already exists.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('assessmentcreditclaim/me/' + accessCode, null, 'GET') + '</pre>');
        }
      }
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Users.claimAssessmentCredits(). Error: ',
        error);
      $('#bwdUserStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when claiming assessment credit with ' + accessCode + ' access code:' + (error.title || error.error));
    });
};

window.updateUser = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdUserStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdUserTableBody').html('');
  $('#bwdUserStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var firstName = $('#firstName').val();
  var lastName = $('#lastName').val();

  var user = {
    firstName: firstName,
    lastName: lastName
  };

  BWD.Users.updateMe(user)
    .then(function(results) {
      console.log('BWD.Users.updateMe() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        var user = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
              str += objToTable(value, key);
              return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(user, '');
        $('#bwdUserStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received user.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('users/me', user, 'PATCH') + '</pre>');
      } else {
        $('#bwdUserStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No user found.');
      }
      $('#bwdUserTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Users.updateMe(). Error: ',
        error);
      $('#bwdUserTableBody').html('');
      $('#bwdUserStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the user: ' + (error.title || error.error));
    });
};

window.getFavorites = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdFavoritesStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdFavoritesTableBody').html('');
  $('#bwdFavoritesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var type = $('#favorite-filter').val();

  var query = {
    'filter[type]': type
  };

  BWD.Favorites.get(query)
    .then(function(results) {
      console.log('BWD.Favorites.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['type', 'ref', 'taxonomyref']);
        }

        $('#bwdFavoritesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received favorites.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('favorites', query, 'GET') + '</pre>');
      } else {
        $('#bwdFavoritesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No favorites found.');
      }
      $('#bwdFavoritesTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Favorites.get(). Error: ',
        error);
      $('#bwdFavoritesTableBody').html('');
      $('#bwdFavoritesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the favorites: ' + (error.title || error.error));
    });
};

window.getFeed = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdFeedStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdFeedTableBody').html('');
  $('#bwdFeedStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var limit = $('#feedLimit').val();

  var query = {
    range: limit
  };

  BWD.Feed.get(query)
    .then(function(results) {
      console.log('BWD.Feed.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['type', 'id', 'source']);
        }

        $('#bwdFeedStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received feed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('feed', query, 'GET') + '</pre>');
      } else {
        $('#bwdFeedStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No feed found.');
      }
      $('#bwdFeedTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Feed.get(). Error: ',
        error);
      $('#bwdFeedTableBody').html('');
      $('#bwdFeedStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the feed: ' + (error.title || error.error));
    });
};

window.getFeedArticles = function(basedOnFollows) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAdviceStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdAdviceTableBody').html('');
  $('#bwdAdviceStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var limit = $('#adviceLimit').val();

  var query = {
    range: limit
  };

  var fetchArticles = basedOnFollows ? BWD.Advice.get : BWD.Feed.getArticles;

  fetchArticles(query)
    .then(function(results) {
      console.log('BWD.Advice.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['type', 'id', 'source']);
        }

        $('#bwdAdviceStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received Advice feed articles.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('advice', query, 'GET') + '</pre>');
      } else {
        $('#bwdAdviceStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No Advice feed articles found.');
      }
      $('#bwdAdviceTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Advice.get(). Error: ',
        error);
      $('#bwdAdviceTableBody').html('');
      $('#bwdAdviceStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting Advice feed articles: ' + (error.title || error.error));
    });
};

window.getFeedJobs = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAdviceStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdAdviceTableBody').html('');
  $('#bwdAdviceStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var limit = $('#adviceLimit').val();

  var query = {
    range: limit
  };

  BWD.Feed.getJobs(query)
    .then(function(results) {
      console.log('BWD.Feed.getJobs() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['type', 'id', 'source']);
        }

        $('#bwdAdviceStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received feed jobs.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('feed/job', query, 'GET') + '</pre>');
      } else {
        $('#bwdAdviceStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No feed jobs found.');
      }
      $('#bwdAdviceTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Feed.getJobs(). Error: ',
        error);
      $('#bwdAdviceTableBody').html('');
      $('#bwdAdviceStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting feed jobs: ' + (error.title || error.error));
    });
};

window.getFeedCompanies = function(showmatch = 0) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAdviceStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdAdviceTableBody').html('');
  $('#bwdAdviceStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var limit = $('#adviceLimit').val();

  var query = {
    range: limit,
    showmatch: showmatch
  };

  BWD.Feed.getCompanies(query)
    .then(function(results) {
      console.log('BWD.Feed.getCompanies() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['type', 'id', 'source']);
        }

        $('#bwdAdviceStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received feed companies.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('feed/companies', query, 'GET') + '</pre>');
      } else {
        $('#bwdAdviceStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No feed companies found.');
      }
      $('#bwdAdviceTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Feed.getCompanies(). Error: ',
        error);
      $('#bwdAdviceTableBody').html('');
      $('#bwdAdviceStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting feed companies: ' + (error.title || error.error));
    });
};

window.getMessages = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdMessagesStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdMessagesTableBody').html('');
  $('#bwdMessagesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    'filter[msgType]': 'message',
    'fields': 'id,from,recipient,subject,body'
  };

  BWD.Messaging.getMessages(query)
    .then(function(results) {
      console.log('BWD.Messaging.getMessages() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i]);
        }
        function objToTableRow(data) {
          return '<tr><td>' + data.id + '</td><td>' + data.from + '</td><td>' + data.recipient[0] + '</td><td>' + data.subject + '</td><td>' + data.body + '</td></tr>';
        }

        $('#bwdMessagesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received a list of messages.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('messages', null, 'GET') + '</pre>');
      } else {
        $('#bwdMessagesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No messages found.');
      }
      $('#bwdMessagesTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Messaging.getMessages(). Error: ',
        error);
      $('#bwdMessagesTableBody').html('');
      $('#bwdMessagesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the message threads: ' + (error.title || error.error));
    });
};

window.getStatsSummary = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdStatsSummaryStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdStatsSummaryTableBody').html('');
  $('#bwdStatsSummaryStatus')
      .attr('class', 'alert')
      .addClass('alert-warning')
      .text('Loading...');

  BWD.Stats.getSummary()
      .then(function(results) {
        console.log('BWD.getSummary.get() returned successfully. Results: ', results);
        var tableHtml;
        if (results.data.length) {
          for (var i = 0; i < results.data.length; i++) {
            tableHtml += objToTableRow(results.data[i], ['stat_key', 'stat_type', 'article_type', 'count']);
          }

          $('#bwdStatsSummaryStatus')
              .attr('class', 'alert')
              .addClass('alert-success')
              .html('Successfully received a list of locations.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('stats/summary', null, 'GET') + '</pre>');
        } else {
          $('#bwdStatsSummaryStatus')
              .attr('class', 'alert')
              .addClass('alert-warning')
              .text('No locations found.');
        }
        $('#bwdStatsSummaryTableBody').html(tableHtml);
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Stats.getSummary(). Error: ',
            error);
        $('#bwdStatsSummaryTableBody').html('');
        $('#bwdStatsSummaryStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting stats summary: ' + (error.title || error.error));
      });
};


window.getLocations = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdLocationsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdLocationsTableBody').html('');
  $('#bwdLocationsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    fields: 'id,name,lat,long'
  };

  BWD.Locations.get(query)
    .then(function(results) {
      console.log('BWD.Locations.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['name', 'lat', 'long']);
        }

        $('#bwdLocationsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received a list of locations.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('locations', null, 'GET') + '</pre>');
      } else {
        $('#bwdLocationsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No locations found.');
      }
      $('#bwdLocationsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Locations.get(). Error: ',
        error);
      $('#bwdLocationsTableBody').html('');
      $('#bwdLocationsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting locations: ' + (error.title || error.error));
    });
};

window.getCourses = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCoursesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdCoursesTableBody').html('');
  $('#bwdCoursesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Courses.get()
    .then(function(results) {
      console.log('BWD.Courses.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'name', 'description']);
        }

        $('#bwdCoursesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received a list of courses.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('courses', null, 'GET') + '</pre>');
      } else {
        $('#bwdCoursesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No courses found.');
      }
      $('#bwdCoursesTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Courses.get(). Error: ',
        error);
      $('#bwdCoursesTableBody').html('');
      $('#bwdCoursesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting courses: ' + (error.title || error.error));
    });
};

window.getOnets = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdOnetsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdOnetsTableBody').html('');
  $('#bwdOnetsStatus')
      .attr('class', 'alert')
      .addClass('alert-warning')
      .text('Loading...');

  var params = {
    range: $('#onetsLimit').val(),
    page: $('#onetsPage').val(),
    fields: 'id,name,onetCode,lead'
  };

  BWD.Onets.get(params)
      .then(function(results) {
        console.log('BWD.Onets.get() returned successfully. Results: ', results);
        var tableHtml;
        if (results.data.length) {
          for (var i = 0; i < results.data.length; i++) {
            tableHtml += objToTableRow(results.data[i], ['id', 'name', 'onetCode', 'lead']);
          }

          $('#bwdOnetsStatus')
              .attr('class', 'alert')
              .addClass('alert-success')
              .html('Successfully received a list of onets.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('onets', params, 'GET') + '</pre>');
        } else {
          $('#bwdOnetsStatus')
              .attr('class', 'alert')
              .addClass('alert-warning')
              .text('No locations found.');
        }
        $('#bwdOnetsTableBody').html(tableHtml);
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Onets.get(). Error: ',
            error);
        $('#bwdOnetsTableBody').html('');
        $('#bwdOnetsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting onets: ' + error.message);
      });
};


window.getOnet = function() {
  window.event.preventDefault();

  var onetId = $('#onetId').val();

  $('#bwdOnetStatsTableBody').html('');
  $('#bwdOnetStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-warning')
      .text('Loading...');

  BWD.Onets.getById(onetId)
      .then(function(results) {
        console.log('BWD.Onets.getById() returned successfully. Results: ', results);
        var tableHtml;
        if (results.data.length) {
          var onet = results.data[0];
          tableHtml += objToTableRow(onet, ['name', 'onetCode', 'lead']);

          $('#bwdOnetStatsStatus')
              .attr('class', 'alert')
              .addClass('alert-success')
              .html('Successfully received onet.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('onets/' + onetId, null, 'GET') + '</pre>');
        } else {
          $('#bwdOnetStatsStatus')
              .attr('class', 'alert')
              .addClass('alert-warning')
              .text('No onet found with given ID.');
        }
        $('#bwdOnetStatsTableBody').html(tableHtml);
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Onets.getById(). Error: ',
            error);
        $('#bwdOnetStatsTableBody').html('');
        $('#bwdOnetStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting the onet: ' + error.message);
      });
};

window.getAutocompletionsForCompany = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAutocompleteStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyTitle = $('#companyTitle').val();

  $('#bwdCompanyAutocompleteTableBody').html('');
  $('#bwdAutocompleteStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: companyTitle
  };

  BWD.Companies.autocomplete(companyTitle)
    .then(function(results) {
      console.log('BWD.Companies.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdAutocompleteStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received companies.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/company', query, 'GET') + '</pre>');
      } else {
        $('#bwdAutocompleteStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No companies found with given title.');
      }
      $('#bwdCompanyAutocompleteTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.autocomplete(). Error: ',
        error);
      $('#bwdCompanyAutocompleteTableBody').html('');
      $('#bwdAutocompleteStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting companies: ' + (error.title || error.error));
    });
};

window.getAutocompletionsForJobAreas = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdJobAreasStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var jobAreaTitle = $('#jobAreaTitle').val();

  $('#bwdJobAreasTableBody').html('');
  $('#bwdJobAreasStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: jobAreaTitle
  };

  BWD.JobAreas.autocomplete(jobAreaTitle)
    .then(function(results) {
      console.log('BWD.JobAreas.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdJobAreasStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received job areas.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/jobarea', query, 'GET') + '</pre>');
      } else {
        $('#bwdJobAreasStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No job areas found with given title.');
      }
      $('#bwdJobAreasTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.JobAreas.autocomplete(). Error: ',
        error);
      $('#bwdJobAreasTableBody').html('');
      $('#bwdJobAreasStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting job areas: ' + (error.title || error.error));
    });
};

window.getAutocompletionsForLocations = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAutocompleteLocationsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var locationTitle = $('#locationTitle').val();

  $('#bwdAutocompleteLocationsTableBody').html('');
  $('#bwdAutocompleteLocationsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: locationTitle
  };

  BWD.Locations.autocomplete(locationTitle)
    .then(function(results) {
      console.log('BWD.Locations.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdAutocompleteLocationsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received locations.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/location', query, 'GET') + '</pre>');
      } else {
        $('#bwdAutocompleteLocationsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No locations found with given title.');
      }
      $('#bwdAutocompleteLocationsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Locations.autocomplete(). Error: ',
        error);
      $('#bwdAutocompleteLocationsTableBody').html('');
      $('#bwdAutocompleteLocationsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting locations: ' + (error.title || error.error));
    });
};

window.getAutocompletionsForOnets = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAutocompleteOnetsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  var onetTitle = $('#onetTitle').val();

  $('#bwdAutocompleteOnetsTableBody').html('');
  $('#bwdAutocompleteOnetsStatus')
      .attr('class', 'alert')
      .addClass('alert-warning')
      .text('Loading...');

  var query = {
     title: onetTitle
   };

  BWD.Onets.autocomplete(onetTitle)
      .then(function(results) {
        console.log('BWD.Onets.autocomplete() returned successfully. Results: ', results);
        var tableHtml;
        if (results.data.length) {
          for (var i = 0; i < results.data.length; i++) {
            tableHtml += objToTableRow(results.data[i], ['id', 'label']);
          }

          $('#bwdAutocompleteOnetsStatus')
              .attr('class', 'alert')
              .addClass('alert-success')
              .html('Successfully received locations.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/onet', query, 'GET') + '</pre>');
        } else {
          $('#bwdAutocompleteOnetsStatus')
              .attr('class', 'alert')
              .addClass('alert-warning')
              .text('No onets found with given title.');
        }
        $('#bwdAutocompleteOnetsTableBody').html(tableHtml);
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Onets.autocomplete(). Error: ',
            error);
        $('#bwdAutocompleteOnetsTableBody').html('');
        $('#bwdAutocompleteOnetsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting onets: ' + error.message);
      });
};

window.getAutocompletionsForSchools = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdSchoolsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var schoolTitle = $('#schoolTitle').val();

  $('#bwdSchoolsTableBody').html('');
  $('#bwdSchoolsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: schoolTitle
  };

  BWD.Schools.autocomplete(schoolTitle)
    .then(function(results) {
      console.log('BWD.Schools.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdSchoolsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received schools.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/school', query, 'GET') + '</pre>');
      } else {
        $('#bwdSchoolsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No schools found with given title.');
      }
      $('#bwdSchoolsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Schools.autocomplete(). Error: ',
        error);
      $('#bwdSchoolsTableBody').html('');
      $('#bwdSchoolsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting schools: ' + (error.title || error.error));
    });
};

window.getAutocompletionsForFieldsOfStudy = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdFieldsOfStudyStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var fieldOfStudyTitle = $('#fieldOfStudyTitle').val();

  $('#bwdFieldsOfStudyBody').html('');
  $('#bwdFieldsOfStudyStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: fieldOfStudyTitle
  };

  BWD.DegreeSpecializations.autocomplete(fieldOfStudyTitle)
    .then(function(results) {
      console.log('BWD.DegreeSpecializations.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdFieldsOfStudyStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received fields of study.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/cip', query, 'GET') + '</pre>');
      } else {
        $('#bwdFieldsOfStudyStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No fields of study found with given title.');
      }
      $('#bwdFieldsOfStudyTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.DegreeSpecializations.autocomplete(). Error: ',
        error);
      $('#bwdFieldsOfStudyTableBody').html('');
      $('#bwdFieldsOfStudyStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting fields of study: ' + (error.title || error.error));
    });
};

window.getAutocompletionsForDegreePrograms = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdDegreeProgramsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var degreeProgramTitle = $('#degreeProgramTitle').val();

  $('#bwdDegreeProgramsBody').html('');
  $('#bwdDegreeProgramsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: degreeProgramTitle
  };

  BWD.DegreePrograms.autocomplete(degreeProgramTitle)
    .then(function(results) {
      console.log('BWD.DegreePrograms.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdDegreeProgramsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received degree programs.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/degreeprogram', query, 'GET') + '</pre>');
      } else {
        $('#bwdDegreeProgramsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No degree programs found with given title.');
      }
      $('#bwdDegreeProgramsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.DegreePrograms.autocomplete(). Error: ',
        error);
      $('#bwdDegreeProgramsTableBody').html('');
      $('#bwdDegreeProgramsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting degree programs: ' + (error.title || error.error));
    });
};

window.getAutocompletionsForCourses = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCourseAutocompletesStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var courseTitle = $('#courseTitle').val();

  $('#bwdCourseAutocompletesTableBody').html('');
  $('#bwdCourseAutocompletesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    title: courseTitle
  };

  BWD.Courses.autocomplete(courseTitle)
    .then(function(results) {
      console.log('BWD.Courses.autocomplete() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'label']);
        }

        $('#bwdCourseAutocompletesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received courses.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('autocomplete/course', query, 'GET') + '</pre>');
      } else {
        $('#bwdCourseAutocompletesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No courses found with given title.');
      }
      $('#bwdCourseAutocompletesTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Courses.autocomplete(). Error: ',
        error);
      $('#bwdCourseAutocompletesTableBody').html('');
      $('#bwdCourseAutocompletesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting courses: ' + (error.title || error.error));
    });
};

window.getArticles = function(preset) {
  window.event.preventDefault();

  var params = {
    range: $('#articlesLimit').val(),
    page: $('#articlesPage').val()
  };

  $('#bwdArticlesTableBody').html('');
  $('#bwdArticlesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.get(params)
    .then(function(results) {
      console.log('BWD.Articles.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'title']);
        }

        $('#bwdArticlesTableBody').html(tableHtml);
        $('#bwdArticlesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received articles.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles', params, 'GET') + '</pre>');
      } else {
        $('#bwdArticlesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No articles was found.');
      }
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.get(). Error: ',
        error);
      $('#bwdArticlesTableBody').html('');
      $('#bwdArticlesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching articles: ' + (error.title || error.error));
    });
};

window.getArticle = function() {
  window.event.preventDefault();

  var articleId = $('#articleId').val();

  $('#bwdArticleStatsTableBody').html('');
  $('#bwdArticleStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.getById(articleId)
    .then(function(results) {
      console.log('BWD.Articles.getById() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        var article = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
              str += objToTable(value, key);
              return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(article, '');
        $('#bwdArticleStatsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received article.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles/' + articleId, null, 'GET') + '</pre>');
      } else {
        $('#bwdArticleStatsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No article found with given ID.');
      }
      $('#bwdArticleStatsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.getById(). Error: ',
        error);
      $('#bwdArticleStatsTableBody').html('');
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the article: ' + (error.title || error.error));
    });
};

window.setViewedArticle = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdArticleStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var articleId = $('#articleId').val();

  $('#bwdArticleStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.setViewed(articleId)
    .then(function(results) {
      console.log('BWD.Articles.setViewed() performed successfully.');
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set article viewed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles/' + articleId + '/viewed', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.setViewed(). Error: ',
        error);
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the article viewed: ' + (error.title || error.error));
    });
};

window.setPreviewedArticle = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdArticleStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var articleId = $('#articleId').val();

  $('#bwdArticleStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.setPreviewed(articleId)
    .then(function(results) {
      console.log('BWD.Articles.setPreviewed() performed successfully.');
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set article previewed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles/' + articleId + '/previewed', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.setPreviewed(). Error: ',
        error);
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the article previewed: ' + (error.title || error.error));
    });
};

window.setFavoriteArticle = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdArticleStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var articleId = $('#articleId').val();

  $('#bwdArticleStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.setFavorite(articleId)
    .then(function(results) {
      console.log('BWD.Articles.setFavorite() performed successfully.');
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set article favorite.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles/' + articleId + '/favorite', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.setFavorite(). Error: ',
        error);
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the article favorite: ' + (error.title || error.error));
    });
};

window.askForHelpOnArticle = function(message) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdArticleStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var articleId = $('#articleId').val();
  var query = {
    message: message
  };

  $('#bwdArticleStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.askForHelp(articleId, query)
    .then(function(results) {
      console.log('BWD.Articles.askForHelp() performed successfully.');
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully ask for help.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles/' + articleId + '/ask', query, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.askForHelp(). Error: ',
        error);
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when asking for help: ' + (error.title || error.error));
    });
};

window.setPassArticle = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdArticleStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var articleId = $('#articleId').val();

  $('#bwdArticleStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.setPass(articleId)
    .then(function(results) {
      console.log('BWD.Articles.setPass() performed successfully.');
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set article pass.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articles/' + articleId + '/pass', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.setPass(). Error: ',
        error);
      $('#bwdArticleStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the article pass: ' + (error.title || error.error));
    });
};

window.getArticleCategories = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdArticleCategoriesStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdArticleCategoriesTableBody').html('');
  $('#bwdArticleCategoriesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Articles.getCategories()
    .then(function(results) {
      console.log('BWD.Articles.getCategories() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'name', 'description', 'imageUrl']);
        }

        $('#bwdArticleCategoriesTableBody').html(tableHtml);
        $('#bwdArticleCategoriesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received article categories.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('articlecategories', null, 'GET') + '</pre>');
      } else {
        $('#bwdArticleCategoriesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No article categories were found.');
      }
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Articles.getCategories(). Error: ',
        error);
      $('#bwdArticleCategoriesTableBody').html('');
      $('#bwdArticleCategoriesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching article categories: ' + (error.title || error.error));
    });
};

window.getCompanyInfoFromGlassdoor = function() {
  window.event.preventDefault();

  var companyId = $('#glassdoorCompanyId').val();

  var params = {
    companyId: companyId
  };

  BWD.Glassdoor.getCompanyInfo(params)
    .then(function(results) {
      console.log('BWD.Glassdoor.getCompanyInfo() returned successfully. Results:', results);
      var tableHtml;
      if (results.data.length) {
        var companyInfo = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
              str += objToTable(value, key);
              return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(companyInfo, '');
        $('#bwdGlassdoorDataStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received company info from Glassdoor.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('glassdoor/', params, 'GET') + '</pre>');
      } else {
        $('#bwdGlassdoorDataStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No company info found with given ID.');
      }
      $('#bwdGlassdoorDataTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Glassdoor.getCompanyInfo(). Error:', error);
      $('#bwdGlassdoorDataTableBody').html('');
      $('#bwdGlassdoorDataStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting company info from Glassdoor: ' + (error.title || error.error));
    });
};

window.getCompanies = function() {
  window.event.preventDefault();

  var params = null;
  if ($('#companiesLimit').val()) {
    params = {
      range: $('#companiesLimit').val()
    };
  }

  $('#bwdCompaniesTableBody').html('');
  $('#bwdCompaniesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.get(params)
    .then(function(results) {
      console.log('BWD.Companies.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'name', 'url']);
        }

        $('#bwdCompaniesTableBody').html(tableHtml);
        $('#bwdCompaniesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received companies.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies', params, 'GET') + '</pre>');
      } else {
        $('#bwdCompaniesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No companies was found.');
      }
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.get(). Error: ',
        error);
      $('#bwdCompaniesTableBody').html('');
      $('#bwdCompaniesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching companies: ' + (error.title || error.error));
    });
};

window.getCompany = function() {
  window.event.preventDefault();

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsTableBody').html('');
  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.getById(companyId)
    .then(function(results) {
      console.log('BWD.Companies.getById() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        var company = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
                str += objToTable(value, key);
                return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(company, '');
        $('#bwdCompanyStatsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received company.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId, null, 'GET') + '</pre>');
      } else {
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-warning')
            .text('No company found with given ID.');
      }
      $('#bwdCompanyStatsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.getById(). Error: ',
        error);
      $('#bwdCompanyStatsTableBody').html('');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the company: ' + (error.title || error.error));
    });
};

window.setFavoriteCompany = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.setFavorite(companyId)
    .then(function(results) {
      console.log('BWD.Companies.setFavorite() performed successfully.');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set company favorite.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/favorite', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.setFavorite(). Error: ',
        error);
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the company favorite: ' + (error.title || error.error));
    });
};

window.askForHelpOnCompany = function(message) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var query = {
    message: message
  };

  BWD.Companies.askForHelp(companyId, query)
    .then(function(results) {
      console.log('BWD.Companies.askForHelp() performed successfully.');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully ask for help.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/ask', query, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.askForHelp(). Error: ',
        error);
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when asking for help: ' + (error.title || error.error));
    });
};

window.setPassCompany = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.setPass(companyId)
    .then(function(results) {
      console.log('BWD.Companies.setPass() performed successfully.');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set company favorite.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/favorite', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
        console.log('An error occurred when invoking BWD.Companies.setPass(). Error: ',
          error);
        $('#bwdCompanyStatsStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('An error occurred when setting the company passed: ' + (error.title || error.error));
    });
};

window.checkCompanyStats = function(stats) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  switch(stats) {
    case 'favorite':
      BWD.Companies.isFavorite(companyId)
        .then(function(results) {
          console.log('BWD.Companies.isFavorite() performed successfully.');
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/favorite', null, 'GET') + '</pre>');
          var result = results.data.shift();
          $('#bwdCompanyStatsInfo')
            .attr('class', 'alert')
            .addClass('alert-info')
            .text('Favorite - ' + result.stat);
        })
        .catch(function(error) {
          console.log('An error occurred when invoking BWD.Companies.isFavorite(). Error: ',
            error);
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting the company favorite status: ' + (error.title || error.error));
        });
      break;
    case 'pass':
      BWD.Companies.isPass(companyId)
        .then(function(results) {
          console.log('BWD.Companies.isPass() performed successfully.');
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/pass', null, 'GET') + '</pre>');
          var result = results.data.shift();
          $('#bwdCompanyStatsInfo')
            .attr('class', 'alert')
            .addClass('alert-info')
            .text('Pass - ' + result.stat);
        })
        .catch(function(error) {
          console.log('An error occurred when invoking BWD.Companies.isPass(). Error: ',
            error);
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting the company passed status: ' + (error.title || error.error));
        });
      break;
    case 'view':
      BWD.Companies.isViewed(companyId)
        .then(function(results) {
          console.log('BWD.Companies.isViewed() performed successfully.');
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/viewed', null, 'GET') + '</pre>');
          var result = results.data.shift();
          $('#bwdCompanyStatsInfo')
            .attr('class', 'alert')
            .addClass('alert-info')
            .text('Viewed - ' + result.stat);
        })
        .catch(function(error) {
          console.log('An error occurred when invoking BWD.Companies.isViewed(). Error: ',
            error);
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting the company viewed status: ' + (error.title || error.error));
        });
      break;
    case 'preview':
      BWD.Companies.isPreviewed(companyId)
        .then(function(results) {
          console.log('BWD.Companies.isPreviewed() performed successfully.');
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/previewed', null, 'GET') + '</pre>');
          var result = results.data.shift();
          $('#bwdCompanyStatsInfo')
            .attr('class', 'alert')
            .addClass('alert-info')
            .text('Previewed - ' + result.stat);
        })
        .catch(function(error) {
          console.log('An error occurred when invoking BWD.Companies.isPreviewed(). Error: ',
            error);
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting the company previewed status: ' + (error.title || error.error));
        });
      break;
    case 'follow':
      BWD.Companies.isFollowing(companyId)
        .then(function(results) {
          console.log('BWD.Companies.isFollowing() performed successfully.');
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/follow', null, 'GET') + '</pre>');
          var result = results.data.shift();
          $('#bwdCompanyStatsInfo')
            .attr('class', 'alert')
            .addClass('alert-info')
            .text('Following - ' + result.follow);
        })
        .catch(function(error) {
          console.log('An error occurred when invoking BWD.Companies.isFollowing(). Error: ',
            error);
          $('#bwdCompanyStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-danger')
            .text('An error occurred when getting the company follow status: ' + (error.title || error.error));
        });
      break;
  }
};

window.setViewedCompany = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.setViewed(companyId)
    .then(function(results) {
      console.log('BWD.Companies.setViewed() performed successfully.');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set company viewed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/viewed', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.setViewed(). Error: ',
        error);
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the company viewed: ' + (error.title || error.error));
    });
};

window.setPreviewedCompany = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.setPreviewed(companyId)
    .then(function(results) {
      console.log('BWD.Companies.setPreviewed() performed successfully.');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set company previewed.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/previewed', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.setPreviewed(). Error: ',
        error);
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the company previewed: ' + (error.title || error.error));
    });
};

window.followCompany = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#companyId').val();

  $('#bwdCompanyStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Companies.follow(companyId)
    .then(function(results) {
      console.log('BWD.Companies.follow() performed successfully.');
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully followed company.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/follow', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Companies.follow(). Error: ',
        error);
      $('#bwdCompanyStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when following the company: ' + (error.title || error.error));
    });
};

window.getSchools = function() {
  window.event.preventDefault();

  var params = null;
  if ($('#schoolsLimit').val()) {
    params = {
      range: $('#schoolsLimit').val()
    };
  }

  $('#bwdSchoolsTableBody').html('');
  $('#bwdSchoolsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Schools.get(params)
    .then(function(results) {
      console.log('BWD.Schools.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'name']);
        }

        $('#bwdSchoolsTableBody').html(tableHtml);
        $('#bwdSchoolsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received schools.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('schools', params, 'GET') + '</pre>');
      } else {
        $('#bwdSchoolsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No schools was found.');
      }
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Schools.get(). Error: ',
        error);
      $('#bwdSchoolsTableBody').html('');
      $('#bwdSchoolsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when fetching schools: ' + (error.title || error.error));
    });
};

window.getSchool = function() {
  window.event.preventDefault();

  var schoolId = $('#schoolId').val();

  $('#bwdSchoolStatsTableBody').html('');
  $('#bwdSchoolStatsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Schools.getById(schoolId)
    .then(function(results) {
      console.log('BWD.Schools.getById() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        var school = results.data[0];
        function isScalar(obj) {
          return (/string|number|boolean/).test(typeof obj);
        }
        function objToTable(entity, prefix) {
          var str = '';
          $.each(entity, function(key, value) {
            key = (prefix.length) ? prefix + '.' + key : key;
            if (value && !isScalar(value)) {
                str += objToTable(value, key);
                return;
            }
            str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
          });
          return str;
        }
        tableHtml += objToTable(school, '');
        $('#bwdSchoolStatsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received school.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('schools/' + schoolId, null, 'GET') + '</pre>');
      } else {
          $('#bwdSchoolStatsStatus')
            .attr('class', 'alert')
            .addClass('alert-warning')
            .text('No school found with given ID.');
      }
      $('#bwdSchoolStatsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Schools.getById(). Error: ',
        error);
      $('#bwdSchoolStatsTableBody').html('');
      $('#bwdSchoolStatsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting the school: ' + (error.title || error.error));
    });
};

window.setMessagesRead = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var ids = $('#messageIds').val();

  $('#bwdMessagesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Messaging.readMessages(ids)
    .then(function(results) {
      console.log('BWD.Messaging.readMessages() performed successfully.');
      $('#bwdMessagesStatus')
        .attr('class', 'alert')
        .addClass('alert-success')
        .html('Successfully set messages as read.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('messageIds/' + ids + '/read', null, 'POST') + '</pre>');
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Messaging.readMessages(). Error: ',
        error);
      $('#bwdMessagesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when setting the messages as read: ' + (error.title || error.error));
    });
};

window.checkMessagesRead = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyStatsStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var ids = $('#messageIds').val();

  $('#bwdMessagesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

    BWD.Messaging.isMessagesRead(ids)
      .then(function(results) {
        console.log('BWD.Messaging.isMessagesRead() performed successfully.');
        $('#bwdMessagesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('This request can be performed with curl using the following options:<pre>' + window.mockCurl('messages/' + ids + '/read', null, 'GET') + '</pre>');

        var stats = [].concat(results.data);

        $('#bwdMessagesStatsInfo')
          .attr('class', 'alert')
          .addClass('alert-info')
          .text('Read - ' + stats.map(function(item) {
            return item.stat;
          }));
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Messaging.isMessagesRead(). Error: ',
          error);
        $('#bwdMessagesStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('An error occurred when getting the messages read status: ' + (error.title || error.error));
      });
};

window.getOrgs = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdOrgsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdOrgsTableBody').html('');
  $('#bwdOrgsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Orgs.get()
    .then(function(results) {
      console.log('BWD.Orgs.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'title', 'inviteCode']);
        }

        $('#bwdOrgsStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received a list of organizations.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('orgs', null, 'GET') + '</pre>');
      } else {
        $('#bwdOrgsStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No organizations found.');
      }
      $('#bwdOrgsTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Orgs.get(). Error: ',
        error);
      $('#bwdOrgsTableBody').html('');
      $('#bwdOrgsStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting organizations: ' + (error.title || error.error));
    });
};

window.getOrg = function() {
  window.event.preventDefault();

  var orgId = $('#orgId').val();

  $('#bwdOrgDetailsTableBody').html('');
  $('#bwdOrgDetailsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  if (orgId) {
    BWD.Orgs.getById(orgId)
      .then(function(results) {
        console.log('BWD.Orgs.getById() returned successfully. Results: ', results);
        var tableHtml;
        if (results.data.length) {
          var org = results.data[0];
          function isScalar(obj) {
            return (/string|number|boolean/).test(typeof obj);
          }
          function objToTable(entity, prefix) {
            var str = '';
            $.each(entity, function(key, value) {
              key = (prefix.length) ? prefix + '.' + key : key;
              if (value && !isScalar(value)) {
                str += objToTable(value, key);
                return;
              }
              str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
            });
            return str;
          }
          tableHtml += objToTable(org, '');
          $('#bwdOrgDetailsStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('Successfully received organization details.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('orgs/' + orgId, null, 'GET') + '</pre>');
        } else {
          $('#bwdOrgDetailsStatus')
            .attr('class', 'alert')
            .addClass('alert-warning')
            .text('No organization found with given ID.');
        }
        $('#bwdOrgDetailsTableBody').html(tableHtml);
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Org.getById(). Error: ',
          error);
        $('#bwdOrgDetailsTableBody').html('');
        $('#bwdOrgDetailsStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('An error occurred when getting the org: ' + (error.title || error.error));
      });
  } else {
    $('#bwdOrgDetailsStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('The organization id is required.');
  }
};

window.getAssessmentInfo = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdAssessmentStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdAssessmentStatusesTableBody').html('');
  $('#bwdAssessmentResultsTableBody').html('');
  $('#bwdAssessmentStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Users.getAssessmentInfo()
    .then(function(results) {
      console.log('BWD.Users.getAssessmentInfo() returned successfully. Results: ', results);
      var assessmentStatusesTableHtml;
      var assessmentStatuses = [];
      if (results.data[0] && results.data[0].statuses && results.data[0].statuses.length) {
        assessmentStatuses = results.data[0].statuses;
      }
      if (assessmentStatuses.length) {
        for (var i = 0; i < assessmentStatuses.length; i++) {
          assessmentStatusesTableHtml += objToTableRow(assessmentStatuses[i], ['sequence', 'title', 'completeDate']);
        }
      }
      $('#bwdAssessmentStatusesTableBody').html(assessmentStatusesTableHtml);

      var assessmentResultsTableHtml;
      var assessmentResults = {};
      if (results.data[0] && results.data[0].results) {
        assessmentResults = results.data[0].results;
      }

      var professionalValues = null;
      var professionalValuesKey = 'Professional Values';
      if (assessmentResults[professionalValuesKey]) {
        professionalValues = assessmentResults[professionalValuesKey];
      }
      if (professionalValues) {
        professionalValues = professionalValues.reduce(function(values, value) {
          return values.concat([value[0]]);
        }, []);
      }
      assessmentResultsTableHtml += objToTableRow(
        { label: professionalValuesKey, values: professionalValues.join(', ') },
        ['label', 'values']
      );

      var assessmentResultsLabels = ['Culture Preferences', 'Educational Keys',
        'Key Strengths', 'Leadership Anchors', 'True North'];
      for (var i = 0; i < assessmentResultsLabels.length; i++) {
        var label = assessmentResultsLabels[i];
        if (assessmentResults[label]) {
          var assessmentResult = {
            label: label,
            values: assessmentResults[label].join(', '),
          };
        }
        assessmentResultsTableHtml += objToTableRow(assessmentResult, ['label', 'values']);
      }
      $('#bwdAssessmentResultsTableBody').html(assessmentResultsTableHtml);

      if (assessmentStatuses.length || Object.keys(assessmentResults).length) {
        $('#bwdAssessmentStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received an assessment info.<br/><br/>This request can be performed with curl using the following options:<pre>' + window.mockCurl('user-assessments/me', null, 'GET') + '</pre>');
      }
    })
    .catch(function(error) {
      if (error.status === 404) {
        $('#bwdAssessmentStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('Assessment not found. User not started assessment yet.');
      } else {
        console.log('An error occurred when invoking Users.getAssessmentInfo(). Error: ',
          error);
        $('#bwdAssessmentStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('An error occurred when getting assessment info: ' + (error.title || error.error));
      }
      $('#bwdAssessmentStatusesTableBody').html('');
      $('#bwdAssessmentResultsTableBody').html('');
    });
};

window.getCompanyMatchStatus = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyMatchStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#compId').val();

  if (companyId) {
    $('#bwdCompanyMatchTableBody').html('');
    $('#bwdCompanyMatchStatus')
      .attr('class', 'alert')
      .addClass('alert-warning')
      .text('Loading...');

    BWD.Companies.getMatchStatus(companyId)
      .then(function(results) {
        console.log('BWD.Companies.getMatchStatus() returned successfully. Results: ', results);
        var tableHtml;
        if (results.data.length) {
          var match = results.data[0];
          function isScalar(obj) {
            return (/string|number|boolean/).test(typeof obj);
          }
          function objToTable(entity, prefix) {
            var str = '';
            $.each(entity, function(key, value) {
              key = (prefix.length) ? prefix + '.' + key : key;
              if (value && !isScalar(value)) {
                str += objToTable(value, key);
                return;
              }
              str += '<tr><td>' + key + '</td><td>' + value + '</td></tr>';
            });
            return str;
          }
          tableHtml += objToTable(match, '');
          $('#bwdCompanyMatchStatus')
            .attr('class', 'alert')
            .addClass('alert-success')
            .html('Successfully received a match.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/match', null, 'GET') + '</pre>');
        } else {
          $('#bwdCompanyMatchStatus')
            .attr('class', 'alert')
            .addClass('alert-warning')
            .text('No match found with given company ID.');
        }
        $('#bwdCompanyMatchTableBody').html(tableHtml);
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Companies.getMatchStatus(). Error: ',
          error);
        $('#bwdCompanyMatchTableBody').html('');
        $('#bwdCompanyMatchStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('An error occurred when getting a match: ' + (error.title || error.error));
      });
  } else {
    $('#bwdCompanyMatchStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('The company id is required.');
  }
};

window.updateInterestOnCompanyMatch = function(interest) {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyMatchStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  var companyId = $('#compId').val();

  if (companyId) {
    $('#bwdCompanyMatchStatus')
      .attr('class', 'alert')
      .addClass('alert-warning')
      .text('Loading...');

    var data = {
      interest: interest
    };

    BWD.Companies.updateMatch(companyId, data)
      .then(function(results) {
        console.log('BWD.Companies.updateMatch() performed successfully.');
        $('#bwdCompanyMatchStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully updated interest on match.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('companies/' + companyId + '/match', data, 'POST') + '</pre>');
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Companies.updateMatch(). Error: ',
          error);
        $('#bwdCompanyMatchStatus')
          .attr('class', 'alert')
          .addClass('alert-danger')
          .text('An error occurred when updating the math: ' + (error.title || error.error));
      });
  } else {
    $('#bwdCompanyMatchStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('The company id is required.');
  }
};

window.getCompanyMatches = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompanyMatchesStatus')
      .attr('class', 'alert')
      .addClass('alert-danger')
      .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdCompanyMatchesTableBody').html('');
  $('#bwdCompanyMatchesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  var limit = $('#matchesLimit').val();

  var query = {
    range: limit,
  };

  BWD.Matches.getCompanies(query)
    .then(function(results) {
      console.log('BWD.Matches.getCompanies() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['type', 'id', 'source', 'score']);
        }

        $('#bwdCompanyMatchesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received company matches.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('matches/company', query, 'GET') + '</pre>');
      } else {
        $('#bwdCompanyMatchesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No matches found.');
      }
      $('#bwdCompanyMatchesTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Matches.getCompanies(). Error: ',
        error);
      $('#bwdCompanyMatchesTableBody').html('');
      $('#bwdCompanyMatchesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting matches: ' + (error.title || error.error));
    });
};

window.getCompetencies = function() {
  window.event.preventDefault();

  if (!isAuthenticated()) {
    $('#bwdCompetenciesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('You must get an API token before you can use this method.');
    return;
  }

  $('#bwdCompetenciesTableBody').html('');
  $('#bwdCompetenciesStatus')
    .attr('class', 'alert')
    .addClass('alert-warning')
    .text('Loading...');

  BWD.Competencies.get()
    .then(function(results) {
      console.log('BWD.Competencies.get() returned successfully. Results: ', results);
      var tableHtml;
      if (results.data.length) {
        for (var i = 0; i < results.data.length; i++) {
          tableHtml += objToTableRow(results.data[i], ['id', 'name', 'competencyType', 'articleRef']);
        }

        $('#bwdCompetenciesStatus')
          .attr('class', 'alert')
          .addClass('alert-success')
          .html('Successfully received a list of competencies.<br><br>This request can be performed with curl using the following options:<pre>' + window.mockCurl('competencies', null, 'GET') + '</pre>');
      } else {
        $('#bwdCompetenciesStatus')
          .attr('class', 'alert')
          .addClass('alert-warning')
          .text('No competencies found.');
      }
      $('#bwdCompetenciesTableBody').html(tableHtml);
    })
    .catch(function(error) {
      console.log('An error occurred when invoking BWD.Competencies.get(). Error: ',
        error);
      $('#bwdCompetenciesTableBody').html('');
      $('#bwdCompetenciesStatus')
        .attr('class', 'alert')
        .addClass('alert-danger')
        .text('An error occurred when getting competencies: ' + (error.title || error.error));
    });
};

window.mockCurl = function(url, params, method) {
  var cmd = 'curl ';
  if (method) {
    cmd += '-X ' + method + ' ';
  }
  var backendEnv = $('#backEnv').val();
  if (backendEnv) {
    cmd += '-H "Backend: ' + backendEnv + '" ';
  }
  var token = '';
  if (BWD.Config.get('AccessToken') && BWD.Config.get('AccessToken').access_token) {
    token = BWD.Config.get('AccessToken').access_token;
  } else if (BWD.Config.get('AccessToken') && BWD.Config.get('AccessToken').id_token) {
    token = 'JWT ' + BWD.Config.get('AccessToken').id_token;
  }
  if (token) {
    cmd += '-H "Authorization: Bearer ' + token + '" ';
  }

  var url = BWD.Client.buildUrl(url);
  var data = '';
  for (var key in params) {
    if (params.hasOwnProperty(key)) {
      if (data.length) {
        data += '&';
      }
      data += key.replace(/\[/g, '\\[').replace(/\]/g, '\\]') + '=' + params[key];
    }
  }
  if (!data.length) {
    cmd += url;
  } else if (method === 'GET' || !method) {
    cmd += '"' + url + '?' + data + '"';
  } else {
    cmd += url + ' -d "' + data + '"';
  }
  return cmd;
};

window.mockCurlToken = function() {
  var headerAuth = 'Basic ' + btoa(BWD.Config.get('APIKey') + ':' +
    BWD.Config.get('APISecret'));
  var cmd = 'curl -XPOST -H "Authorization: ' + headerAuth +
    '" -H "Content-Type: application/x-www-form-urlencoded" ' +
    BWD.Client.buildUrl('oauth/token') + ' -d "grant_type=client_credentials"';
  return cmd;
};

// Shared helpers

function objToTableRow(data, props) {
  var columns = [];
  props.forEach(function(prop) {
    if (data[prop]) {
      columns.push('<td>' + data[prop] + '</td>');
    } else {
      columns.push('<td></td>');
    }
  });
  return '<tr>' + columns.join() + '</tr>';
}

function isAuthenticated() {
  var token = BWD.Config.get('AccessToken');
  return token && (token.access_token || token.id_token);
};

// Check for a hash parameter
var anchors = ['home', 'access-token', 'jobs', 'job'];
var hash = window.location.hash;
var matches = hash ? hash.match(/^#(\w+),(\w+)/) : false;
if (matches && matches.length === 3 && anchors.indexOf(matches[1]) === -1) {
  // Assume this is an access token returned as a callback from the API.
  var token = {access_token: matches[1]};
  BWD.Config.set('AccessToken', token);
  BWD.Config.set('APIEnv', matches[2]);
  $('#apiKey').val(matches[2]);
  $('#apiKey,#apiSecret,#env').attr('readonly', 'readonly');
  $('#accessToken').val(token.access_token);
  $('.nav-pills a[href="#access-token"]').tab('show');
  window.location.hash = '';
}
