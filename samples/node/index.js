var APIKey = process.argv[2] || process.env.BWD_API_KEY;
var APISecret = process.argv[3] || process.env.BWD_API_SECRET;

if (!APIKey || !APIKey.length || !APISecret || !APISecret.length) {
  throw new Error('API key and secret are required. Provide them as ' +
    'command-line arguments or in the BWD_API_KEY and BWD_API_SECRET ' +
    'environment variables.');
}

var BWD = require('../../')({
  APIKey: APIKey,
  APISecret: APISecret
});

var params = {
  range: 100
};

BWD.Client.getAccessToken()
  .then(function() {
    BWD.Jobs.get(params)
      .then(function(results) {
        console.log('BWD.Jobs.get() returned successfully. Results:\n');
        console.log(strTrimPad('ID', 12) + ' ' + strTrimPad('DESCRIPTION', 30) +
          ' ' + strTrimPad('COMPANY', 20) + ' ' + strTrimPad('LOCATION', 15));
        console.log(strTrimPad('', 12, '-') + ' ' + strTrimPad('', 30, '-') +
          ' ' + strTrimPad('', 20, '-') + ' ' + strTrimPad('', 15, '-'));
        var i;
        var num = results.data.length;
        for (i = 0; i < num; i++) {
          var job = results.data[i];
          var company = job.company ? job.company.name : '';
          var location = job.location ? job.location.name : '';
          console.log(strTrimPad(job.id, 12) + ' ' + strTrimPad(job.title, 30) +
            ' ' + strTrimPad(company, 20) + ' ' + strTrimPad(location, 15));
        }
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Jobs.get(). Error: ',
          error);
      });
  })
  .then(function() {
    BWD.Companies.get(params)
      .then(function(results) {
        console.log('BWD.Companies.get() returned successfully. Results:\n');
        console.log(strTrimPad('ID', 12) + ' ' + strTrimPad('COMPANY', 30) +
          ' ' + strTrimPad('LOCATION', 20) + ' ' + strTrimPad('EMPLOYEES', 15));
        console.log(strTrimPad('', 12, '-') + ' ' + strTrimPad('', 30, '-') +
          ' ' + strTrimPad('', 20, '-') + ' ' + strTrimPad('', 15, '-'));
        var i;
        var num = results.data.length;
        for (i = 0; i < num; i++) {
          var company = results.data[i];
          var companyName = company.name ? company.name : '';
          var location = company.location ? company.location : '';
          var employees = company.employees ? company.employees : '';
          console.log(strTrimPad(company.id, 12) + ' ' + strTrimPad(companyName, 30) +
            ' ' + strTrimPad(location, 20) + ' ' + strTrimPad(employees, 15));
        }
      })
      .catch(function(error) {
        console.log('An error occurred when invoking BWD.Companies.get(). Error: ',
          error);
      });
  });

function strTrimPad(str, len, ch) {
  str = String(str);
  if (str.length > len) {
    str = str.substr(0, len - 3) + '...';
  } else if (str.length < len) {
    var i = -1;
    if (!ch && ch !== 0) {
      ch = ' ';
    }
    len -= str.length;
    while (++i < len) {
      str += ch;
    }
  }
  return str;
}
