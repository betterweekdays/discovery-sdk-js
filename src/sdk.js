/**
 * Better Weekdays Discovery SDK for JavaScript
 *
 * A browser- and Node.js-compatible JavaScript SDK for integrating with the
 * Better Weekdays Discovery API.
 *
 * In browser, access the API using the global `window.BWD`.
 *
 * For Node.js, load this module using:
 *   `var BWD = require('@betterweekdays/discovery-sdk-js');`
 */

var BWD = function(config) {
  BWD.Config.set(config);
  return BWD;
};

BWD.AuthType = {
  TOKEN: 'token',
  COOKIE: 'cookie'
};

BWD.DefaultConfig = {
  AccessToken: null,
  APIEnv: 'test',
  APIHost: 'thewhether-[env].apigee.net',
  APIKey: '',
  APISecret: '',
  APIBaseUrl: 'discovery',
  Auth0Domain: 'betterweekdays.auth0.com',
  Auth0ID: '',
  Auth0Connection: 'Username-Password-Authentication',
  Auth0RefreshCallbackURL: null,
  Auth0RefreshFunction: null,
  Auth0RefreshUsePostMessage: false,
  Auth0TokenExpires: null,
  BackendEnv: null,
  AuthType: BWD.AuthType.TOKEN,
  UseSSL: true
};

BWD.Client = new (require('./services/Client.js'))(BWD);
BWD.Companies = new (require('./services/Companies.js'))(BWD);
BWD.Config = new (require('./services/Config.js'))(BWD);
BWD.Jobs = new (require('./services/Jobs.js'))(BWD);
BWD.Users = new (require('./services/Users.js'))(BWD);
BWD.Favorites = new (require('./services/Favorites.js'))(BWD);
BWD.Feed = new (require('./services/Feed.js'))(BWD);
BWD.Interactions = new (require('./services/Interactions'))(BWD);
BWD.Messaging = new (require('./services/Messaging'))(BWD);
BWD.Orgs = new (require('./services/Orgs'))(BWD);
BWD.Locations = new (require('./services/Locations'))(BWD);
BWD.Cips = new (require('./services/Cips'))(BWD);
BWD.JobAreas = new (require('./services/JobAreas'))(BWD);
BWD.Schools = new (require('./services/Schools'))(BWD);
BWD.DegreeSpecializations =
  new (require('./services/DegreeSpecializations'))(BWD);
BWD.DegreePrograms = new (require('./services/DegreePrograms'))(BWD);
BWD.Articles = new (require('./services/Articles'))(BWD);
BWD.Auth = new (require('./services/Auth'))(BWD);
BWD.Stats = new (require('./services/Stats'))(BWD);
BWD.Onets = new (require('./services/Onets'))(BWD);

BWD.GlassdoorClient = new (require('./services/GlassdoorClient.js'))(BWD);
BWD.Glassdoor = new (require('./services/Glassdoor.js'))(BWD);

BWD.Search = new (require('./services/Search.js'))(BWD);
BWD.Courses = new (require('./services/Courses.js'))(BWD);
BWD.Competencies = new (require('./services/Competencies.js'))(BWD);

BWD.Advice = new (require('./services/Advice.js'))(BWD);
BWD.Matches = new (require('./services/Matches.js'))(BWD);

BWD.Teams = new (require('./services/Teams.js'))(BWD);
BWD.Comment = new (require('./services/Comment.js'))(BWD);
BWD.Assessment = new (require('./services/Assessment.js'))(BWD);

BWD.Invite = new (require('./services/Invite.js'))(BWD);
BWD.Firebase = new (require('./services/Firebase.js'))(BWD);

BWD.Group = new (require('./services/Group.js'))(BWD);

module.exports = BWD;
