var interceptor = require('rest/interceptor');
var when = require('when');
var includes = require('lodash/includes');

/**
 * Retries a rejected request using an exponential backoff.
 * Do not retry rejected request with error codes specified in except option,
 * 404 by default.
 *
 * Defaults to an initial interval of 100ms, a multiplier of 2, and no max interval.
 *
 * @param {Client} [client] client to wrap
 * @param {number} [config.intial=100] initial interval in ms
 * @param {number} [config.multiplier=2] interval multiplier
 * @param {number} [config.max] max interval in ms
 * @param {number} [config.except] error code that should be excluded
 *
 * @returns {Client}
 */

var retry = interceptor({
  init: function(config) {
    config.initial = config.initial || 100;
    config.multiplier = config.multiplier || 2;
    config.max = config.max || Infinity;
    config.except = config.except || [400, 404, 422];
    return config;
  },
  error: function(response, config, meta) {
    var request;
    var TIMEOUT = 30e3;

    request = response.request;
    request.retry = request.retry || config.initial;

    if (response.status &&
      response.status.code &&
      includes(config.except, response.status.code)) {
      return when.reject(response.entity);
    }

    // to do not wait for be cancelled by timeout
    if (request.retry >= TIMEOUT) {
      return when.reject(response.entity);
    }

    return when(request).delay(request.retry).then(function(request) {
      if (request.canceled) {
        // cancel here in case client doesn't check canceled flag
        return when.reject({request: request, error: 'precanceled'});
      }
      request.retry = Math.min(request.retry * config.multiplier, config.max);
      return meta.client(request);
    });
  }
});

module.exports = retry;
