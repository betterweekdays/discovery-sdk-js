var BWD;

var BWDGroup = function(_BWD) {
  BWD = _BWD;
};

BWDGroup.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/group/' + id),
    params: query
  };

  return BWD.Client.request(options);
};
BWDGroup.prototype.getById = BWDGroup.prototype.getByIdV10;

BWDGroup.prototype.getMeV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/group/me'),
    params: query
  };

  return BWD.Client.request(options);
};
BWDGroup.prototype.getMe = BWDGroup.prototype.getMeV10;

BWDGroup.prototype.createGroupV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/group'),
    entity: data
  };

  return BWD.Client.request(options);
};
BWDGroup.prototype.createGroup = BWDGroup.prototype.createGroupV10;

BWDGroup.prototype.updateGroupV10 = function(id, data) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/group/' + id),
    entity: data
  };

  return BWD.Client.request(options);
};
BWDGroup.prototype.updateGroup = BWDGroup.prototype.updateGroupV10;

BWDGroup.prototype.joinGroupV10 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/group/' + id + '/join'),
    entity: data
  };

  return BWD.Client.request(options);
};
BWDGroup.prototype.joinGroup = BWDGroup.prototype.joinGroupV10;

BWDGroup.prototype.leaveGroupV10 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/group/' + id + '/leave'),
    entity: data
  };

  return BWD.Client.request(options);
};
BWDGroup.prototype.leaveGroup = BWDGroup.prototype.leaveGroupV10;

BWDGroup.prototype.getFeedV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/group-feed'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDGroup.prototype.getFeed = BWDGroup.prototype.getFeedV10;

module.exports = BWDGroup;
