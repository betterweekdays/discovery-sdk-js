var BWD;
var IdTokenVerifier;

var BWDAuth = function(_BWD) {
  BWD = _BWD;
  IdTokenVerifier = new (require('idtoken-verifier'))({
    issuer: 'https://' + BWD.Config.get('Auth0Domain') + '/',
    audience: BWD.Config.get('Auth0ID')
  });
};

var init = function() {
  if (!BWD.Auth0) {
    BWD.Auth0 = new (require('auth0-js')).WebAuth({
      domain: BWD.Config.get('Auth0Domain'),
      clientID: BWD.Config.get('Auth0ID')
    });
  }
};

BWDAuth.prototype.reset = function() {
  BWD.Auth0 = null;
};

BWDAuth.prototype.login = function(email, password) {
  init();
  return new Promise(function(resolve, reject) {
    BWD.Auth0.client.login({
      realm: BWD.Config.get('Auth0Connection'),
      username: email,
      password: password,
      scope: 'openid'
    }, function(err, authResult) {
      if (err) {
        reject(err);
      } else {
        resolve(authResult);
      }
    });
  });
};

BWDAuth.prototype.signup = function(email, password) {
  init();
  return new Promise(function(resolve, reject) {
    BWD.Auth0.signupAndAuthorize({
      connection: BWD.Config.get('Auth0Connection'),
      email: email,
      password: password,
      scope: 'openid'
    }, function(error, result) {
      if (error) {
        reject(error);
      } else {
        resolve(result);
      }
    });
  });
};

BWDAuth.prototype.refreshToken = function() {
  var refreshTokenFunction = BWD.Config.get('Auth0RefreshFunction');
  if (refreshTokenFunction) {
    return refreshTokenFunction();
  }

  init();
  return new Promise(function(resolve, reject) {
    var options = {
      responseType: 'token',
      scope: 'openid',
      redirectUri: window.location.origin // eslint-disable-line no-undef
    };
    var callbackURL = BWD.Config.get('Auth0RefreshCallbackURL');
    if (callbackURL) {
      options.redirectUri = callbackURL;
    }
    var usePostMessage = BWD.Config.get('Auth0RefreshUsePostMessage');
    if (usePostMessage) {
      options.usePostMessage = usePostMessage;
    }

    BWD.Auth0.renewAuth(options, function(error, result) {
      if (error || result.error) {
        reject(error);
      } else {
        var idTokenDecoded = IdTokenVerifier.decode(result.idToken);
        if (idTokenDecoded && idTokenDecoded.payload) {
          BWD.Config.set({
            AccessToken: {
              id_token: result.idToken // eslint-disable-line camelcase
            },
            Auth0TokenExpires: new Date(idTokenDecoded.payload.exp * 1000)
          });
          resolve(result);
          return;
        }

        reject(
          new Error(
            'Received invalid response when attempting to refresh the token.'
          )
        );
      }
    });
  });
};

module.exports = BWDAuth;
