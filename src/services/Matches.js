var BWD;

var BWDMatches = function(_BWD) {
  BWD = _BWD;
};

BWDMatches.prototype.getCompanies = function(query) {
  var options = {
    path: BWD.Client.buildUrl('matches/company'),
    params: query
  };

  return BWD.Client.request(options);
};

module.exports = BWDMatches;
