var BWD;

var BWDSearch = function(_BWD) {
  BWD = _BWD;
};

BWDSearch.prototype.searchV10 = function(contentType, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/search/' + contentType),
    params: query
  };
  return BWD.Client.request(options);
};

BWDSearch.prototype.search = BWDSearch.prototype.searchV10;

module.exports = BWDSearch;
