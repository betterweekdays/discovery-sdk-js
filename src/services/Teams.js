var BWD;

var BWDTeams = function(_BWD) {
  BWD = _BWD;
};

BWDTeams.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/team/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDTeams.prototype.getById = BWDTeams.prototype.getByIdV10;

BWDTeams.prototype.getAggregateAssessmentResultsV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/team/' + id + '/aggregate'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDTeams.prototype.getAggregateAssessmentResults =
  BWDTeams.prototype.getAggregateAssessmentResultsV10;

BWDTeams.prototype.triggerAggregateResultsViewedEventV11 = function(id) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/team/' + id + '/aggregate/viewed')
  };

  return BWD.Client.request(options);
};

BWDTeams.prototype.triggerAggregateResultsViewedEvent =
  BWDTeams.prototype.triggerAggregateResultsViewedEventV11;

module.exports = BWDTeams;
