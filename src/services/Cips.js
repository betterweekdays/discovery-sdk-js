var BWD;

var BWDCips = function(_BWD) {
  BWD = _BWD;
};

BWDCips.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/cips'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDCips.prototype.get = BWDCips.prototype.getV10;

BWDCips.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/cips/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDCips.prototype.getById = BWDCips.prototype.getByIdV10;

BWDCips.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/cip'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDCips.prototype.autocomplete = BWDCips.prototype.autocompleteV10;

module.exports = BWDCips;
