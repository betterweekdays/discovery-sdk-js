var BWD;

var BWDMessaging = function(_BWD) {
  BWD = _BWD;
};

BWDMessaging.prototype.getMessagesV11 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/messages'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDMessaging.prototype.getMessages = BWDMessaging.prototype.getMessagesV11;

BWDMessaging.prototype.getMessageV11 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/messages/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDMessaging.prototype.getMessage = BWDMessaging.prototype.getMessageV11;

BWDMessaging.prototype.sendMessageV11 = function(message) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/messages'),
    entity: message
  };

  return BWD.Client.request(options);
};

BWDMessaging.prototype.sendMessage = BWDMessaging.prototype.sendMessageV11;

BWDMessaging.prototype.readMessagesV11 = function(ids) {
  var options = {
    method: 'POST',
    headers: {
      Accept: '*/*'
    },
    path: BWD.Client.buildUrl('v1.1/messages/' + ids + '/read')
  };
  return BWD.Client.request(options);
};

BWDMessaging.prototype.readMessages = BWDMessaging.prototype.readMessagesV11;

BWDMessaging.prototype.isMessagesReadV11 = function(ids) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/messages/' + ids + '/read')
  };
  return BWD.Client.request(options);
};

BWDMessaging.prototype.isMessagesRead =
  BWDMessaging.prototype.isMessagesReadV11;

module.exports = BWDMessaging;
