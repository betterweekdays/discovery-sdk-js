var BWD;

var BWDCompetencies = function(_BWD) {
  BWD = _BWD;
};

BWDCompetencies.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/competencies'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDCompetencies.prototype.get = BWDCompetencies.prototype.getV10;

BWDCompetencies.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/competencies/' + id),
    params: query
  };
  return BWD.Client.request(options);
};

BWDCompetencies.prototype.getById = BWDCompetencies.prototype.getByIdV10;

module.exports = BWDCompetencies;
