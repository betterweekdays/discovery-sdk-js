var BWD;

var BWDAssessment = function(_BWD) {
  BWD = _BWD;
};

BWDAssessment.prototype.getResultsByShareCodeV10 = function(code, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/user-assessments/share-code/' + code),
    params: query
  };
  return BWD.Client.request(options);
};

BWDAssessment.prototype.getResultsByShareCode =
  BWDAssessment.prototype.getResultsByShareCodeV10;

module.exports = BWDAssessment;
