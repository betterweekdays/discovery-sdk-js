var BWD;

var BWDGlassdoor = function(_BWD) {
  BWD = _BWD;
};

BWDGlassdoor.prototype.getCompanyInfoV10 = function(params, headers) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/glassdoor'),
    params: params
  };

  if (headers) {
    options.headers = headers;
  }

  return BWD.GlassdoorClient.request(options);
};

BWDGlassdoor.prototype.getCompanyInfo =
  BWDGlassdoor.prototype.getCompanyInfoV10;

module.exports = BWDGlassdoor;
