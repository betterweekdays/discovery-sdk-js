var BWD;

var BWDLocations = function(_BWD) {
  BWD = _BWD;
};

BWDLocations.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/locations'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDLocations.prototype.get = BWDLocations.prototype.getV10;

BWDLocations.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/locations/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDLocations.prototype.getById = BWDLocations.prototype.getByIdV10;

BWDLocations.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/location'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDLocations.prototype.autocomplete = BWDLocations.prototype.autocompleteV10;

module.exports = BWDLocations;
