var BWD;

var BWDOnets = function(_BWD) {
  BWD = _BWD;
};

BWDOnets.prototype.getV10 = function(query, func) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/onets'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDOnets.prototype.get = BWDOnets.prototype.getV10;

BWDOnets.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/onets/' + id),
    params: query
  };
  return BWD.Client.request(options);
};

BWDOnets.prototype.getById = BWDOnets.prototype.getByIdV10;

BWDOnets.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/onet'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDOnets.prototype.autocomplete = BWDOnets.prototype.autocompleteV10;

module.exports = BWDOnets;
