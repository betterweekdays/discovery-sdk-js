var BWD;

var BWDDegreeSpecializations = function(_BWD) {
  BWD = _BWD;
};

BWDDegreeSpecializations.prototype.autocomplete = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/cip'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

module.exports = BWDDegreeSpecializations;
