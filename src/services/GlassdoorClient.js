var BWD;

var BWDGlassdoorClient = function(_BWD) {
  BWD = _BWD;

  var interceptor = require('rest/interceptor');
  var when = require('when');
  var mime = require('rest/interceptor/mime');

  // Set up rest module with interceptor for providing auth key.
  var auth = interceptor({
    request: BWD.authInterceptor
  });

  // Set up rest module with interceptor for providing backend env.
  BWD.backendInterceptor = function(request) {
    var backendEnv = BWD.Config.get('BackendEnv');
    if (backendEnv) {
      if (!request.headers) {
        request.headers = {};
      }
      request.headers.Backend = backendEnv;
    }
    return request;
  };
  var backend = interceptor({
    request: BWD.backendInterceptor
  });

  // Set interceptor that retry request for glassdoor data
  // if the glassdoor data is not yet available in the db
  BWD.ensureGlassdoorDataInterceptor = function(response, config, meta) {
    var request = response.request;
    request.time = request.time || 1;

    if (response.entity.data && response.entity.data[0] &&
    response.entity.data[0].status === 'REQUESTED') {
      if (request.time <= config.max) {
        return when(request).delay(config.space).then(function(request) {
          request.time++;
          return meta.client(request);
        });
      }
      throw new Error('Cannot get Glassdoor data. Status: ' +
        response.entity.data[0].status);
    }
    return response;
  };
  var ensureGlassdoorData = interceptor({
    init: function(config) {
      config.space = config.space || 3000;
      config.max = config.max || 3;
      return config;
    },
    response: BWD.ensureGlassdoorDataInterceptor
  });

  var retry = require('rest/interceptor/retry');
  var timeout = require('rest/interceptor/timeout');

  BWD.glassdoorRest = require('rest')
    .wrap(auth)
    .wrap(backend)
    .wrap(mime)
    .wrap(ensureGlassdoorData)
    .wrap(retry, {initial: 2e3, multiplier: 30e3, max: 30e3})
    .wrap(timeout, {timeout: 20e3});
};

BWDGlassdoorClient.prototype.request = function(options) {
  return BWD.glassdoorRest(options)
    .then(function(result) {
      if (!result || !result.status || !result.status.code) {
        throw new Error('Malformed request result.');
      } else if (result.status.code >= 400) {
        throw new Error('Request returned status code ' + result.status.code +
          ': ' + result.request.method + ' ' + result.request.path);
      }
      return result.entity;
    });
};

module.exports = BWDGlassdoorClient;
