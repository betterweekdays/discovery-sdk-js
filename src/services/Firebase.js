var BWD;

var BWDFirebase = function(_BWD) {
  BWD = _BWD;
};

BWDFirebase.prototype.subscribeV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/firebase/subscribe'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDFirebase.prototype.subscribe = BWDFirebase.prototype.subscribeV10;

module.exports = BWDFirebase;
