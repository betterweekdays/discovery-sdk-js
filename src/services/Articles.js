var BWD;

var BWDArticles = function(_BWD) {
  BWD = _BWD;
};

BWDArticles.prototype.getV12 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.2/articles'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.get = BWDArticles.prototype.getV12;

BWDArticles.prototype.getByIdV12 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.2/articles/' + id),
    params: query
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.getById = BWDArticles.prototype.getByIdV12;

BWDArticles.prototype.setFavoriteV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/favorite'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.setFavorite = BWDArticles.prototype.setFavoriteV11;

BWDArticles.prototype.isFavoriteV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/favorite')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.isFavorite = BWDArticles.prototype.isFavoriteV11;

BWDArticles.prototype.setPassV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/pass'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.setPass = BWDArticles.prototype.setPassV11;

BWDArticles.prototype.isPassV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/pass')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.isPass = BWDArticles.prototype.isPassV11;

BWDArticles.prototype.setPreviewedV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/previewed'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.setPreviewed = BWDArticles.prototype.setPreviewedV11;

BWDArticles.prototype.isPreviewedV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/previewed')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.isPreviewed = BWDArticles.prototype.isPreviewedV11;

BWDArticles.prototype.setViewedV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/viewed'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.setViewed = BWDArticles.prototype.setViewedV11;

BWDArticles.prototype.isViewedV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/viewed')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.isViewed = BWDArticles.prototype.isViewedV11;

BWDArticles.prototype.setReadMoreV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/read-more'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.setReadMore = BWDArticles.prototype.setReadMoreV11;

BWDArticles.prototype.isReadMoreV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/read-more')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.isReadMore = BWDArticles.prototype.isReadMoreV11;

BWDArticles.prototype.askForHelpV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/ask'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.askForHelp = BWDArticles.prototype.askForHelpV11;

BWDArticles.prototype.getAsksForHelpV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/articles/' + id + '/ask')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.getAsksForHelp = BWDArticles.prototype.getAsksForHelpV11;

BWDArticles.prototype.getCategoriesV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/articlecategories'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.getCategories = BWDArticles.prototype.getCategoriesV10;

BWDArticles.prototype.getCategoriesByIdV10 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/articlecategories/' + id)
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.getCategoriesById =
  BWDArticles.prototype.getCategoriesByIdV10;

BWDArticles.prototype.followCategoriesV10 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/articlecategories/' + id + '/follow'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.followCategories =
  BWDArticles.prototype.followCategoriesV10;

BWDArticles.prototype.isFollowingCategoriesV10 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/articlecategories/' + id + '/follow')
  };
  return BWD.Client.request(options);
};

BWDArticles.prototype.isFollowingCategories =
  BWDArticles.prototype.isFollowingCategoriesV10;

BWDArticles.prototype.createArticleV12 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.2/articles'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDArticles.prototype.createArticle =
  BWDArticles.prototype.createArticleV12;

BWDArticles.prototype.updateArticleV12 = function(id, data) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.2/articles/' + id),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDArticles.prototype.updateArticle =
  BWDArticles.prototype.updateArticleV12;

module.exports = BWDArticles;
