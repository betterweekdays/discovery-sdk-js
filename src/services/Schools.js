var BWD;

var BWDSchools = function(_BWD) {
  BWD = _BWD;
};

BWDSchools.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/schools'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDSchools.prototype.get = BWDSchools.prototype.getV10;

BWDSchools.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/schools/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDSchools.prototype.getById = BWDSchools.prototype.getByIdV10;

BWDSchools.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/school'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDSchools.prototype.autocomplete = BWDSchools.prototype.autocompleteV10;

module.exports = BWDSchools;
