var BWD;

var BWDInteractions = function(_BWD) {
  BWD = _BWD;
};

BWDInteractions.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/interactions'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDInteractions.prototype.get = BWDInteractions.prototype.getV10;

BWDInteractions.prototype.setDoneV10 = function(id) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/interactions/' + id),
    entity: {
      action: 'done'
    }
  };
  return BWD.Client.request(options);
};

BWDInteractions.prototype.setDone = BWDInteractions.prototype.setDoneV10;

BWDInteractions.prototype.setNotDoneV10 = function(id) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/interactions/' + id),
    entity: {
      action: 'not-done'
    }
  };
  return BWD.Client.request(options);
};

BWDInteractions.prototype.setNotDone = BWDInteractions.prototype.setNotDoneV10;

module.exports = BWDInteractions;
