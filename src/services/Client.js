var BWD;

var BWDClient = function(_BWD) {
  BWD = _BWD;

  var isTokenExpired = function() {
    var tokenExpires = BWD.Config.get('Auth0TokenExpires');
    return tokenExpires && tokenExpires.getTime() <= new Date().getTime();
  };

  BWD.isTokenExpired = isTokenExpired;
  BWD.tokenRefreshPromise = null;

  var mime = require('rest/interceptor/mime');

  // Set up rest module with interceptor for providing auth key.
  var interceptor = require('rest/interceptor');
  BWD.authInterceptor = function(request) {
    if (!request.headers || !request.headers.Authorization) {
      if (!request.headers) {
        request.headers = {};
      }
      var token = BWD.Config.get('AccessToken');
      if (token && token.access_token) {
        request.headers.Authorization = 'Bearer ' + token.access_token;
      } else if (token && token.id_token) {
        request.headers.Authorization = 'Bearer JWT ' + token.id_token;
      }
    }
    return request;
  };
  var auth = interceptor({
    request: BWD.authInterceptor
  });

  var retry = require('../interceptors/retry');
  var timeout = require('rest/interceptor/timeout');
  var errorCode = require('rest/interceptor/errorCode');

  BWD.rest = require('rest')
    .wrap(mime)
    .wrap(auth)
    .wrap(errorCode)
    .wrap(retry, {initial: 2e3, multiplier: 30e3, max: 30e3})
    .wrap(timeout, {timeout: 30e3});
};

BWDClient.prototype.buildUrl = function(path, base) {
  var baseUrl = base || BWD.Config.get('APIBaseUrl');
  var url = BWD.Config.get('UseSSL') ? 'https' : 'http';
  url += '://';
  url += BWD.Config.get('APIHost').replace('[env]', BWD.Config.get('APIEnv'));
  url += '/' + baseUrl;
  url += '/' + path;
  return url;
};

BWDClient.prototype.buildOAuthUrl = function(path) {
  return BWD.Client.buildUrl(path, 'oauth');
};

BWDClient.prototype.getAccessToken = function() {
  var base64 = require('rest/util/base64');
  var headerAuth = 'Basic ' + base64.encode(BWD.Config.get('APIKey') + ':' +
    BWD.Config.get('APISecret'));
  var options = {
    path: BWD.Client.buildOAuthUrl('token'),
    method: 'POST',
    entity: {
      grant_type: 'client_credentials' // eslint-disable-line camelcase
    },
    headers: {
      'Authorization': headerAuth,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  };
  return BWD.Client.request(options)
    .then(function(result) {
      BWD.Config.set({AccessToken: result});
      return result;
    });
};

BWDClient.prototype.getLogin = function() {
  return BWD.Client.buildOAuthUrl('login') + '?apiKey=' +
    BWD.Config.get('APIKey');
};

BWDClient.prototype.getLogout = function() {
  return BWD.Client.buildOAuthUrl('logout');
};

BWDClient.prototype.v3SpecificRequest = function(options) {
  var backendEnv = BWD.Config.get('BackendEnv');
  if (backendEnv) {
    if (!options.headers) {
      options.headers = {};
    }
    options.headers.Backend = backendEnv;
  }

  // The XMLHttpRequest specification does not explicitly define `send(undefined)`, but `send()
  // or `send(null)` are accepted. While most browsers treat `undefined` and `null` as acceptable,
  // IE 10/11 convert `undefined` into the body "undefined" with text/plain as the content type.
  if (options.method === 'POST' && options.entity === undefined) {
    options.entity = '{}';
  }

  if (BWD.isTokenExpired()) {
    if (BWD.tokenRefreshPromise) {
      return BWD.tokenRefreshPromise
        .then(function() {
          return BWDClient.prototype.request(options);
        });
    }
    BWD.tokenRefreshPromise = BWD.Auth.refreshToken()
      .then(function() {
        BWD.tokenRefreshPromise = null;
        return BWDClient.prototype.request(options);
      })
      .catch(function() {
        throw new Error('Unable to refresh expired token. Aborting request.');
      });
    return BWD.tokenRefreshPromise;
  }

  return BWD.rest(options)
    .then(function(result) {
      if (!result || !result.status || !result.status.code) {
        throw new Error('Malformed request result.');
      }
      return result.entity;
    });
};

BWDClient.prototype.baseRequest = function(options) {
  var token = BWD.Config.get('AccessToken');
  if (token && token.session_token) {
    if (!options.headers) {
      options.headers = {};
    }
    options.headers['X-CSRF-Token'] = token.session_token;
  }

  // The XMLHttpRequest specification does not explicitly define `send(undefined)`, but `send()
  // or `send(null)` are accepted. While most browsers treat `undefined` and `null` as acceptable,
  // IE 10/11 convert `undefined` into the body "undefined" with text/plain as the content type.
  if (options.method === 'POST' && options.entity === undefined) {
    options.entity = '{}';
  }

  return BWD.rest(options)
    .then(function(result) {
      if (!result || !result.status || !result.status.code) {
        throw new Error('Malformed request result.');
      }
      return result.entity;
    });
};

BWDClient.prototype.request = function(options) {
  return BWD.Config.get('AuthType') === BWD.AuthType.TOKEN ?
    BWDClient.prototype.v3SpecificRequest(options) :
    BWDClient.prototype.baseRequest(options);
};

module.exports = BWDClient;
