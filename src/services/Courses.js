var BWD;

var BWDCourses = function(_BWD) {
  BWD = _BWD;
};

BWDCourses.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/courses'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDCourses.prototype.get = BWDCourses.prototype.getV10;

BWDCourses.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/courses/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDCourses.prototype.getById = BWDCourses.prototype.getByIdV10;

BWDCourses.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/course'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDCourses.prototype.autocomplete = BWDCourses.prototype.autocompleteV10;

module.exports = BWDCourses;
