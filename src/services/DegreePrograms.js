var BWD;

var BWDDegreePrograms = function(_BWD) {
  BWD = _BWD;
};

BWDDegreePrograms.prototype.autocomplete = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/degreeprogram'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

module.exports = BWDDegreePrograms;
