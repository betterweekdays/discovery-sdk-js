var BWD;

var BWDFeed = function(_BWD) {
  BWD = _BWD;
};

BWDFeed.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/feed'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDFeed.prototype.get = BWDFeed.prototype.getV10;

BWDFeed.prototype.getByTypeV10 = function(type, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/feed/' + type),
    params: query
  };

  return BWD.Client.request(options);
};

BWDFeed.prototype.getByType = BWDFeed.prototype.getByTypeV10;

BWDFeed.prototype.getArticlesV10 = function(query) {
  return BWD.Feed.getByType('article', query);
};

BWDFeed.prototype.getArticles = BWDFeed.prototype.getArticlesV10;

BWDFeed.prototype.getJobsV10 = function(query) {
  return BWD.Feed.getByType('job', query);
};

BWDFeed.prototype.getJobs = BWDFeed.prototype.getJobsV10;

BWDFeed.prototype.getCompaniesV10 = function(query) {
  return BWD.Feed.getByType('company', query);
};

BWDFeed.prototype.getCompanies = BWDFeed.prototype.getCompaniesV10;

module.exports = BWDFeed;
