var BWD;

var BWDCompanies = function(_BWD) {
  BWD = _BWD;
};

BWDCompanies.prototype.getV11 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.get = BWDCompanies.prototype.getV11;

BWDCompanies.prototype.getByIdV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id)
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.getById = BWDCompanies.prototype.getByIdV11;

BWDCompanies.prototype.setFavoriteV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/favorite'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.setFavorite = BWDCompanies.prototype.setFavoriteV11;

BWDCompanies.prototype.isFavoriteV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/favorite')
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.isFavorite = BWDCompanies.prototype.isFavoriteV11;

BWDCompanies.prototype.setPassV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/pass'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.setPass = BWDCompanies.prototype.setPassV11;

BWDCompanies.prototype.isPassV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/pass')
  };

  return BWD.Client.request(options);
};

BWDCompanies.prototype.isPass = BWDCompanies.prototype.isPassV11;

BWDCompanies.prototype.setViewedV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/viewed'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.setViewed = BWDCompanies.prototype.setViewedV11;

BWDCompanies.prototype.isViewedV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/viewed')
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.isViewed = BWDCompanies.prototype.isViewedV11;

BWDCompanies.prototype.setPreviewedV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/previewed'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.setPreviewed = BWDCompanies.prototype.setPreviewedV11;

BWDCompanies.prototype.isPreviewedV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/previewed')
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.isPreviewed = BWDCompanies.prototype.isPreviewedV11;

BWDCompanies.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/company'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.autocomplete = BWDCompanies.prototype.autocompleteV10;

BWDCompanies.prototype.askForHelpV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/ask'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.askForHelp = BWDCompanies.prototype.askForHelpV11;

BWDCompanies.prototype.getAsksForHelpV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/ask')
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.getAsksForHelp =
  BWDCompanies.prototype.getAsksForHelpV11;

BWDCompanies.prototype.followV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/follow'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.follow = BWDCompanies.prototype.followV11;

BWDCompanies.prototype.isFollowingV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/follow')
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.isFollowing = BWDCompanies.prototype.isFollowingV11;

BWDCompanies.prototype.updateMatchV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/match'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.updateMatch = BWDCompanies.prototype.updateMatchV11;

BWDCompanies.prototype.getMatchStatusV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/companies/' + id + '/match')
  };
  return BWD.Client.request(options);
};

BWDCompanies.prototype.getMatchStatus =
  BWDCompanies.prototype.getMatchStatusV11;

module.exports = BWDCompanies;
