var BWD;

var BWDComment = function(_BWD) {
  BWD = _BWD;
};

BWDComment.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/comment'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDComment.prototype.get = BWDComment.prototype.getV10;

BWDComment.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/comment/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDComment.prototype.getById =
  BWDComment.prototype.getByIdV10;

BWDComment.prototype.createCommentV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/comment'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDComment.prototype.createComment =
  BWDComment.prototype.createCommentV10;

BWDComment.prototype.updateCommentV10 = function(id, data) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/comment/' + id),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDComment.prototype.updateComment =
  BWDComment.prototype.updateCommentV10;

module.exports = BWDComment;
