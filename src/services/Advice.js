var BWD;

var BWDAdvice = function(_BWD) {
  BWD = _BWD;
};

BWDAdvice.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/advice'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDAdvice.prototype.get = BWDAdvice.prototype.getV10;

module.exports = BWDAdvice;
