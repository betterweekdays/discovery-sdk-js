var BWD;

var BWDFavorites = function(_BWD) {
  BWD = _BWD;
};

BWDFavorites.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/favorites'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDFavorites.prototype.get = BWDFavorites.prototype.getV10;

module.exports = BWDFavorites;
