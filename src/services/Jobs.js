var BWD;

var BWDJobs = function(_BWD) {
  BWD = _BWD;
};

BWDJobs.prototype.getV11 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.get = BWDJobs.prototype.getV11;

BWDJobs.prototype.getByIdV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id)
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.getById = BWDJobs.prototype.getByIdV11;

BWDJobs.prototype.setFavoriteV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/favorite'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.setFavorite = BWDJobs.prototype.setFavoriteV11;

BWDJobs.prototype.isFavoriteV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/favorite')
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.isFavorite = BWDJobs.prototype.isFavoriteV11;

BWDJobs.prototype.setPassV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/pass'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.setPass = BWDJobs.prototype.setPassV11;

BWDJobs.prototype.isPassV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/pass')
  };

  return BWD.Client.request(options);
};

BWDJobs.prototype.isPass = BWDJobs.prototype.isPassV11;

BWDJobs.prototype.setInterestedInApplyingV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client
      .buildUrl('v1.1/jobs/' + id + '/interested-in-apply'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.setInterestedInApplying =
  BWDJobs.prototype.setInterestedInApplyingV11;

BWDJobs.prototype.isInterestedInApplyingV11 = function(id) {
  var options = {
    path: BWD.Client
      .buildUrl('v1.1/jobs/' + id + '/interested-in-apply')
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.isInterestedInApplying =
  BWDJobs.prototype.isInterestedInApplyingV11;

BWDJobs.prototype.setRecommendJobV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/recommend-job'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.setRecommendJob = BWDJobs.prototype.setRecommendJobV11;

BWDJobs.prototype.isRecommendJobV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/recommend-job')
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.isRecommendJob = BWDJobs.prototype.isRecommendJobV11;

BWDJobs.prototype.setPreviewedV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/previewed'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.setPreviewed = BWDJobs.prototype.setPreviewedV11;

BWDJobs.prototype.isPreviewedV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/previewed')
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.isPreviewed = BWDJobs.prototype.isPreviewedV11;

BWDJobs.prototype.setViewedV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/viewed'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.setViewed = BWDJobs.prototype.setViewedV11;

BWDJobs.prototype.isViewedV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/viewed')
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.isViewed = BWDJobs.prototype.isViewedV11;

BWDJobs.prototype.getInteractionsV11 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/todos'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.getInteractions = BWDJobs.prototype.getInteractionsV11;

BWDJobs.prototype.askForHelpV11 = function(id, data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/ask'),
    entity: data
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.askForHelp = BWDJobs.prototype.askForHelpV11;

BWDJobs.prototype.getAsksForHelpV11 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.1/jobs/' + id + '/ask')
  };
  return BWD.Client.request(options);
};

BWDJobs.prototype.getAsksForHelp = BWDJobs.prototype.getAsksForHelpV11;

module.exports = BWDJobs;
