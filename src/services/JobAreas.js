var BWD;

var BWDJobAreas = function(_BWD) {
  BWD = _BWD;
};

BWDJobAreas.prototype.autocompleteV10 = function(title) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/autocomplete/jobarea'),
    params: {
      title: title
    }
  };
  return BWD.Client.request(options);
};

BWDJobAreas.prototype.autocomplete = BWDJobAreas.prototype.autocompleteV10;

module.exports = BWDJobAreas;
