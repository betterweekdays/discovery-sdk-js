var BWD;
var Config;
var assign = require('lodash/assign');

var BWDConfig = function(_BWD) {
  BWD = _BWD;
  this.reset();
};

BWDConfig.prototype.get = function(key) {
  return Config[key];
};

BWDConfig.prototype.reset = function() {
  Config = assign({}, BWD.DefaultConfig);
};

BWDConfig.prototype.set = function(configOrKey, value) {
  if (typeof configOrKey === 'string' &&
      BWD.DefaultConfig.hasOwnProperty(configOrKey)) {
    Config[configOrKey] = value;
  } else {
    for (var key in configOrKey) {
      if (configOrKey.hasOwnProperty(key) &&
          BWD.DefaultConfig.hasOwnProperty(key)) {
        Config[key] = configOrKey[key];
      }
    }
  }
};

module.exports = BWDConfig;
