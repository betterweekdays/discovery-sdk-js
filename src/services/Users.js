var BWD;

var BWDUsers = function(_BWD) {
  BWD = _BWD;
};

BWDUsers.prototype.getV12 = function() {
  var options = {
    path: BWD.Client.buildUrl('v1.2/users')
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.get = BWDUsers.prototype.getV12;

BWDUsers.prototype.getMeV15 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.5/users/me'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getMe = BWDUsers.prototype.getMeV15;

BWDUsers.prototype.getByIdV12 = function(id) {
  var options = {
    path: BWD.Client.buildUrl('v1.2/users/' + id)
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getById = BWDUsers.prototype.getByIdV12;

BWDUsers.prototype.updateV12 = function(id, user) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.2/users/' + id),
    entity: user
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.update = BWDUsers.prototype.updateV12;

BWDUsers.prototype.updateMeV15 = function(user) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.5/users/me'),
    entity: user
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.updateMe = BWDUsers.prototype.updateMeV15;

BWDUsers.prototype.registerAuth0TokenV11 = function(token, user) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    path: BWD.Client.buildUrl('v1.1/user-registration'),
    entity: user
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.registerAuth0Token =
  BWDUsers.prototype.registerAuth0TokenV11;

BWDUsers.prototype.recordLoginStatsV12 = function() {
  var options = {
    method: 'POST',
    headers: {
      Accept: '*/*'
    },
    path: BWD.Client.buildUrl('v1.2/users/me/login')
  };
  return BWD.Client.request(options);
};

BWDUsers.prototype.recordLoginStats = BWDUsers.prototype.recordLoginStatsV12;

BWDUsers.prototype.recordLogoutStatsV12 = function() {
  var options = {
    method: 'POST',
    headers: {
      Accept: '*/*'
    },
    path: BWD.Client.buildUrl('v1.2/users/me/logout')
  };
  return BWD.Client.request(options);
};

BWDUsers.prototype.recordLogoutStats = BWDUsers.prototype.recordLogoutStatsV12;

BWDUsers.prototype.getAssessmentInfoV10 = function() {
  var options = {
    path: BWD.Client.buildUrl('v1.0/user-assessments/me')
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getAssessmentInfo = BWDUsers.prototype.getAssessmentInfoV10;

BWDUsers.prototype.updateAssessmentInfoV10 = function(assessmentInfo) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/user-assessments/me'),
    entity: assessmentInfo
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.updateAssessmentInfo =
  BWDUsers.prototype.updateAssessmentInfoV10;

BWDUsers.prototype.submitAssessmentInfoV10 = function() {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/user-assessments/me/submit')
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.submitAssessmentInfo =
  BWDUsers.prototype.submitAssessmentInfoV10;

BWDUsers.prototype.getIndividualAssessmentResultsV10 = function(teamMemberId) {
  const basePath = 'v1.0/user-assessments/individual/';
  var options = {
    method: 'GET',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl(basePath + teamMemberId)
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getIndividualAssessmentResults =
  BWDUsers.prototype.getIndividualAssessmentResultsV10;

BWDUsers.prototype.triggerPayWallEventV10 = function(data, query) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/user-assessments/paywall'),
    entity: data,
    params: query
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.triggerPayWallEvent =
  BWDUsers.prototype.triggerPayWallEventV10;

BWDUsers.prototype.triggerAssessPreviewStartedEventV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/user-assessments/preview'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.triggerAssessPreviewStartedEvent =
  BWDUsers.prototype.triggerAssessPreviewStartedEventV10;

BWDUsers.prototype.triggerResultsViewedEventV10 = function(id) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/user-assessments/viewed/' + id)
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.triggerResultsViewedEvent =
  BWDUsers.prototype.triggerResultsViewedEventV10;

BWDUsers.prototype.getAssessmentCreditsV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/assessmentcredit/me'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getAssessmentCredits =
  BWDUsers.prototype.getAssessmentCreditsV10;

BWDUsers.prototype.registerAssessmentV10 = function() {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/user-assessments')
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.registerAssessment =
  BWDUsers.prototype.registerAssessmentV10;

BWDUsers.prototype.claimAssessmentCreditsV11 = function(accessCode) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/assessmentcreditclaim/me/' + accessCode)
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.claimAssessmentCredits =
  BWDUsers.prototype.claimAssessmentCreditsV11;

BWDUsers.prototype.getTeamsV10 = function(query) {
  var options = {
    method: 'GET',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/team/me'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getTeams =
  BWDUsers.prototype.getTeamsV10;

BWDUsers.prototype.getTeamAggregateAssessmentResultsV10 = function(query) {
  var options = {
    method: 'GET',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/team/me/aggregate'),
    params: query
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getTeamAggregateAssessmentResults =
  BWDUsers.prototype.getTeamAggregateAssessmentResultsV10;

BWDUsers.prototype.triggerTeamAggregateResultsViewedEventV11 = function() {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.1/team/me/aggregate/viewed')
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.triggerTeamAggregateResultsViewedEvent =
  BWDUsers.prototype.triggerTeamAggregateResultsViewedEventV11;

BWDUsers.prototype.getProfileV10 = function(id) {
  var options = {
    method: 'GET',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/profile/' + id)
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getProfile = BWDUsers.prototype.getProfileV10;

BWDUsers.prototype.createProfileV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/profile'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.createProfile = BWDUsers.prototype.createProfileV10;

BWDUsers.prototype.updateProfileV10 = function(id, data) {
  var options = {
    method: 'PATCH',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/profile/' + id),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.updateProfile = BWDUsers.prototype.updateProfileV10;

BWDUsers.prototype.discourseLoginV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/discourse/login'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.discourseLogin = BWDUsers.prototype.discourseLoginV10;

BWDUsers.prototype.getReferralsV10 = function(id) {
  var options = {
    method: 'GET',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/referrals/' + id)
  };

  return BWD.Client.request(options);
};

BWDUsers.prototype.getReferrals = BWDUsers.prototype.getReferralsV10;

module.exports = BWDUsers;
