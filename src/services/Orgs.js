var BWD;

var BWDOrgs = function(_BWD) {
  BWD = _BWD;
};

BWDOrgs.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/orgs'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDOrgs.prototype.get = BWDOrgs.prototype.getV10;

BWDOrgs.prototype.getByIdV10 = function(id, query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/orgs/' + id),
    params: query
  };

  return BWD.Client.request(options);
};

BWDOrgs.prototype.getById = BWDOrgs.prototype.getByIdV10;

module.exports = BWDOrgs;
