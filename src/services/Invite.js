var BWD;

var BWDInvite = function(_BWD) {
  BWD = _BWD;
};

BWDInvite.prototype.inviteEmailV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/invite/email'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDInvite.prototype.inviteEmail =
  BWDInvite.prototype.inviteEmailV10;

BWDInvite.prototype.inviteSmsV10 = function(data) {
  var options = {
    method: 'POST',
    headers: {
      'Accept': '*/*',
      'Content-Type': 'application/json'
    },
    path: BWD.Client.buildUrl('v1.0/invite/sms'),
    entity: data
  };

  return BWD.Client.request(options);
};

BWDInvite.prototype.inviteSms =
  BWDInvite.prototype.inviteSmsV10;

module.exports = BWDInvite;
