var BWD;

var BWDStats = function(_BWD) {
  BWD = _BWD;
};

BWDStats.prototype.getV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/stats'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDStats.prototype.get = BWDStats.prototype.getV10;

BWDStats.prototype.getSummaryV10 = function(query) {
  var options = {
    path: BWD.Client.buildUrl('v1.0/stats/summary'),
    params: query
  };
  return BWD.Client.request(options);
};

BWDStats.prototype.getSummary = BWDStats.prototype.getSummaryV10;

module.exports = BWDStats;
